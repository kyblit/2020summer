DELETE FROM ky_raw_data WHERE object_id = 1;
DELETE FROM ky_object_relation WHERE lookup_id = 1;
DELETE FROM ky_object WHERE id in (1,2,3);
DELETE FROM ky_relation_type WHERE id in (1,2, 3);
DELETE FROM `ky_source` WHERE id = 1;
DELETE FROM `ky_group` WHERE id in (1,2);

insert into ky_object (id, `name`)
values (1, "Africa"),(2, "Saharan"),(3, "noun");

insert into `ky_source` (id, source_name, source_url, created_date)
values (1, "Word Associations", "https://wordassociations.net/en/api", '2020-07-14 10:37:22');

insert into `ky_raw_data` (object_id, source_id, raw_data, created_date)
values (1, 1, "{\"random json data\":123}", '2020-07-14 10:37:22');

insert into ky_relation_type (id, relation_name)
values (1, "associated word"), (2, "POS"), (3,"score");

insert into `ky_group` (id, group_name)
values (1, "africa saharan"), (2, "POS");

insert into ky_object_relation (lookup_id, result, data_source_id, relation_type_id, group_id, is_pointer ,created_date)
values (1, 2, 1, 1, 1, 1, '2020-07-14 10:37:22'), (1, 3, 1, 2, 2, 1,'2020-07-14 10:37:22'), (1, 0, 1, 1, 1, 0,'2020-07-14 10:37:22');


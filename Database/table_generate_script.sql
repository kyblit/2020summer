USE `2020Summer`;
DROP TABLE IF EXISTS `ky_object_relation`;
DROP TABLE IF EXISTS `ky_raw_data`;
DROP TABLE IF EXISTS `ky_object`;
DROP TABLE IF EXISTS `ky_source`;
DROP TABLE IF EXISTS `ky_relation_type`;
DROP TABLE IF EXISTS `ky_group`;
DROP TABLE IF EXISTS `ky_version`;



CREATE TABLE `ky_object` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  unique key (`name`));


CREATE TABLE `ky_source` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `source_name` VARCHAR(255) NOT NULL,
  `source_url` VARCHAR(255) NOT NULL,
  `created_date` TIMESTAMP NOT NULL default current_timestamp,
  PRIMARY KEY (`id`),
  unique key (`source_name`, `source_url`));

CREATE TABLE `ky_relation_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `relation_name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `ky_group` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `group_name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `ky_version` (
  `version` VARCHAR(255) NOT NULL);

CREATE TABLE `ky_raw_data` (
  `source_id` INT NOT NULL,
  `object_id` INT NOT NULL,
  `raw_data` JSON NOT NULL,
  `created_date` TIMESTAMP NOT NULL default current_timestamp,
  INDEX `object_id_idx` (`object_id` ASC) VISIBLE,
  INDEX `source_id_idx` (`source_id` ASC) VISIBLE,
  CONSTRAINT `object_id`
    FOREIGN KEY (`object_id`)
    REFERENCES `ky_object` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `source_id`
    FOREIGN KEY (`source_id`)
    REFERENCES `ky_source` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


CREATE TABLE `ky_object_relation` (
  `lookup_id` INT NOT NULL,
  `result` INT NOT NULL,
  `relation_type_id` INT NOT NULL,
  `data_source_id` INT NOT NULL, 
  `group_id` INT NULL,
  `is_pointer` boolean NOT NULL,
  `created_date` TIMESTAMP NOT NULL default current_timestamp,
  INDEX `lookup_id_idx` (`lookup_id` ASC) VISIBLE,
  INDEX `relation_id_idx` (`relation_type_id` ASC) VISIBLE,
  INDEX `source_id_idx` (`data_source_id` ASC) VISIBLE,
  INDEX `group_id_idx` (`group_id` ASC) visible,
  CONSTRAINT `lookup_id`
    FOREIGN KEY (`lookup_id`)
    REFERENCES `ky_object` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `relation_type_id`
    FOREIGN KEY (`relation_type_id`)
    REFERENCES `ky_relation_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `data_source_id`
    FOREIGN KEY (`data_source_id`)
    REFERENCES `ky_source` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
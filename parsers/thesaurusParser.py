import json
import requests

# key for thesaurus api
api_key = "49efd194-fe32-40ea-b2bf-e4503ec11435"
# thesaurus api url
url = 'https://www.dictionaryapi.com/api/v3/references/thesaurus/json/sporty?key=49efd194-fe32-40ea-b2bf-e4503ec11435'
r = requests.get(url)
# decodes the json data
data = r.json()
nData = data[0]

def parser(toParse):
    global term
    global syns
    global ants
    global definition
    global newData
    newData = toParse[0]
    existsInSyns = 0
    existsInAnts = 0

    # checks to see whether there is data available to parse
    if(isinstance(newData, str)):
        return []
    else:
        # get word
        tempTerm = newData.get('meta').get('id')
        term = ""
        for letter in tempTerm:
            if letter != ":":
                term = term + letter
            else:
                break
        # get synonyms
        if len(newData.get('meta').get('syns')) == 0:
            syns = []
        else:
            syns = []
            for wordList in newData.get('meta').get('syns'):
                for word in wordList:
                    for w in syns:
                        if(word == w):
                            existsInSyns = 1
                    if(existsInSyns != 1):
                        syns.append(word)
        # get antonyms
        if len(newData.get('meta').get('ants')) == 0:
            ants = []
        else:
            ants = []
            for wordList in newData.get('meta').get('ants'):
                for word in wordList:
                    for w in ants:
                        if (word == w):
                            existsInAnts = 1
                    if (existsInAnts != 1):
                        ants.append(word)
        # get definition
        definition = []
        for defn in newData.get('shortdef'):
            definition.append(defn)

        # create array
        # TODO: figure out various configurations
        # array = arrayObject()
        # newArray = array.returnArray("synonyms")
        # return newArray
        array = getArrayWithSynonyms()
        return array

# TODO: implement a facade class
class arrayObject(object):
    def returnArray(self, key):
        if(key == "synonyms"):
            getArrayWithSynonyms()

def getArrayWithDefinitions():
    arr = []
    definitionsCopy = definition.copy()
    rows, cols = (2, len(definition) + 1)
    for i in range(cols):
        if(i == 0):
            col = ["search term", "definition"]
        else:
            col = []
        for j in range(rows):
            if(i != 0):
                if(j == 0):
                    col.append(term)
                else:
                    if(len(definitionsCopy) != 0):
                        col.append(definitionsCopy[0])
                        del definitionsCopy[0]
        arr.append(col)
    return arr

def getArrayWithSynonyms():
    #   j  0    1
    # i
    # 0    def  syn
    # 1    def  syn
    arr = []
    synsCopy = syns.copy()
    rows, cols = (2, len(syns) + 1)
    for i in range(cols):
        if (i == 0):
            col = ["search term", "synonyms"]
        else:
            col = []
        for j in range(rows):
            if(i != 0):
                if (j == 0):
                     col.append(term)
                else:
                    if (len(synsCopy) != 0):
                        col.append(synsCopy[0])
                        del synsCopy[0]
        arr.append(col)
    return arr

def getArrayWithAntonyms():
    #   j  0    1
    # i
    # 0    def  syn
    # 1    def  syn
    arr = []
    antsCopy = ants.copy()
    rows, cols = (2, len(ants) + 1)
    for i in range(cols):
        if (i == 0):
            col = ["search term", "antonyms"]
        else:
            col = []
        for j in range(rows):
            if(i != 0):
                if (j == 0):
                     col.append(term)
                else:
                    if (len(antsCopy) != 0):
                        col.append(antsCopy[0])
                        del antsCopy[0]
        arr.append(col)
    return arr

def getArrayWithAllElements():
    synsCopy = syns.copy()
    antsCopy = ants.copy()
    definitionCopy = definition.copy()
    blankChar = ""
    if (len(syns) > len(ants)):
        numCols = len(syns) + 1
    else:
        numCols = len(ants) + 1
    rows, cols = (4, numCols)
    arr = []
    for i in range(cols):
        if (i == 0):
            col = ["search term", "synonyms", "antonyms", "definition"]
        else:
            col = []
        for j in range(rows):
            if (i != 0):
                if (i == 1 and j == 0):
                    col.append(term)
                if (j == 3):
                    if (len(definitionCopy) != 0):
                        col.append(definitionCopy[0])
                        del definitionCopy[0]
                    else:
                        col.append(blankChar)
                if (i >= 2 and j == 0):
                    col.append(blankChar)
                if (j == 1):
                    if (len(synsCopy) != 0):
                        col.append(synsCopy[0])
                        del synsCopy[0]
                    else:
                        col.append(blankChar)
                if (j == 2):
                    if (len(antsCopy) != 0):
                        col.append(antsCopy[0])
                        del antsCopy[0]
                    else:
                        col.append(blankChar)
        arr.append(col)
    return arr

def apiResponse():
    return r.status_code

def getWord():
    parser(data)
    return term

def getSynonyms():
    parser(data)
    return syns

def getAntonyms():
    parser(data)
    return ants

def getDefinition():
    return definition

def printArray(arr):
    for row in arr:
        print(row)

def main():
    printArray(parser(newData))

if __name__ == '__main__':
    main()

# code for accessing data as list
# word = data[0]['meta']['id']
# print("word: " + word)
# print("stems: ")
# stems = data[0]['meta']['stems']
# print(stems)
# print("synonyms: ")
# synonyms = data[0]['meta']['syns'][0]
# print(synonyms)
# print("antonyms: ")
# antonyms = data[0]['meta']['ants'][0]
# print(antonyms)
# print("short definition: ")
# shortDef = data[0]['shortdef']
# print(shortDef)
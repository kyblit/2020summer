import requests
import csv
import time
import json

def parser(toParse):
    start = 1
    allRows = [["Search Item", "Related", "POS", "Weight"]]
    wa_results = toParse.get("response")
    wa_items=wa_results[0].get("items")
    wa_word=wa_results[0].get("text")
    for wordInfo in wa_items:
        wa_res_aWord = wordInfo.get("item")
        wa_res_pos = wordInfo.get("pos")
        wa_res_weight = wordInfo.get("weight")
        tempRow = [wa_word, wa_res_aWord, wa_res_pos, wa_res_weight]
        allRows.append(tempRow)
    return allRows
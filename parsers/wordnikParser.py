import json
import re


# def wordnikParser(toParse, url):
#     arr = re.split("\?|/", url)[-2::]
#     if (arr[0] == "examples"):
#         wordnikExParser(toParse)
#     elif (arr[0] == "definitions"):
#         wordnikDefParser(toParse)

# def parser(toParse):
#     result = [["word","partOfSpeech","attributionText","text","attributionUrl"]]
#     for p in toParse:
#         result.append([p["word"],p["partOfSpeech"],p["attributionText"],p["text"],p["attributionUrl"]])
#     return result

def parser(toParse, word, cfgData):
    result = [["word", "rating", "url", "text"]]
    if 'examples' in toParse:
        for p in toParse['examples']:
            toAdd = []
            toAdd.append(p["word"])
            if 'rating' in p:
                toAdd.append(p["rating"])
            if 'url' in p:
                toAdd.append(p["url"])
            if 'text' in p:
                toAdd.append(p["text"])
            result.append(toAdd)
    return result

# def test1():
#     with open('testfiles/wordniktest.json') as f:
#         parser(json.load(f))

# def test2():
#      with open('testfiles/wordnikexample.json') as f:
#         wordnikParser(json.load(f), "https://api.wordnik.com/v4/word.json/apple/examples?includeDuplicates=false&useCanonical=false&limit=5&api_key=YOURAPIKEY")


# test1()
import requests
import json
import csv


for word in words:
    parameters = {'ml':word}
    response=requests.get('https://api.datamuse.com/words', params=parameters)
    data = response.json()
    for entry in data:
        aWord = entry.get("word").encode('utf-8')
        score = entry.get("score")
        tempRow = {'Word':word,'Associated Word': aWord, 'Score':score}
        allRows.append(tempRow)

filename = "../../experiments/lauren/wordAssociations/dataMuse_results.csv"
with open(filename, 'w') as csvfile:
    csvWriter = csv.DictWriter(csvfile, fieldnames = csv_fields)
    csvWriter.writeheader()
    for row in allRows:
    	csvWriter.writerow(row)


def parser(toParse):
    start = 1
    allRows = [["Search Item", "Related", "Score"]]
    for entry in toParse:
        aWord = entry.get("word").encode('utf-8')
        score = entry.get("score")
        tempRow = [word, aWord , score]
        allRows.append(tempRow)


    wa_results = toParse.get("response")
    wa_items=wa_results[0].get("items")
    wa_word=wa_results[0].get("text")
    for wordInfo in wa_items:
        wa_res_aWord = wordInfo.get("item")
        wa_res_pos = wordInfo.get("pos")
        wa_res_weight = wordInfo.get("weight")
        tempRow = [wa_word, wa_res_aWord, wa_res_pos, wa_res_weight]
        allRows.append(tempRow)
    return allRows
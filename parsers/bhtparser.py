import json
import csv


# prints json data from api call into file
def writeJson(data, filename="temp_bht.json"):
    with open(filename, "w") as jFile:
        json.dump(data, jFile, indent=3)


# Big Huge Thesaurus api call format: https://words.bighugelabs.com/api/2/{API key}/{word}/{format}
#api_key = "494106960f39f3845a583d073574aca2"


# reformats the json data from the api call in a format that's easier to parse
# new format will be a list of dictionaries in form: [{word, partOfSpeech, relation, related}]
# related is a list of the related words
def parser(rawData):
    if isinstance(rawData, str):
        rawDict = eval(rawData)
    else:
        rawDict = rawData
    #iterate through original json
    reformattedData = dict()
    keyList = [""]
    #the key is always part of speech
    for key in rawDict:
        append = False
        #iterate through list of related words
        for relation in rawDict[key]:
            if relation != "ant":  # filter out antonyms
            #sort data into repsective relations
            #TODO: sort is brute force and inefficient
                for relationKey in keyList:
                    if relation == relationKey:
                        append = True
                        break
                else:
                    keyList.append(relation)
                related = rawDict[key][relation]
                if append:
                    reformattedData[relation].extend(related)
                else:
                    reformattedData.update({relation: related})
        #tempDict.update({"word": word, "partOfSpeech": bhtDict, "relation": relation, "related": related})
    print(reformattedData)
    return reformattedData
    #return jsonToDBArr(reformattedData)
    #return jsonToCsvArr(reformattedData) #this is so apiDataFetcher returns the 2d array


# converts the reformatted json data into the db insert array format
# {word, partOfSpeech, relation, related}
# array format: [[word], [relation, related]]
def jsonToDBArr(jsonData):
    for wordDict in jsonData:
        word = wordDict["word"]
        relation = wordDict["relation"]
        header = [word]
    bhtArr = [header]
    for wordDict in jsonData:
        #since we already know the dict format, we can just access what we need

        relatedList = wordDict["related"]
        bhtArr = [word, ]
        for related in relatedList:
            tempList = [relation, related]
            bhtArr.append(tempList)
            tempList = []
    return bhtArr


# converts json to another array organization
# csv array format: [word, partOfSpeech, relation, related]
def jsonToCsvArr(jsonData):
    bhtArr = []
    for wordDict in jsonData:
        insList = []
        word = wordDict["word"]
        pos = wordDict["partOfSpeech"]
        relation = wordDict["relation"]
        relatedList = wordDict["related"]
        #every related word has a separate entry with word, part of speech, and relation
        for related in relatedList:
            insList = [word, pos, relation, related]
            bhtArr.append(insList)
    return bhtArr

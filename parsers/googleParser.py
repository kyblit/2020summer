# sample parser for google custom search engine json format dictionaries.
# @toParse: the json object to parse in python's representation (dictionary)
# @return: a 2d array where the first row is the defining name and first column is the search term
def parser(toParse):
    returnArr = [["Search Term", "URL", "Site Title", "HTML Snippet"]]
    se_results = toParse.get("items")
    searchTerm = toParse.get("queries").get("request")[0].get("searchTerms")
    for tempWork in se_results:
        tempTitle = tempWork.get("title").replace('<b>', '')
        tempTitle = tempTitle.replace('</b>', '')
        tempSnip = tempWork.get("htmlSnippet").replace('<b>', '')
        tempSnip = tempSnip.replace('</b>', '')
        tempArr = [searchTerm, tempWork.get("link"), tempTitle, tempSnip]
        returnArr.append(tempArr)
    return returnArr

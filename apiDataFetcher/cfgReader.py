import configparser
from datetime import datetime
import sys
import json

# parses a cfg file into a cfgData object, format described below
# cfgParse(cfgPath) = [
# ['dog', 'cat', 'airplane', 'book'],
# {"sourcename": "thisApi", "url": "www.thisAPIURL.com", "key": "1234qwerty", "parser": "thisApiParser"},
# {"host": "localhost", "user": "mySqlUserName", "pass": "mySqlPassword", "dbname": "kyblitDataBase"},
# {"expiration": 30}
# ]


# @cfgName absolute path to the cfg file on the local machine, formatted as r"C:\thisisthepath"
# @return the cfgData object which format is shown above
def cfgParse(cfgName):
    config = configparser.ConfigParser()

    try:
        config.read(cfgName)
    except OSError:
        # TODO: Major Error, Error Level CRITICAL, might only be ERROR if we default to all default values
        sys.exit()

    fileLoc = ""
    for i in config['WordList']:
        fileLoc = config['WordList'][i]

    apiDetails = {}
    for i in config['SourceAccess']:
        apiDetails[i] = config['SourceAccess'][i]

    mySQLCred = {}
    for i in config['MySqlAccess']:
        mySQLCred[i] = config['MySqlAccess'][i]

    programOptions = {}
    for i in config['Options']:
        programOptions[i] = config['Options'][i]

    '''loggerCfg = {}
    for i in config['LoggerName']:
        loggerCfg[i] = config['LoggerName'][i]'''

    wordList = []
    try:
        wordFile = open(fileLoc, "r")
    except OSError:
        # TODO: log Error, CRITICAL Error
        sys.exit()

    for i in wordFile:
        wordList.append(i.replace("\n", ""))

    return [wordList, apiDetails, mySQLCred, programOptions]

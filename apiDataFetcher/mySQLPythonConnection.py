import mysql.connector
from mysql.connector import Error
from datetime import datetime
import json
from apiDataFetcher import versatileGET

# @cfgData = relevant data from the configuration file, the third section.
# @tableName = name of the table to access
# @column_names = name of the columns in the table to access, access those columns in relation to the id column
# @filter = the term to search for within the specified column name
# @return = a list of lists containing the row that matches the filter.
def filteredTableGet(table_name, column_name, toFilter, connection):

    returnTable = []
    try:
        cursor = connection.cursor()
        # construct the query in mySQL
        sql_select_Query = f"select * FROM {table_name} WHERE {column_name} = '{toFilter}'"

        # execute the query
        cursor.execute(sql_select_Query)
        records = cursor.fetchall()
        #print("total rows: ", cursor.rowcount)

        # insert the result as a list of lists
        for row in records:
            toAdd = []
            for x in row:
                toAdd.append(x)
            returnTable.append(toAdd)
    except Error as e:
        print("Error reading", e)
    #finally:
        #if connection.is_connected():
            #print("Successfully fetched")
    return returnTable #table of all rows that match the criteria

# inserts parsed data into db
# parsedDict: {relation: [list of related words pertaining to this relation]}
# checkArray: [word, urlID, objID]. if an id doesn't exist there is a empty list instead
# connection is a connection object
def tableIns(parsedDict, checkArray, connection):
    try:
        cursor = connection.cursor()
        # get ids
        srcID = checkArray[1]
        wordID = checkArray[2]

        # insert data into db
        #TODO: sorting for ky_group
        #iterate through parsedDict
        for relation in parsedDict:
            # ky_relation_type table
            relationID = getEntry("ky_relation_type", "relation_name", relation, connection)
            #if relation doesn't exist insert it
            if (not relationID):
                tempRelation = (relation,)
                typeQuery = "INSERT INTO ky_relation_type (relation_name) VALUES (%s)"
                cursor.execute(typeQuery, tempRelation)
                connection.commit()
                relationID = cursor.lastrowid
            #iterate through list of related words
            for related in parsedDict[relation]:
                # related words in ky_object Table
                relatedID = getEntry("ky_object", "name", related, connection)
                if not relatedID:
                    tempRelated = (related,)
                    relatedQuery = "INSERT INTO ky_object (name) VALUES (%s)"
                    cursor.execute(relatedQuery, tempRelated)
                    connection.commit()
                    relatedID = cursor.lastrowid
                # ky_object_relation Table
                #if generated row already exists skip insertion
                selectQuery = "select * FROM ky_object_relation WHERE lookup_id = (%s) AND result = (%s) AND " \
                              "relation_type_id = (%s) AND data_source_id = (%s) AND group_id = (%s) AND is_pointer =" \
                              " (%s)"
                relationVal = (wordID, relatedID, relationID, srcID, 0, 1)
                cursor.execute(selectQuery, relationVal)
                records = cursor.fetchone()
                if not records:
                    relationQuery = "INSERT INTO ky_object_relation (lookup_id, result, relation_type_id, data_source_id, " \
                                    "group_id, is_pointer) VALUES (%s, %s, %s, %s, %s, %s) "
                    # TODO: groupID temporarily always set to 1 and isPointer always set to true
                    cursor.execute(relationQuery, relationVal)
                    connection.commit()
            print(cursor.rowcount, "Record(s) successfully inserted for parsed data.")
    except Error as e:
        print("Error Inserting", e)
    finally:
        if connection.is_connected():
            cursor.close()
    return

#inserts raw data
#cfgData dictionary of config settings from ini file
# rawData is a string
def insertRaw(cfgData, rawData, checkArray, connection):
    try:
        cursor = connection.cursor()
        # cfgData parser converts keys to lower case
        srcName = cfgData[1]["sourcename"]
        srcURL = cfgData[1]["url"]
        # Source table
        # check if source exists
        srcID = checkArray[1]
        if not srcID:
            tempSrcQuery = "INSERT INTO ky_source (source_name, source_url) VALUES (%s, %s)"
            srcVal = (srcName, srcURL)
            cursor.execute(tempSrcQuery, srcVal)
            connection.commit()
            srcID = cursor.lastrowid  # gets the auto incremented id from the last updated row from memory
            checkArray[1] = srcID
            # connection.commit()
        # insert word into object table
        word = checkArray[0]
        wordID = checkArray[2]
        if not wordID:
            # insert word into Object table
            word = (word,)
            objQuery = "INSERT INTO ky_object (name) VALUES (%s)"
            cursor.execute(objQuery, word)
            connection.commit()
            wordID = cursor.lastrowid
            checkArray[2] = wordID
            # connection.commit()
        # Raw_Data table
        rawData = json.dumps(rawData)
        rawQuery = "INSERT INTO ky_raw_data (source_id, object_id, raw_data) VALUES (%s, %s, %s)"
        rawValue = (srcID, wordID, rawData)
        cursor.execute(rawQuery, rawValue)
        connection.commit()
        print(cursor.rowcount, "Record(s) successfully inserted")
    except Error as e:
        print("Error Inserting", e)
    finally:
        if connection.is_connected():
            cursor.close()
            print("Successfully inserted")
    return checkArray

# gets the entry from its respective table and column.
# if the entry exists the id is returned
# if not a empty list is returned
def getEntry(tableName, colName, objName, connection):
    entryID = filteredTableGet(tableName, colName, objName, connection)
    if entryID:
        #TODO: assumes the id we want is at [0][0]
        return entryID[0][0]
    return entryID #empty list if id doesn't exist


# returns True if object_name has expired
def checkExpiration(cfgData, word, connection):
    expiryTime = int(cfgData[3]["expiration"]) #expiration is in days
    if not expiryTime:
        expiryTime = 30 #if no expiration is specified set default to 30
    #assumes object_name already exists in table
    objectID = getEntry("ky_object", "name", word, connection)
    if not objectID:
        return True
    rawDataRow = filteredTableGet("ky_raw_data", "object_id", objectID, connection)
    if rawDataRow:
        currentTime = datetime.now()
        dataFetchTime = rawDataRow[0][3]
        daysPassed = (currentTime - dataFetchTime).days
        if daysPassed < expiryTime:
            return False
    return True

# update entry in raw data
def updateRaw(checkArray, rawData, connection):
    try:
        raw = json.dumps(rawData)
        cursor = connection.cursor()
        objID = checkArray[2]
        urlID = checkArray[1]
        updateQuery = "UPDATE ky_raw_data SET raw_data = (%s), created_date = (%s) WHERE object_id = (%s) AND source_id = (%s)"
        updateValue = (raw, datetime.now(), objID, urlID)
        cursor.execute(updateQuery, updateValue)
        connection.commit()
        print("Raw data for " + checkArray[0] + " has been updated")
    except Error as e:
        print("Error Inserting", e)
    finally:
        if connection.is_connected():
            cursor.close()
    return

import json
import sys
import requests
from datetime import datetime
import re


# takes in a single word and makes an api call for it
def versGET(apiDetails, word):
    # replace all <> parameters in the api
    apiUrl = apiDetails["url"]
    apiUrl = apiUrl.replace("<key>", apiDetails['key'])
    apiUrl = apiUrl.replace("<word>", word)
    keyList = re.findall(r'<[^>]*>', apiUrl)
    for i in keyList:
        apiUrl = apiUrl.replace(i, apiDetails[i.replace("<", "").replace(">", "")])
    try:
        response = requests.get(url=apiUrl)
    except requests.exceptions.Timeout:
        return
        # maybe continue requests in a loop? Ask during sprint
    except requests.exceptions.TooManyRedirects:
        return
    except requests.exceptions.RequestException as e:
        raise SystemExit(e)  # Fatal Error
    # check for any error codes the api may have returned and return it
    data = response.json()
    return [data, response.status_code]


# dumps raw data of search and term into a text file
def jsonRaw(orgJson, searchTerm, cfgData):
    fileName = f"{searchTerm}_{cfgData[1]['sourcename']}_RAW.txt"
    with open(fileName + ".txt", 'w', encoding='utf-8') as f:
        json.dump(orgJson, f, ensure_ascii=False, indent=4)
    return


# Used to check if the data passed to it qualifies as an error in the context of the versatileGET suite
# @checkError = the item which to check if it is an error or not
# @searchTerm = the term that was being searched when this method was called
# @cfgData = the cfgData object that contains all the data for the current run,
#          see apiDataFetcher\cfgReader for details on how to access elements inside it
# @return = a positive number if the current item is an error, a negative number if it is a normal object,
#          and 0 if a critical error is found

def apiStatusCodeHandling(checkError, searchTerm, cfgData):
    print(checkError)
    if isinstance(checkError, int):
        if checkError == 200:
            # TODO: No Error, INFO level in logger
            pass
        else:
            # TODO: Minor Error, ERROR level in logger
            return checkError
    else:
        return 0
    return -1


# !!! UNUSED CODE !!!
# take in a list of words and iterate it into versGET, spit out list of json objects to send to parser INDIVIDUALLY
# add proper support for time and source URL
'''
def multiGET(apiDetails, wordList):
    resultList = []
    extraResults = []
    for i in wordList:
        resultList.append(versGET(apiDetails, i))
        extraResults.append(getAndFormatTime())
        extraResults.append(apiDetails["sourcename"])
    return resultList
'''

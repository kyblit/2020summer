from datetime import datetime
import sys

# global variables
fileLoc = ""
logType = ""
cfgData = []


# start the logger by creating or wiping the existing log file and checking any arguments
# add argument to differentiate debugging, normal operation, and others
# add argument to chose filename/location
# add argument for cfgData
def startLogger(loggerType="basic", fileLocation=r"C:\Users\ChuckeyTheBear\testLogger.txt"):
    global fileLoc
    fileLoc = fileLocation
    global logType
    logType = loggerType
    try:
        with open(fileLoc, "w", encoding='utf-8') as f:
            f.write("Log file starting at: " + str(datetime.now()) + "\n")
    except IOError:
        print("Critical Error : Could not open log file at " + fileLoc)
        sys.exit()
    return


# print a message to the logger
# @toPrint = message to print to logger
def printToLogger(toPrint):
    with open(fileLoc, "a", encoding='utf-8') as f:
        f.write(toPrint)
        f.write("\n")


# add the CfgData to the logger
# @configData = CFGData to add
def addCfgData(configData):
    global cfgData
    cfgData = configData


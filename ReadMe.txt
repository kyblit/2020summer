A quick guide to using the apiDataFetcher

Required python packages:
	mysql.connector
	requests

To run the apiDataFetcher, locate the 'clientApiDataFetcher.py' file in the apiDataFetcher directory of the project, and run the method 'apiDataFetcher' in the terminal.

The 'apiDataFetcher' method takes 1 argument, the absolute path to your configuration file on *YOUR* machine. Reminder that python requires the string to be formatted in it's raw format (r"C:\Users\MakeSureToPut[r]InFront").

The format of the configuration file is shown in 'sampleINI.ini', located within the project folder.

[WordList]
contains the absolute filepath to the list of words to be searched as a .txt file. In this list of words, each word must be contained on it's own line. An example is placed within the 2020summer project folder as wordList.txt.

[SourceAccess]
contains all the information needed to access the API.
'SourceName' is purely an identifying field, and allows you to identify which source data arrived from.
'URL' is the API's url link that you are attempting to access. You must specifiy the entire query url, replaceing any fields that you would like to change with angle brackets surrounding the field's name. There are only 2 required fields, <key> and <word>, but you may add as many fields as you need by adding <[fieldname]> and then adding the field below in [SourceAccess]

example: https://www.thisIsApi.com/key=<key>/toSearch=<word>/language=<lang>/spaceing=<space>
key=123correcthorsebatterystaple
lang=english
space=5

'Parser' is the name of the parser that you wish to use. It must be stored in the parsers file of the 2020summer project file as a python file, and must only contain 1 method to parse the json object it recieves from the api. An example parser provided is googleCSE_res.py.


[MySqlAccess]
These fields contain the details to access the local MySql server, generated following the SQL script.

[Options]
Additional options that can be changed to modify program behavior
'expiration' time in days until data expires and will be fetched again

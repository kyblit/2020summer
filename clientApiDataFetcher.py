import sys
from apiDataFetcher import versatileGET
from apiDataFetcher import cfgReader
from apiDataFetcher import mySQLPythonConnection
import importlib
import mysql.connector
from mysql.connector import Error
from mysql.connector import pooling
import csv
import logging
from logging.config import fileConfig
from apiDataFetcher.mySQLPythonConnection import updateRaw


def apiDataFetcher(cfgFilePath):
    global parser, parsedArray

    #cfgData: [{"filepath"}, {"sourcename", "url", "key", "parser"}, {"db_name", "user", "password", "host"}, {"expiration"}]
    cfgData = cfgReader.cfgParse(cfgFilePath)
    #open logger
    fileConfig("loggerConfig.ini")
    logger = logging.getLogger(__name__)
    logger.info("Parsing the config file")

    # import the parser specified in the CFG file
    parserName = cfgData[1]["parser"]
    fullParserPath = "parsers." + parserName
    logger.info("Importing the parser from " + fullParserPath)
    try:
        parser = importlib.import_module(fullParserPath)
    except ImportError:
        logger.critical("Could not import the parser from " + fullParserPath)
        sys.exit()
    logger.info("Parser imported without errors")
    # open connection pool
    db_name = cfgData[2]["db_name"]
    db_user = cfgData[2]["user"]
    db_pass = cfgData[2]["password"]
    db_host = cfgData[2]["host"]
    try:
        #initalize connection pool so we don't open a new connection for every query
        connection_pool = mysql.connector.pooling.MySQLConnectionPool(pool_name="dataFetcherPool",
                                                                      pool_size=1,
                                                                      pool_reset_session=True,
                                                                      host=db_host,
                                                                      database=db_name,
                                                                      user=db_user,
                                                                      password=db_pass)
        logger.info("Connection pool " + connection_pool.pool_name + " with pool size " + str(connection_pool.pool_size))
        connection_object = connection_pool.get_connection()
        #call and insert data
        if connection_object.is_connected():
            logger.info("Connecting to database with pool on server version " + connection_object.get_server_info())
            for word in cfgData[0]:
                rawData = []
                logger.info("Begin fetching data for '" + word + "'")
                logger.info("Running versGET for '" + word + "' with configurations " + str(cfgData))
                # gets IDs of object and url. If they don't exist a [] is returned
                # checkArray: [word, urlID, objID] this is for insertion
                checkArray = [word]
                url = cfgData[1]["url"]
                urlID = mySQLPythonConnection.getEntry("ky_source", "source_url", url, connection_object)
                checkArray.append(urlID)
                objID = mySQLPythonConnection.getEntry("ky_object", "name", word, connection_object)
                checkArray.append(objID)
                logger.info("Passing checkArray with " + str(checkArray) + " for insertion")

                # checks if object and url exist. If they do, check to see if the data has expired.
                grabData = False
                updateData = False
                if objID and urlID:
                    # we need to check if the 2 ids have a corresponding row in the ky_raw_data table
                    matchArr = mySQLPythonConnection.filteredTableGet("ky_raw_data", "object_id", objID, connection_object)
                    # iterate through returned entries from ky_raw_data
                    for i in range(len(matchArr)):
                        if matchArr[i][0] == urlID:
                            #if entry exists and has expired update after making another api call
                            if mySQLPythonConnection.checkExpiration(cfgData, word, connection_object):
                                grabData = True
                                updateData = True
                            else:
                                #if data hasn't expired get raw data from db and pass to parser
                                logger.info("Data for '" + word + "' has not expired yet. Getting raw data from database.")
                                rawData.append(matchArr[i][2])
                                logger.info("Raw data from db: " + str(rawData))
                            break
                    else:
                        grabData = True
                else:
                    grabData = True
                if grabData:
                    logger.info("Getting data for '" + word + "' from api call. ")
                    #rawData format [rawData, api call status code]
                    rawData = versatileGET.versGET(cfgData[1], word)
                    logger.info("Raw data array: " + str(rawData))

                    # basic error handling, apiStatusCodeHandling will print to console and log
                    checkError = versatileGET.apiStatusCodeHandling(rawData[1], word, cfgData)
                    if checkError > 0:
                        # non critical error, skip current word and keep going
                        continue
                    elif checkError == 0:
                        # very critical error, stop the program
                        logger.error("Major error with the word " + word + ", skipping parsing and logging")
                        break
                    logger.info("saving raw data for the '" + word + "'")
                    versatileGET.jsonRaw(rawData[0], word, cfgData)
                    logger.info("parsing data for '" + word + "'")
                    #insert raw data
                    #TODO: raw data is converted to a string in mySQLPythonConnection.tableIns and assumed json, need to account for other types
                    #if entry exists update it. if not insert new entry
                    if updateData:
                        updateRaw(checkArray, rawData[0], connection_object)
                    else:
                        checkArray = mySQLPythonConnection.insertRaw(cfgData, rawData[0], checkArray, connection_object)
                        logger.info("Updated checkArray: " + str(checkArray))
                        logger.info("Raw data for '" + word + "' has been inserted. ")
                # parsed data insertion
                if parser:
                    #sends raw data string directly to parser
                    parsedDict = parser.parser(rawData[0])
                    logger.info("Parsed dictionary: " + str(parsedDict))
                    # tableIns takes in a dictionary with the format [{relation: [list of related words]}]
                    mySQLPythonConnection.tableIns(parsedDict, checkArray, connection_object)
                    logger.info("Parsed data for '" + word + "' has been inserted. ")

                '''# temporary code for 8/9 to 8/16 sprint
                fileName = f"{i}_{cfgData[1]['sourcename']}.csv"
                with open(fileName, "w+", encoding='utf-8') as my_csv:
                    csvWriter = csv.writer(my_csv, delimiter=',')
                    csvWriter.writerows(parsedDict)
                # \temporary code for 8/9 to 8/16 sprint'''
            logger.info("The program has finished running")
    except Error as e:
        logger.info("Error while connecting to mySQL using connection pool. ", e)
    finally:
        if (connection_object.is_connected()):
            logger.info("Connection has successfully closed. ")
    return

#specify directive in command line
apiDataFetcher(r'C:\Kyblit\2020summer\experiments\olivia\bht.ini')
#apiDataFetcher(sys.argv[0])
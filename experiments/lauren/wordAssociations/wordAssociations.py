import requests
import json
words= ['africa', 'air', 'angel', 'antarctica', 'back', 'ball', 'band', 'bark', 'beat', 'bed', 'beijing', 'bermuda', 'bill', 'block', 
'bomb', 'buck', 'bug', 'capital', 'car', 'card', 'carrot', 'cast', 'chick', 'china', 'church', 'cook', 'cross', 'cycle', 'czech', 
'doctor', 'dog', 'dragon', 'dress', 'engine', 'eye', 'face', 'field', 'film', 'fly', 'force', 'ground', 'horseshoe', 'hospital', 
'hotel', 'ice', 'iron', 'jam', 'kangaroo', 'key', 'lead', 'leprechaun', 'life', 'limousine', 'march', 'mass', 'mole', 'mommoth', 
'moscow', 'mug', 'ninja', 'nurse', 'oil', 'opera', 'orange', 'palm', 'pants', 'paper', 'phoenix', 'plastic', 'port', 'pupil', 'robin',
 'root', 'scorpion', 'server', 'shoe', 'shop', 'slug', 'snow', 'snowman', 'sock', 'soldier', 'soul', 'spell', 'spider', 'spy', 'stream',
  'string', 'switch', 'tablet', 'tail', 'teacher', 'teacher', 'theater', 'tie', 'tower', 'turkey', 'vet', 'war', 'wave']
finalData=None



def write_json(data, filename='wordData.json'): 
    with open(filename,'w') as f: 
        json.dump(data, f, indent=4) 

for x in range(0,len(words),10):
	parameters = {'apikey': 'c0ff9acd-a001-45b3-b876-fe676b0b673c', 'text': words[x:x+10], 'lang': 'en'}
	response=requests.get('https://api.wordassociations.net/associations/v1.0/json/search', params=parameters)
	if(finalData ==None):
		finalData=json.loads(response.text)
	else:
		temp=finalData["response"]
		temp.append(json.loads(response.text)["response"])

write_json(finalData)
# Data Sets
### LA Arrest Data from 2010-2019
- link: https://data.lacity.org/A-Safe-City/Arrest-Data-from-2010-to-2019/yru6-6re4
#### Description 
- This dataset reflects arrest incidents in the City of Los Angeles from 2010 to 2019.
- Columns of interest
    - Report type: Booking/Released
    - Arrest Date
    - Area ID: which LA police station made arrest
    - Age
    - Sex Code
    - Decent Code: Ethnicity
    - Charge Group Description: Category of arrest charge (Aggravated assault, Narcotic Drug Laws, etc)
    - Arrest Type Code:A code to indicate the type of charge the individual was arrested for.
    - Address (possibly)- street address of crime incident
---
### SFPD Incident Reports (2018-2020)
- link: https://data.sfgov.org/Public-Safety/Police-Department-Incident-Reports-2018-to-Present/wg3w-h783
#### Description 
- Mostly same information as the LA one. Includes neighborhood information. 
- Interest: Examine most common incident by age, race, and location. 
- Want to supplement this data with SF census data by neighboorhood (https://default.sfplanning.org/publications_reports/SF_NGBD_SocioEconomic_Profiles/2010-2014_ACS_Profile_Neighborhoods_v3AH.pdf_) 
    - see how incident demographics compare with actual population demographics

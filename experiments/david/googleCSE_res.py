import requests
import csv
import time
import json

apiKey = "AIzaSyBVYvyjg_UbG9iNSqk8lggjAF970nZybDA"
engineID = "003330174904582784281:ycdnjzifiqk"
# words = ['africa', 'teacher', 'leprechaun', 'jam', 'march']

words = ['africa', 'air', 'angel', 'antarctica', 'back', 'ball', 'band', 'bark', 'beat', 'bed', 'beijing', 'bermuda',
         'bill', 'block',
         'bomb', 'buck', 'bug', 'capital', 'car', 'card', 'carrot', 'cast', 'chick', 'china', 'church', 'cook', 'cross',
         'cycle', 'czech',
         'doctor', 'dog', 'dragon', 'dress', 'engine', 'eye', 'face', 'field', 'film', 'fly', 'force', 'ground',
         'horseshoe', 'hospital',
         'hotel', 'ice', 'iron', 'jam', 'kangaroo', 'key', 'lead', 'leprechaun', 'life', 'limousine', 'march', 'mass',
         'mole', 'mammoth',
         'moscow', 'mug', 'ninja', 'nurse', 'oil', 'opera', 'orange', 'palm', 'pants', 'paper', 'phoenix', 'plastic',
         'port', 'pupil', 'robin',
         'root', 'scorpion', 'server', 'shoe', 'shop', 'slug', 'snow', 'snowman', 'sock', 'soldier', 'soul', 'spell',
         'spider', 'spy', 'stream',
         'string', 'switch', 'tablet', 'tail', 'teacher', 'teacher', 'theater', 'tie', 'tower', 'turkey', 'vet', 'war',
         'wave']
words_ab = ['africa', 'air', 'angel', 'antarctica', 'back', 'ball', 'band', 'bark', 'beat', 'bed', 'beijing',
            'bermuda', 'bill', 'block', 'bomb', 'buck', 'bug']
csv_fields = ['Word', 'URL', 'Title', 'Snippet']
allRows = []
start = 1


def parser(toParse, word):
    se_results = toParse.get("items")
    start = 1
    allRows = []
    for i, se_results in enumerate(se_results, start):
        se_res_title = se_results.get("title")
        se_res_html_snippet = se_results.get("htmlSnippet")
        se_res_link = se_results.get("link")
        tempRow = [word, se_res_link, se_res_title, se_res_html_snippet]
        print(tempRow)
        allRows.append(tempRow)
    return allRows


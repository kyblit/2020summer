import flask

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['GET'])
def home():
    return "<h1>kyblit Summer 2020 Data API project landing page</h1>"


app.run()

import json
import requests

# key for thesaurus api
api_key = "49efd194-fe32-40ea-b2bf-e4503ec11435"
# thesaurus api url
url = 'https://www.dictionaryapi.com/api/v3/references/thesaurus/json/sporty?key=49efd194-fe32-40ea-b2bf-e4503ec11435'
goodreadsUrl = 'https://www.goodreads.com/book/isbn/0441172717?callback=myCallback&format=xml&key=pfGjmFZSwoYv8JzbDgmgaw'
r = requests.get(url)
# decodes the json data
data = r.json()
nData = data[0]

# str_conv = json.dumps(data)
# print(type(str_conv))

def parse(toParse):
    global word
    global syns
    global ants
    global definition
    global newData
    newData = toParse[0]
    word = newData.get('meta').get('id')
    syns = newData.get('meta').get('syns')[0]
    ants = newData.get('meta').get('ants')[0]
    definition = newData.get('shortdef')[0]
    # print(word)
    # print(syns)
    # print(ants)

    # arr = [[word], syns, ants]
    synsCopy = syns.copy()
    antsCopy = ants.copy()
    blankChar = ""
    numCols = 0
    if (len(syns) > len(ants)):
        numCols = len(syns)
    else:
        numCols = len(ants)
    rows, cols = (4, numCols)
    arr = []
    for i in range(cols):
        if (i == 0):
            col = ["word", "synonyms", "antonyms", "definition"]
        else:
            col = []
        for j in range(rows):
            if (i != 0):
                if (i == 1 and j == 0):
                    col.append(word)
                if (i == 1 and j == 3):
                    col.append(definition)
                if (i >= 2 and (j == 0 or j == 3)):
                    col.append(blankChar)
                if (j == 1):
                    if (len(synsCopy) != 0):
                        col.append(synsCopy[0])
                        del synsCopy[0]
                    else:
                        col.append(blankChar)
                if (j == 2):
                    if (len(antsCopy) != 0):
                        col.append(antsCopy[0])
                        del antsCopy[0]
                    else:
                        col.append(blankChar)
        arr.append(col)
    return arr

def apiResponse():
    return r.status_code

def getWord():
    parse(data)
    return word

def getSynonyms():
    parse(data)
    return syns

def getAntonyms():
    parse(data)
    return ants

def getDefinition():
    return definition

def printArray(arr):
    for row in arr:
        print(row)

def main():
    printArray(parse(newData))

if __name__ == '__main__':
    main()

# code for accessing data as list
# word = data[0]['meta']['id']
# print("word: " + word)
# print("stems: ")
# stems = data[0]['meta']['stems']
# print(stems)
# print("synonyms: ")
# synonyms = data[0]['meta']['syns'][0]
# print(synonyms)
# print("antonyms: ")
# antonyms = data[0]['meta']['ants'][0]
# print(antonyms)
# print("short definition: ")
# shortDef = data[0]['shortdef']
# print(shortDef)


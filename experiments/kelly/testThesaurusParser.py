import unittest
import thesaurusParser


class testThesaurus(unittest.TestCase):
    parser = thesaurusParser

    def test_response(self):
        print("Testing response code: ")
        assert (True == True)
        self.assertEqual(self.parser.apiResponse(), 200)
        print('Response code is' + str(self.parser.apiResponse()))

    def test_get_word(self):
        word = self.parser.getWord()
        self.assertEqual(word, "sporty")

    def test_get_syns(self):
        syns = self.parser.getSynonyms()
        self.assertEqual(syns,
                         ['dégagé', 'relaxed', 'casual', 'everyday', 'informal', 'workaday', 'dressed down', 'shabby',
                          'sloppy', 'slovenly', 'unkempt'])

    def test_get_ants(self):
        ants = self.parser.getAntonyms()
        self.assertEqual(ants, ['dressy', 'formal', 'noncasual', 'best', 'Sunday', 'dressed up', 'chic', 'elegant',
                                'fashionable', 'smart', 'stylish', 'neat', 'tidy', 'trim', 'semiformal'])


if __name__ == '__main__':
    unittest.main()


{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - tie  synonyms",
                "totalResults": "21800000",
                "searchTerms": "tie  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - tie  synonyms",
                "totalResults": "21800000",
                "searchTerms": "tie  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.351751,
        "formattedSearchTime": "0.35",
        "totalResults": "21800000",
        "formattedTotalResults": "21,800,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Tie Synonyms, Tie Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Tie Synonyms</b>, Tie Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/tie",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · connection · link · fetter · gag · ligature · network · strap · tie-in.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; connection &middot; link &middot; fetter &middot; gag &middot; ligature &middot; network &middot; strap &middot; <b>tie</b>-in.",
            "cacheId": "47gNxcBzqLMJ",
            "formattedUrl": "https://www.thesaurus.com/browse/tie",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>tie</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of tie | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for tie from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Tie Synonyms, Tie Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Tie Synonyms</b>, Tie Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/tie",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms & Antonyms of tie. (Entry 1 of 2). 1 to gather into a tight mass by means \nof a ...",
            "htmlSnippet": "<b>Synonyms</b> &amp; Antonyms of <b>tie</b>. (Entry 1 of 2). 1 to gather into a tight mass by means <br>\nof a&nbsp;...",
            "cacheId": "1ICibLGv2XwJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/tie",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>tie</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for TIE",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/tie",
                        "og:title": "Thesaurus results for TIE",
                        "twitter:aria-text": "Share more words for tie on Twitter",
                        "og:aria-text": "Post more words for tie to Facebook",
                        "og:description": "Tie: to gather into a tight mass by means of a line or cord. Synonyms: band, bind, truss… Antonyms: unbind, untie… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Tie: to gather into a tight mass by means of a line or cord. Synonyms: band, bind, truss… Antonyms: unbind, untie… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/tie"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Tie synonyms | Best 133 synonyms for tie",
            "htmlTitle": "<b>Tie synonyms</b> | Best 133 synonyms for tie",
            "link": "https://thesaurus.yourdictionary.com/tie",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 133 synonyms for tie, including: affinity, fall-behind, band, bond, strap, \nlace, bandage, brace, tackle, zipper, yoke and more... Find another word for tie at\n ...",
            "htmlSnippet": "The best 133 <b>synonyms</b> for <b>tie</b>, including: affinity, fall-behind, band, bond, strap, <br>\nlace, bandage, brace, tackle, zipper, yoke and more... Find another word for <b>tie</b> at<br>\n&nbsp;...",
            "cacheId": "Cs07wgp5i7MJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/tie",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>tie</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Tie game Synonyms, Tie game Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Tie</b> game <b>Synonyms</b>, <b>Tie</b> game Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/tie%20game",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for tie game at Thesaurus.com with free online thesaurus, antonyms, \nand definitions. Find descriptive alternatives for tie game.",
            "htmlSnippet": "<b>Synonyms</b> for <b>tie</b> game at Thesaurus.com with free online thesaurus, antonyms, <br>\nand definitions. Find descriptive alternatives for <b>tie</b> game.",
            "cacheId": "ezpAujUgvEIJ",
            "formattedUrl": "https://www.thesaurus.com/browse/tie%20game",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>tie</b>%20game",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of tie game | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for tie game from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for tie? | Tie Synonyms - WordHippo Thesaurus",
            "htmlTitle": "What is another word for tie? | <b>Tie Synonyms</b> - WordHippo Thesaurus",
            "link": "https://www.wordhippo.com/what-is/another-word-for/tie.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 7500 synonyms for tie and other similar words that you can use instead \nbased on 39 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 7500 <b>synonyms</b> for <b>tie</b> and other similar words that you can use instead <br>\nbased on 39 separate contexts from our thesaurus.",
            "cacheId": "ZJMeWGG8Ec0J",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/tie.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>tie</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Tie Synonyms, Tie Antonyms: Page 4 of 133 | Thesaurus.com",
            "htmlTitle": "<b>Tie Synonyms</b>, Tie Antonyms: Page 4 of 133 | Thesaurus.com",
            "link": "http://www.thesaurus.com/browse/tie/4",
            "displayLink": "www.thesaurus.com",
            "snippet": "Strong as is the tie of interest, it has been often found ineffectual. She called for \nEileen, told her to tie on her sunshade and be ready for a short ride. As an honest\n ...",
            "htmlSnippet": "Strong as is the <b>tie</b> of interest, it has been often found ineffectual. She called for <br>\nEileen, told her to <b>tie</b> on her sunshade and be ready for a short ride. As an honest<br>\n&nbsp;...",
            "cacheId": "dfcdZ9-bz1gJ",
            "formattedUrl": "www.thesaurus.com/browse/tie/4",
            "htmlFormattedUrl": "www.thesaurus.com/browse/<b>tie</b>/4",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "og:title": "Synonyms of tie | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for tie from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Tie Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Tie Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/tie",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for tie: fasten, bind, join, unite, link | Collins English Thesaurus.",
            "htmlSnippet": "Another word for <b>tie</b>: fasten, bind, join, unite, link | Collins English Thesaurus.",
            "cacheId": "HXOvij1u9dUJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/tie",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/<b>tie</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Tie Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "2a932609-1cbd-4ba8-9a4c-13ee19102fb8",
                        "_csrf_parameter": "_csrf",
                        "title": "Tie Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for tie: fasten, bind, join, unite, link | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/tie"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Tie | Synonyms of Tie by Lexico",
            "htmlTitle": "<b>Tie</b> | <b>Synonyms</b> of <b>Tie</b> by Lexico",
            "link": "https://www.lexico.com/synonym/tie",
            "displayLink": "www.lexico.com",
            "snippet": "Phrases · 1'Gabriel tied up his pony' SYNONYMS bind, tie, tether, hitch, strap, \ntruss, fetter, rope, chain, make fast, moor, lash, attach, fasten, fix, secure · 2'do not \ntie ...",
            "htmlSnippet": "Phrases &middot; 1&#39;Gabriel <b>tied</b> up his pony&#39; <b>SYNONYMS</b> bind, <b>tie</b>, tether, hitch, strap, <br>\ntruss, fetter, rope, chain, make fast, moor, lash, attach, fasten, fix, secure &middot; 2&#39;do not <br>\n<b>tie</b>&nbsp;...",
            "cacheId": "-m_OlqYJ6GIJ",
            "formattedUrl": "https://www.lexico.com/synonym/tie",
            "htmlFormattedUrl": "https://www.lexico.com/<b>synonym</b>/<b>tie</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT-fHqiHsELFzBLFkvhF3MUNoGyDc02pwOeICq9JD-vhUo5TV_0a-_rYOwh",
                        "width": "291",
                        "height": "173"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#50b46c",
                        "og:image": "https://www.lexico.com/lexico-logo.png",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Tie | Synonyms of Tie by Lexico",
                        "og:type": "website",
                        "theme-color": "#50b46c",
                        "og:site_name": "Lexico Dictionaries | English",
                        "og:title": "Tie | Synonyms of Tie by Lexico",
                        "msapplication-tileimage": "/mstile-144x144.png",
                        "csrf-param": "authenticity_token",
                        "og:description": "What is the definition of tie? What is the meaning of tie? How do you use tie in a sentence? What are synonyms for tie?",
                        "twitter:image": "https://www.lexico.com/lexico-logo.png",
                        "viewport": "width=device-width, initial-scale=1, shrink-to-fit=no",
                        "twitter:description": "What is the definition of tie? What is the meaning of tie? How do you use tie in a sentence? What are synonyms for tie?",
                        "csrf-token": "gTfy+5fgDPovdJBnR9LW99xe5UFbPq28rKKUuLMUeL3rpdHSkfxzKBN0AQYe2lilkktLE7CkKltE9t1hBspXRQ==",
                        "og:url": "https://www.lexico.com/synonym/tie"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.lexico.com/lexico-logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "tie synonyms with definition | Macmillan Thesaurus",
            "htmlTitle": "<b>tie synonyms</b> with definition | Macmillan Thesaurus",
            "link": "https://www.macmillanthesaurus.com/tie",
            "displayLink": "www.macmillanthesaurus.com",
            "snippet": "Synonyms for 'tie': tie up, buckle, do up, splice, chain, wedge.",
            "htmlSnippet": "<b>Synonyms</b> for &#39;<b>tie</b>&#39;: <b>tie</b> up, buckle, do up, splice, chain, wedge.",
            "cacheId": "Ph_SSi5HyCAJ",
            "formattedUrl": "https://www.macmillanthesaurus.com/tie",
            "htmlFormattedUrl": "https://www.macmillanthesaurus.com/<b>tie</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTvi2fKGyVfH8J_NZJPZvttg3WD5erxxyfmRQT9xohcKNV8Oa4QTCKfDg",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/external/images/logoThesaurus.png?version=1.0.46",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no",
                        "og:title": "tie synonyms with definition | Macmillan Thesaurus",
                        "title": "tie synonyms with definition | Macmillan Thesaurus",
                        "og:url": "https://www.macmillanthesaurus.com/tie",
                        "og:description": "Synonyms for 'tie': tie up, buckle, do up, splice, chain, wedge"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillanthesaurus.com/external/images/bg-wave.svg?version=1.0.46"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Railroad Tie Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "Railroad <b>Tie Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/railroad+tie",
            "displayLink": "www.synonyms.com",
            "snippet": "Princeton's WordNet(0.00 / 0 votes)Rate these synonyms: tie, railroad tie, crosstie\n, sleeper(noun). one of the cross braces that support the rails on a railway track.",
            "htmlSnippet": "Princeton&#39;s WordNet(0.00 / 0 votes)Rate these <b>synonyms</b>: <b>tie</b>, railroad <b>tie</b>, crosstie<br>\n, sleeper(noun). one of the cross braces that support the rails on a railway track.",
            "cacheId": "YbN8Mh8LgB8J",
            "formattedUrl": "https://www.synonyms.com/synonym/railroad+tie",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/railroad+<b>tie</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/railroad+tie",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        }
    ]
}
{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - nurse  synonyms",
                "totalResults": "6450000",
                "searchTerms": "nurse  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - nurse  synonyms",
                "totalResults": "6450000",
                "searchTerms": "nurse  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.285003,
        "formattedSearchTime": "0.29",
        "totalResults": "6450000",
        "formattedTotalResults": "6,450,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Nurse Synonyms, Nurse Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Nurse Synonyms</b>, Nurse Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/nurse",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · assistant · attendant · medic · registered nurse · therapist · RN · \ncaretaker · sitter.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; assistant &middot; attendant &middot; medic &middot; registered <b>nurse</b> &middot; therapist &middot; RN &middot; <br>\ncaretaker &middot; sitter.",
            "cacheId": "mvfJJwOFa0cJ",
            "formattedUrl": "https://www.thesaurus.com/browse/nurse",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>nurse</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of nurse | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for nurse from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Nurse Synonyms, Nurse Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Nurse Synonyms</b>, Nurse Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/nurse",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms of nurse · 1 to attend to the needs and comforts of · 2 to give milk to \nfrom the breast · 3 to keep in one's mind or heart · 4 to treat with great or \nexcessive care.",
            "htmlSnippet": "<b>Synonyms</b> of <b>nurse</b> &middot; 1 to attend to the needs and comforts of &middot; 2 to give milk to <br>\nfrom the breast &middot; 3 to keep in one&#39;s mind or heart &middot; 4 to treat with great or <br>\nexcessive care.",
            "cacheId": "Y1mbtT1VAtcJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/nurse",
            "htmlFormattedUrl": "https://www.merriam-webster.com/<b>thesaurus</b>/<b>nurse</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for NURSE",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/nurse",
                        "og:title": "Thesaurus results for NURSE",
                        "twitter:aria-text": "Share more words for nurse on Twitter",
                        "og:aria-text": "Post more words for nurse to Facebook",
                        "og:description": "Nurse: a person employed to care for a young child or children. Synonyms: babysitter, dry nurse, nanny… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Nurse: a person employed to care for a young child or children. Synonyms: babysitter, dry nurse, nanny… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/nurse"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Nursing Synonyms, Nursing Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Nursing Synonyms</b>, Nursing Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/nursing",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for nursing · attending · cherishing · fostering · tending · caring for · \nwatching over.",
            "htmlSnippet": "<b>Synonyms</b> for <b>nursing</b> &middot; attending &middot; cherishing &middot; fostering &middot; tending &middot; caring for &middot; <br>\nwatching over.",
            "cacheId": "Y1QvWNghMu4J",
            "formattedUrl": "https://www.thesaurus.com/browse/nursing",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>nursing</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of nursing | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for nursing from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Nurse synonyms | Best 78 synonyms for nurse",
            "htmlTitle": "<b>Nurse synonyms</b> | Best 78 synonyms for nurse",
            "link": "https://thesaurus.yourdictionary.com/nurse",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 78 synonyms for nurse, including: immunize, attendant, male nurse, \npractical-nurse, orderly, health care provider, be a father to, licensed practical ...",
            "htmlSnippet": "The best 78 <b>synonyms</b> for <b>nurse</b>, including: immunize, attendant, male <b>nurse</b>, <br>\npractical-<b>nurse</b>, orderly, health care provider, be a father to, licensed practical&nbsp;...",
            "cacheId": "wQVe_9A_Y_QJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/nurse",
            "htmlFormattedUrl": "https://<b>thesaurus</b>.yourdictionary.com/<b>nurse</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for nurse? | Nurse Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for nurse? | <b>Nurse Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/nurse.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 3112 synonyms for nurse and other similar words that you can use instead \nbased on 21 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 3112 <b>synonyms</b> for <b>nurse</b> and other similar words that you can use instead <br>\nbased on 21 separate contexts from our <b>thesaurus</b>.",
            "cacheId": "mJF2mBGbMXgJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/nurse.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>nurse</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "1 632 Nurse Synonyms and 85 Nurse Antonyms | Nurse in Thesaurus",
            "htmlTitle": "1 632 <b>Nurse Synonyms</b> and 85 Nurse Antonyms | Nurse in Thesaurus",
            "link": "https://www.powerthesaurus.org/nurse",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Synonyms (Other Words) for Nurse & Antonyms (Opposite Meaning) for Nurse.",
            "htmlSnippet": "<b>Synonyms</b> (Other Words) for <b>Nurse</b> &amp; Antonyms (Opposite Meaning) for <b>Nurse</b>.",
            "cacheId": "Y8j7Dt0N4d4J",
            "formattedUrl": "https://www.powerthesaurus.org/nurse",
            "htmlFormattedUrl": "https://www.power<b>thesaurus</b>.org/<b>nurse</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "1 632 Nurse Synonyms and 85 Nurse Antonyms | Nurse in Thesaurus",
                        "og:description": "Synonyms (Other Words) for Nurse & Antonyms (Opposite Meaning) for Nurse.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/nurse",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Nurse Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Nurse Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/nurse",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for nurse: carer, caregiver, angel | Collins English Thesaurus. ... \nPatients are dying because of an acute shortage of nurses. Synonyms. carer.",
            "htmlSnippet": "Another word for <b>nurse</b>: carer, caregiver, angel | Collins English <b>Thesaurus</b>. ... <br>\nPatients are dying because of an acute shortage of <b>nurses</b>. <b>Synonyms</b>. carer.",
            "cacheId": "H4mPlwxZFWAJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/nurse",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-<b>thesaurus</b>/<b>nurse</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Nurse Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "e8b5b994-de10-4242-b5cd-bc28a3e73ce4",
                        "_csrf_parameter": "_csrf",
                        "title": "Nurse Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for nurse: carer, caregiver, angel | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/nurse"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for nurse | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>nurse</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/nurse",
            "displayLink": "www.synonym.com",
            "snippet": "1. nurse · 2. foster-nurse · 3. wet-nurse · 4. nurse-midwife · 5. nurse · 6. nurse · 7. \nnurse · 8. nurse.",
            "htmlSnippet": "1. <b>nurse</b> &middot; 2. foster-<b>nurse</b> &middot; 3. wet-<b>nurse</b> &middot; 4. <b>nurse</b>-midwife &middot; 5. <b>nurse</b> &middot; 6. <b>nurse</b> &middot; 7. <br>\n<b>nurse</b> &middot; 8. <b>nurse</b>.",
            "cacheId": "gfu8xcnVR-oJ",
            "formattedUrl": "https://www.synonym.com/synonyms/nurse",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>nurse</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSNPaQwyj2t_vB9GfyNeHSwmvM09gteOLaZBnBpO4dPbZHGXuZcGOBvQKFb",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for nurse",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for nurse",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "nurse | definition: try to cure by special care of treatment, of an illness or injury | synonyms: care for, treat| antonyms: breastfeed, disinherit, stay",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "nurse | definition: try to cure by special care of treatment, of an illness or injury | synonyms: care for, treat| antonyms: breastfeed, disinherit, stay",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/nurse"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/dist/img/wordtigo-logo.987687df.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Nurse Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Nurse Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/nurse",
            "displayLink": "www.synonyms.com",
            "snippet": "Princeton's WordNet(0.00 / 0 votes)Rate these synonyms: · nurse(noun) · nanny, \nnursemaid, nurse(verb) · nurse(verb) · harbor, harbour, hold, entertain, nurse(\nverb).",
            "htmlSnippet": "Princeton&#39;s WordNet(0.00 / 0 votes)Rate these <b>synonyms</b>: &middot; <b>nurse</b>(noun) &middot; nanny, <br>\nnursemaid, <b>nurse</b>(verb) &middot; <b>nurse</b>(verb) &middot; harbor, harbour, hold, entertain, <b>nurse</b>(<br>\nverb).",
            "cacheId": "ZeLHhJQ8iMAJ",
            "formattedUrl": "https://www.synonyms.com/synonym/nurse",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>nurse</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/nurse",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "nurse synonyms with definition | Macmillan Thesaurus",
            "htmlTitle": "<b>nurse synonyms</b> with definition | Macmillan Thesaurus",
            "link": "https://www.macmillanthesaurus.com/nurse",
            "displayLink": "www.macmillanthesaurus.com",
            "snippet": "Related terms for 'nurse': charge nurse, district nurse, health visitor, matron, \nmidwife, nurse practitioner, registered nurse, RN, sister, Sister.",
            "htmlSnippet": "Related terms for &#39;<b>nurse</b>&#39;: charge <b>nurse</b>, district <b>nurse</b>, health visitor, matron, <br>\nmidwife, <b>nurse</b> practitioner, registered <b>nurse</b>, RN, sister, Sister.",
            "cacheId": "SFfxVJ51AnAJ",
            "formattedUrl": "https://www.macmillanthesaurus.com/nurse",
            "htmlFormattedUrl": "https://www.macmillan<b>thesaurus</b>.com/<b>nurse</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTvi2fKGyVfH8J_NZJPZvttg3WD5erxxyfmRQT9xohcKNV8Oa4QTCKfDg",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/external/images/logoThesaurus.png?version=1.0.46",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no",
                        "og:title": "nurse synonyms with definition | Macmillan Thesaurus",
                        "title": "nurse synonyms with definition | Macmillan Thesaurus",
                        "og:url": "https://www.macmillanthesaurus.com/nurse",
                        "og:description": "Related terms for 'nurse': charge nurse, district nurse, health visitor, matron, midwife, nurse practitioner, registered nurse, RN, sister, Sister"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillanthesaurus.com/external/images/bg-wave.svg?version=1.0.46"
                    }
                ]
            }
        }
    ]
}
{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - force  synonyms",
                "totalResults": "90500000",
                "searchTerms": "force  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - force  synonyms",
                "totalResults": "90500000",
                "searchTerms": "force  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.245397,
        "formattedSearchTime": "0.25",
        "totalResults": "90500000",
        "formattedTotalResults": "90,500,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Force Synonyms, Force Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Force Synonyms</b>, Force Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/force",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · effort · strength · violence · clout · enforcement · fury · speed · \nstrain.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; effort &middot; strength &middot; violence &middot; clout &middot; enforcement &middot; fury &middot; speed &middot; <br>\nstrain.",
            "cacheId": "yy4tgMGDCMQJ",
            "formattedUrl": "https://www.thesaurus.com/browse/force",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>force</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of force | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for force from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Force Synonyms, Force Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Force Synonyms</b>, Force Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/force",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms & Antonyms of force. (Entry 1 of 2). 1 to cause (a person) to give in to ...",
            "htmlSnippet": "<b>Synonyms</b> &amp; Antonyms of <b>force</b>. (Entry 1 of 2). 1 to cause (a person) to give in to&nbsp;...",
            "cacheId": "dR106YUyhecJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/force",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>force</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for FORCE",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/force",
                        "og:title": "Thesaurus results for FORCE",
                        "twitter:aria-text": "Share more words for force on Twitter",
                        "og:aria-text": "Post more words for force to Facebook",
                        "og:description": "Force: to cause (a person) to give in to pressure. Synonyms: blackjack, coerce, compel… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Force: to cause (a person) to give in to pressure. Synonyms: blackjack, coerce, compel… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/force"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Forces Synonyms, Forces Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Forces Synonyms</b>, Forces Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/forces",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · effort · strength · violence · clout · enforcement · fury · speed · \nstrain.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; effort &middot; strength &middot; violence &middot; clout &middot; enforcement &middot; fury &middot; speed &middot; <br>\nstrain.",
            "cacheId": "IISDC-yrUW4J",
            "formattedUrl": "https://www.thesaurus.com/browse/forces",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>forces</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of forces | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for forces from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Force synonyms | Best 258 synonyms for force",
            "htmlTitle": "<b>Force synonyms</b> | Best 258 synonyms for force",
            "link": "https://thesaurus.yourdictionary.com/force",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 258 synonyms for force, including: press, persuade, might, power, \ndecree, forcefulness, willpower, pin-down, strength, compulsion, duress and \nmore.",
            "htmlSnippet": "The best 258 <b>synonyms</b> for <b>force</b>, including: press, persuade, might, power, <br>\ndecree, forcefulness, willpower, pin-down, strength, compulsion, duress and <br>\nmore.",
            "cacheId": "iKIIukdkiJUJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/force",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>force</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for force? | Force Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for force? | <b>Force Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/force.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 14599 synonyms for force and other similar words that you can use instead \nbased on 69 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 14599 <b>synonyms</b> for <b>force</b> and other similar words that you can use instead <br>\nbased on 69 separate contexts from our thesaurus.",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/force.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>force</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for force | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>force</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/force",
            "displayLink": "www.synonym.com",
            "snippet": "1. force · 2. force · 3. force · 4. force · 5. force · 6. force · 7. force · 8. force.",
            "htmlSnippet": "1. <b>force</b> &middot; 2. <b>force</b> &middot; 3. <b>force</b> &middot; 4. <b>force</b> &middot; 5. <b>force</b> &middot; 6. <b>force</b> &middot; 7. <b>force</b> &middot; 8. <b>force</b>.",
            "cacheId": "KJzchFW-A6kJ",
            "formattedUrl": "https://www.synonym.com/synonyms/force",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>force</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for force",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for force",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "force | definition: to cause to do through pressure or necessity, by physical, moral or intellectual means :",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "force | definition: to cause to do through pressure or necessity, by physical, moral or intellectual means :",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/force"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "5 813 Force synonyms - Other Words for Force",
            "htmlTitle": "5 813 <b>Force synonyms</b> - Other Words for Force",
            "link": "https://www.powerthesaurus.org/force/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Force synonyms. Top synonyms for force (other words for force) are power, push \nand strength.",
            "htmlSnippet": "<b>Force synonyms</b>. Top synonyms for force (other words for force) are power, push <br>\nand strength.",
            "cacheId": "5rN0FO8IxEoJ",
            "formattedUrl": "https://www.powerthesaurus.org/force/synonyms",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>force</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "5 813 Force synonyms - Other Words for Force",
                        "og:description": "Force synonyms. Top synonyms for force (other words for force) are power, push and strength.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/force/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Force | Synonyms of Force by Lexico",
            "htmlTitle": "<b>Force</b> | <b>Synonyms</b> of <b>Force</b> by Lexico",
            "link": "https://www.lexico.com/synonym/force",
            "displayLink": "www.lexico.com",
            "snippet": "verb · 1'the raiders forced him to open the safe' SYNONYMS compel, coerce, \nmake, constrain, oblige, impel, drive, necessitate, pressurize, pressure, press, \npush.",
            "htmlSnippet": "verb &middot; 1&#39;the raiders forced him to open the safe&#39; <b>SYNONYMS</b> compel, coerce, <br>\nmake, constrain, oblige, impel, drive, necessitate, pressurize, pressure, press, <br>\npush.",
            "cacheId": "v-TyIXUAjCkJ",
            "formattedUrl": "https://www.lexico.com/synonym/force",
            "htmlFormattedUrl": "https://www.lexico.com/<b>synonym</b>/<b>force</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT-fHqiHsELFzBLFkvhF3MUNoGyDc02pwOeICq9JD-vhUo5TV_0a-_rYOwh",
                        "width": "291",
                        "height": "173"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#50b46c",
                        "og:image": "https://www.lexico.com/lexico-logo.png",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Force | Synonyms of Force by Lexico",
                        "og:type": "website",
                        "theme-color": "#50b46c",
                        "og:site_name": "Lexico Dictionaries | English",
                        "og:title": "Force | Synonyms of Force by Lexico",
                        "msapplication-tileimage": "/mstile-144x144.png",
                        "csrf-param": "authenticity_token",
                        "og:description": "What is the definition of force? What is the meaning of force? How do you use force in a sentence? What are synonyms for force?",
                        "twitter:image": "https://www.lexico.com/lexico-logo.png",
                        "viewport": "width=device-width, initial-scale=1, shrink-to-fit=no",
                        "twitter:description": "What is the definition of force? What is the meaning of force? How do you use force in a sentence? What are synonyms for force?",
                        "csrf-token": "hqEsXfVN5RoBMhE3qQVeKKXtBSleIWwirClc2v7IlduX6V/wDIcQHHYCa4LGGOVnntHwnJ+bRYyUQDeAlzNOnQ==",
                        "og:url": "https://www.lexico.com/synonym/force"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.lexico.com/lexico-logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Defense forces Synonyms, Defense forces Antonyms | Thesaurus.com",
            "htmlTitle": "Defense <b>forces Synonyms</b>, Defense forces Antonyms | Thesaurus.com",
            "link": "http://thesaurus.com/browse/defense+force?o=100074",
            "displayLink": "thesaurus.com",
            "snippet": "Synonyms for defense forces at Thesaurus.com with free online thesaurus, \nantonyms, and definitions. Find descriptive alternatives for defense forces.",
            "htmlSnippet": "<b>Synonyms</b> for defense <b>forces</b> at Thesaurus.com with free online thesaurus, <br>\nantonyms, and definitions. Find descriptive alternatives for defense <b>forces</b>.",
            "cacheId": "0xujuxgIAl8J",
            "formattedUrl": "thesaurus.com/browse/defense+force?o=100074",
            "htmlFormattedUrl": "thesaurus.com/browse/defense+<b>force</b>?o=100074",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "og:title": "Synonyms of defense forces | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for defense forces from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Force Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Force Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/force",
            "displayLink": "www.synonyms.com",
            "snippet": "Apr 2, 2020 ... Princeton's WordNet(1.00 / 1 vote)Rate these synonyms: · force(noun) a powerful \neffect or influence · force(noun) · force, forcefulness, strength( ...",
            "htmlSnippet": "Apr 2, 2020 <b>...</b> Princeton&#39;s WordNet(1.00 / 1 vote)Rate these <b>synonyms</b>: &middot; <b>force</b>(noun) a powerful <br>\neffect or influence &middot; <b>force</b>(noun) &middot; <b>force</b>, forcefulness, strength(&nbsp;...",
            "cacheId": "pD02ghArv_gJ",
            "formattedUrl": "https://www.synonyms.com/synonym/force",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>force</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/force",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        }
    ]
}
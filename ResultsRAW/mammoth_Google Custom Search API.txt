{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - mammoth  synonyms",
                "totalResults": "941000",
                "searchTerms": "mammoth  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - mammoth  synonyms",
                "totalResults": "941000",
                "searchTerms": "mammoth  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.19973,
        "formattedSearchTime": "0.20",
        "totalResults": "941000",
        "formattedTotalResults": "941,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Mammoth Synonyms, Mammoth Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Mammoth Synonyms</b>, Mammoth Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/mammoth",
            "displayLink": "www.thesaurus.com",
            "snippet": "RELATED WORDS AND SYNONYMS FOR MAMMOTH. Brobdingnagian. \nadjectivegiant. Bunyanesque · behemoth · colossal ...",
            "htmlSnippet": "RELATED WORDS AND <b>SYNONYMS</b> FOR <b>MAMMOTH</b>. Brobdingnagian. <br>\nadjectivegiant. Bunyanesque &middot; behemoth &middot; colossal&nbsp;...",
            "cacheId": "0sq-LP2AuMwJ",
            "formattedUrl": "https://www.thesaurus.com/browse/mammoth",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>mammoth</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of mammoth | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for mammoth from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Mammoth Synonyms, Mammoth Antonyms | Merriam-Webster ...",
            "htmlTitle": "<b>Mammoth Synonyms</b>, Mammoth Antonyms | Merriam-Webster ...",
            "link": "https://www.merriam-webster.com/thesaurus/mammoth",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Some common synonyms of mammoth are colossal, enormous, gigantic, huge, \nimmense, and vast. While all these words mean \"exceedingly large,\" mammoth ...",
            "htmlSnippet": "Some common <b>synonyms</b> of <b>mammoth</b> are colossal, enormous, gigantic, huge, <br>\nimmense, and vast. While all these words mean &quot;exceedingly large,&quot; <b>mammoth</b>&nbsp;...",
            "cacheId": "FaBJwpu0xWUJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/mammoth",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>mammoth</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for MAMMOTH",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/mammoth",
                        "og:title": "Thesaurus results for MAMMOTH",
                        "twitter:aria-text": "Share more words for mammoth on Twitter",
                        "og:aria-text": "Post more words for mammoth to Facebook",
                        "og:description": "Mammoth: unusually large. Synonyms: astronomical, Brobdingnagian, bumper… Antonyms: bantam, bitty, diminutive… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Mammoth: unusually large. Synonyms: astronomical, Brobdingnagian, bumper… Antonyms: bantam, bitty, diminutive… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/mammoth"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Mammoth synonyms | Best 46 synonyms for mammoth",
            "htmlTitle": "<b>Mammoth synonyms</b> | Best 46 synonyms for mammoth",
            "link": "https://thesaurus.yourdictionary.com/mammoth",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 46 synonyms for mammoth, including: gigantic, immense, enormous, \nhigh, large, colossal, gargantuan, giant, great, huge, massive and more.",
            "htmlSnippet": "The best 46 <b>synonyms</b> for <b>mammoth</b>, including: gigantic, immense, enormous, <br>\nhigh, large, colossal, gargantuan, giant, great, huge, massive and more.",
            "cacheId": "r0WDZBoroAoJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/mammoth",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>mammoth</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for mammoth? | Mammoth Synonyms ...",
            "htmlTitle": "What is another word for mammoth? | <b>Mammoth Synonyms</b> ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/mammoth.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "mammoth · Adjective · Fitted together of huge irregular stones · Requiring, or \ninvolving, great mental or physical effort · Having too many to count · Of \nconsiderable ...",
            "htmlSnippet": "<b>mammoth</b> &middot; Adjective &middot; Fitted together of huge irregular stones &middot; Requiring, or <br>\ninvolving, great mental or physical effort &middot; Having too many to count &middot; Of <br>\nconsiderable&nbsp;...",
            "cacheId": "fdFOntw6Y_YJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/mammoth.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>mammoth</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "394 Mammoth Synonyms and 103 Mammoth Antonyms | Mammoth ...",
            "htmlTitle": "394 <b>Mammoth Synonyms</b> and 103 Mammoth Antonyms | Mammoth ...",
            "link": "https://www.powerthesaurus.org/mammoth",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "mammoth · synonyms · antonyms · definitions · examples. Parts of speech. Tags. \nsynonyms. - 394. gigantic · adj. enormous · adj. huge · adj. colossal · adj. giant.",
            "htmlSnippet": "<b>mammoth</b> &middot; <b>synonyms</b> &middot; antonyms &middot; definitions &middot; examples. Parts of speech. Tags. <br>\n<b>synonyms</b>. - 394. gigantic &middot; adj. enormous &middot; adj. huge &middot; adj. colossal &middot; adj. giant.",
            "cacheId": "05VQrNh-fpgJ",
            "formattedUrl": "https://www.powerthesaurus.org/mammoth",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>mammoth</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "394 Mammoth Synonyms and 103 Mammoth Antonyms | Mammoth in Thesaurus",
                        "og:description": "Synonyms (Other Words) for Mammoth & Antonyms (Opposite Meaning) for Mammoth.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/mammoth",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for mammoth | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>mammoth</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/mammoth",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "NRp5YOG2_eAJ",
            "formattedUrl": "https://www.synonym.com/synonyms/mammoth",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>mammoth</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSNPaQwyj2t_vB9GfyNeHSwmvM09gteOLaZBnBpO4dPbZHGXuZcGOBvQKFb",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for mammoth",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for mammoth",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "mammoth | definition: so exceedingly large or extensive as to suggest a giant or mammoth | synonyms: large, big, gigantic| antonyms: little, small, nonpregnant, unrhetorical",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "mammoth | definition: so exceedingly large or extensive as to suggest a giant or mammoth | synonyms: large, big, gigantic| antonyms: little, small, nonpregnant, unrhetorical",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/mammoth"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/dist/img/wordtigo-logo.987687df.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Mammoth Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Mammoth Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/mammoth",
            "displayLink": "www.synonyms.com",
            "snippet": "Synonyms for mammoth ˈmæm əθ. This thesaurus page is about all possible \nsynonyms, equivalent, same meaning and similar words for the term mammoth ...",
            "htmlSnippet": "<b>Synonyms</b> for <b>mammoth</b> ˈmæm əθ. This thesaurus page is about all possible <br>\n<b>synonyms</b>, equivalent, same meaning and similar words for the term <b>mammoth</b>&nbsp;...",
            "cacheId": "9l0MKeKolhoJ",
            "formattedUrl": "https://www.synonyms.com/synonym/mammoth",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>mammoth</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/mammoth",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Mammoth | Synonyms of Mammoth by Lexico",
            "htmlTitle": "<b>Mammoth</b> | <b>Synonyms</b> of <b>Mammoth</b> by Lexico",
            "link": "https://www.lexico.com/synonym/mammoth",
            "displayLink": "www.lexico.com",
            "snippet": "adjective. 1'a mammoth task'. SYNONYMS. huge, enormous, gigantic, giant ...",
            "htmlSnippet": "adjective. 1&#39;a <b>mammoth</b> task&#39;. <b>SYNONYMS</b>. huge, enormous, gigantic, giant&nbsp;...",
            "cacheId": "Nzok3zRzBjcJ",
            "formattedUrl": "https://www.lexico.com/synonym/mammoth",
            "htmlFormattedUrl": "https://www.lexico.com/<b>synonym</b>/<b>mammoth</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT-fHqiHsELFzBLFkvhF3MUNoGyDc02pwOeICq9JD-vhUo5TV_0a-_rYOwh",
                        "width": "291",
                        "height": "173"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#50b46c",
                        "og:image": "https://www.lexico.com/lexico-logo.png",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Mammoth | Synonyms of Mammoth by Lexico",
                        "og:type": "website",
                        "theme-color": "#50b46c",
                        "og:site_name": "Lexico Dictionaries | English",
                        "og:title": "Mammoth | Synonyms of Mammoth by Lexico",
                        "msapplication-tileimage": "/mstile-144x144.png",
                        "csrf-param": "authenticity_token",
                        "og:description": "What is the definition of mammoth? What is the meaning of mammoth? How do you use mammoth in a sentence? What are synonyms for mammoth?",
                        "twitter:image": "https://www.lexico.com/lexico-logo.png",
                        "viewport": "width=device-width, initial-scale=1, shrink-to-fit=no",
                        "twitter:description": "What is the definition of mammoth? What is the meaning of mammoth? How do you use mammoth in a sentence? What are synonyms for mammoth?",
                        "csrf-token": "rqDEKKz4gOYGrr6QfO95knN5gKtkw/Rcd8B45dbLrXybV5Ubl5BUdOp2K8zfurKry63mO6h1fYFddiKBlK24gQ==",
                        "og:url": "https://www.lexico.com/synonym/mammoth"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.lexico.com/lexico-logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Antonym of mammoth",
            "htmlTitle": "Antonym of <b>mammoth</b>",
            "link": "https://www.synonyms.com/antonyms/mammoth",
            "displayLink": "www.synonyms.com",
            "snippet": "Antonyms for mammoth at Synonyms.com with free online thesaurus, synonyms, \ndefinitions and translations.",
            "htmlSnippet": "Antonyms for <b>mammoth</b> at <b>Synonyms</b>.com with free online thesaurus, <b>synonyms</b>, <br>\ndefinitions and translations.",
            "cacheId": "jfEEwrnYbfwJ",
            "formattedUrl": "https://www.synonyms.com/antonyms/mammoth",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/antonyms/<b>mammoth</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/antonyms/mammoth",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Mammoth synonyms, mammoth antonyms - FreeThesaurus.com",
            "htmlTitle": "<b>Mammoth synonyms</b>, mammoth antonyms - FreeThesaurus.com",
            "link": "https://www.freethesaurus.com/mammoth",
            "displayLink": "www.freethesaurus.com",
            "snippet": "Synonyms for mammoth in Free Thesaurus. Antonyms for mammoth. 63 \nsynonyms for mammoth: colossal, huge, giant, massive, vast, enormous, mighty,\n ...",
            "htmlSnippet": "<b>Synonyms</b> for <b>mammoth</b> in Free Thesaurus. Antonyms for <b>mammoth</b>. 63 <br>\n<b>synonyms</b> for <b>mammoth</b>: colossal, huge, giant, massive, vast, enormous, mighty,<br>\n&nbsp;...",
            "cacheId": "vrtq3cW3FNAJ",
            "formattedUrl": "https://www.freethesaurus.com/mammoth",
            "htmlFormattedUrl": "https://www.freethesaurus.com/<b>mammoth</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYdkuI0uE9gcqSkj_ayX2U9j3w9PMpjL9KOS4mDt6h2s29lFotin4do3Q",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-packagefamilyname": "Farlex.581429F59E1D8_wyegy4e46y996",
                        "apple-itunes-app": "app-id=379450383, app-argument=thefreedictionary://search/mammoth?",
                        "og:image": "http://img.tfd.com/TFDlogo1200x1200.png",
                        "og:type": "article",
                        "og:image:width": "1200",
                        "og:site_name": "TheFreeDictionary.com",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "mammoth",
                        "og:image:height": "1200",
                        "og:url": "https://www.freethesaurus.com/mammoth",
                        "msapplication-id": "Farlex.581429F59E1D8",
                        "og:description": "mammoth synonyms, antonyms, and related words in the Free Thesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "http://img.tfd.com/TFDlogo1200x1200.png"
                    }
                ]
            }
        }
    ]
}
{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - greece  synonyms",
                "totalResults": "13900000",
                "searchTerms": "greece  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - greece  synonyms",
                "totalResults": "13900000",
                "searchTerms": "greece  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.232771,
        "formattedSearchTime": "0.23",
        "totalResults": "13900000",
        "formattedTotalResults": "13,900,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Greece synonyms | Best 15 synonyms for greece",
            "htmlTitle": "<b>Greece synonyms</b> | Best 15 synonyms for greece",
            "link": "https://thesaurus.yourdictionary.com/greece",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 15 synonyms for greece, including: hellas, hellenic-republic, graecia, \nGreek peninsula, Ellas, classical, greek, hellenic, philhellene, philhellenic, \nachaia ...",
            "htmlSnippet": "The best 15 <b>synonyms</b> for <b>greece</b>, including: hellas, hellenic-republic, graecia, <br>\n<b>Greek</b> peninsula, Ellas, classical, <b>greek</b>, hellenic, philhellene, philhellenic, <br>\nachaia&nbsp;...",
            "cacheId": "BuM6wY0Y6vIJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/greece",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>greece</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Greek Synonyms, Greek Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Greek Synonyms</b>, Greek Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/greek",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for Greek at Thesaurus.com with free online thesaurus, antonyms, and \ndefinitions. Find descriptive alternatives for Greek.",
            "htmlSnippet": "<b>Synonyms</b> for <b>Greek</b> at Thesaurus.com with free online thesaurus, antonyms, and <br>\ndefinitions. Find descriptive alternatives for <b>Greek</b>.",
            "cacheId": "ktY706spmlIJ",
            "formattedUrl": "https://www.thesaurus.com/browse/greek",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>greek</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of Greek | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for Greek from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for greece | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> for <b>greece</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/greece",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "r5uGdcnuMYIJ",
            "formattedUrl": "https://www.synonym.com/synonyms/greece",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>greece</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for greece",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms for greece",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "greece | definition: a republic in southeastern Europe on the southern part of the Balkan peninsula; known for grapes and olives and olive oil | synonyms: Grecian, Mount Olympus, Thessaloniki, European Community, Mytilene, EU, Peloponnese, Thessalonica, Common Market, Olimbos, Laconia, Ellas, Parnassus, Peloponnesian Peninsula, Corinth, Khios, Doris, Peloponnesus, Athens, Arcadia, EC, capital of Greece, Cyclades, European Economic Community, North Atlantic Treaty Organization, Achaea, European Union, Attica, Ithaca, Rhodes, Lemnos, Lesvos, Limnos, Gulf of Aegina, Athinai, Epirus, Rodhos, EEC, Hellenic Republic, Kriti, Olympus, Salonika, Mount Athos, Kikladhes, Stagirus, Dhodhekanisos, Hellene, Crete, Mount Parnassus, Liakoura, Delphi, Aigina, Thessalia, Salonica, Chios, Aegina, Korinthos, Mt. Olympus, Mycenae, Saronic Gulf, Europe, Greek, Stagira, Thessaly, Dodecanese, Nemea, Boeotia, Argos, Lesbos, Actium, Ithaki, NATO, Athos, Greek capital",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "greece | definition: a republic in southeastern Europe on the southern part of the Balkan peninsula; known for grapes and olives and olive oil | synonyms: Grecian, Mount Olympus, Thessaloniki, European Community, Mytilene, EU, Peloponnese, Thessalonica, Common Market, Olimbos, Laconia, Ellas, Parnassus, Peloponnesian Peninsula, Corinth, Khios, Doris, Peloponnesus, Athens, Arcadia, EC, capital of Greece, Cyclades, European Economic Community, North Atlantic Treaty Organization, Achaea, European Union, Attica, Ithaca, Rhodes, Lemnos, Lesvos, Limnos, Gulf of Aegina, Athinai, Epirus, Rodhos, EEC, Hellenic Republic, Kriti, Olympus, Salonika, Mount Athos, Kikladhes, Stagirus, Dhodhekanisos, Hellene, Crete, Mount Parnassus, Liakoura, Delphi, Aigina, Thessalia, Salonica, Chios, Aegina, Korinthos, Mt. Olympus, Mycenae, Saronic Gulf, Europe, Greek, Stagira, Thessaly, Dodecanese, Nemea, Boeotia, Argos, Lesbos, Actium, Ithaki, NATO, Athos, Greek capital",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/greece"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Greek Synonyms, Greek Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Greek Synonyms</b>, Greek Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/greek",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms & Antonyms of greek · absurdity, · asininity, · fatuity, · foolery, · idiocy, · \nimbecility, · inaneness, · inanity, ...",
            "htmlSnippet": "<b>Synonyms</b> &amp; Antonyms of <b>greek</b> &middot; absurdity, &middot; asininity, &middot; fatuity, &middot; foolery, &middot; idiocy, &middot; <br>\nimbecility, &middot; inaneness, &middot; inanity,&nbsp;...",
            "cacheId": "A8MpkCwCM_8J",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/greek",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>greek</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for GREEK",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/greek",
                        "og:title": "Thesaurus results for GREEK",
                        "twitter:aria-text": "Share more words for greek on Twitter",
                        "og:aria-text": "Post more words for greek to Facebook",
                        "og:description": "Greek: as in hocus-pocus, double-talk. Synonyms: double-talk, hocus-pocus, gas… Antonyms: levelheadedness, rationality, reasonability… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Greek: as in hocus-pocus, double-talk. Synonyms: double-talk, hocus-pocus, gas… Antonyms: levelheadedness, rationality, reasonability… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/greek"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Greece Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Greece Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/greece",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for Greece: Collins Dictionary Definition | Collins English \nThesaurus.",
            "htmlSnippet": "Another word for <b>Greece</b>: Collins Dictionary Definition | Collins English <br>\nThesaurus.",
            "cacheId": "NJ-xT3SOGnAJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/greece",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/<b>greece</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Greece Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "f04d79d1-ba34-454e-8da2-44d3fbff58ea",
                        "_csrf_parameter": "_csrf",
                        "title": "Greece Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for Greece: Collins Dictionary Definition | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/greece"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "145 Greece Synonyms | Greece in Thesaurus",
            "htmlTitle": "145 <b>Greece Synonyms</b> | Greece in Thesaurus",
            "link": "https://www.powerthesaurus.org/greece",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Synonyms (Other Words) for Greece & Antonyms (Opposite Meaning) for Greece.",
            "htmlSnippet": "<b>Synonyms</b> (Other Words) for <b>Greece</b> &amp; Antonyms (Opposite Meaning) for <b>Greece</b>.",
            "cacheId": "hn9njizEl14J",
            "formattedUrl": "https://www.powerthesaurus.org/greece",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>greece</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "145 Greece Synonyms | Greece in Thesaurus",
                        "og:description": "Synonyms (Other Words) for Greece & Antonyms (Opposite Meaning) for Greece.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/greece",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Greek Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Greek Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/Greek",
            "displayLink": "www.synonyms.com",
            "snippet": "Find all the synonyms and alternative words for Greek at Synonyms.com, the \nlargest free online thesaurus, antonyms, definitions and translations resource on\n ...",
            "htmlSnippet": "Find all the <b>synonyms</b> and alternative words for <b>Greek</b> at <b>Synonyms</b>.com, the <br>\nlargest free online thesaurus, antonyms, definitions and translations resource on<br>\n&nbsp;...",
            "cacheId": "7tMV3xDUcnQJ",
            "formattedUrl": "https://www.synonyms.com/synonym/Greek",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>Greek</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/Greek",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Greece synonyms, Greece antonyms - FreeThesaurus.com",
            "htmlTitle": "<b>Greece synonyms</b>, Greece antonyms - FreeThesaurus.com",
            "link": "https://www.freethesaurus.com/Greece",
            "displayLink": "www.freethesaurus.com",
            "snippet": "Synonyms for Greece in Free Thesaurus. Antonyms for Greece. 2 synonyms for \nGreece: Ellas, Hellenic Republic. What are synonyms for Greece?",
            "htmlSnippet": "<b>Synonyms</b> for <b>Greece</b> in Free Thesaurus. Antonyms for <b>Greece</b>. 2 <b>synonyms</b> for <br>\n<b>Greece</b>: Ellas, Hellenic Republic. What are <b>synonyms</b> for <b>Greece</b>?",
            "cacheId": "-LNY1u6zFXwJ",
            "formattedUrl": "https://www.freethesaurus.com/Greece",
            "htmlFormattedUrl": "https://www.freethesaurus.com/<b>Greece</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYdkuI0uE9gcqSkj_ayX2U9j3w9PMpjL9KOS4mDt6h2s29lFotin4do3Q",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-packagefamilyname": "Farlex.581429F59E1D8_wyegy4e46y996",
                        "apple-itunes-app": "app-id=379450383, app-argument=thefreedictionary://search/Greece?",
                        "og:image": "http://img.tfd.com/TFDlogo1200x1200.png",
                        "og:type": "article",
                        "og:image:width": "1200",
                        "og:site_name": "TheFreeDictionary.com",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Greece",
                        "og:image:height": "1200",
                        "og:url": "https://www.freethesaurus.com/Greece",
                        "msapplication-id": "Farlex.581429F59E1D8",
                        "og:description": "Greece synonyms, antonyms, and related words in the Free Thesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "http://img.tfd.com/TFDlogo1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "'ancient-greece' Tag Synonyms - History Stack Exchange",
            "htmlTitle": "&#39;ancient-<b>greece</b>&#39; Tag <b>Synonyms</b> - History Stack Exchange",
            "link": "https://history.stackexchange.com/tags/ancient-greece/synonyms",
            "displayLink": "history.stackexchange.com",
            "snippet": "Tag synonyms for ancient-greece. Incorrectly tagged questions are hard to find \nand answer. If you know of common, alternate spellings or phrasings for this tag,\n ...",
            "htmlSnippet": "Tag <b>synonyms</b> for ancient-<b>greece</b>. Incorrectly tagged questions are hard to find <br>\nand answer. If you know of common, alternate spellings or phrasings for this tag,<br>\n&nbsp;...",
            "cacheId": "fiGmbmTiCj0J",
            "formattedUrl": "https://history.stackexchange.com/tags/ancient-greece/synonyms",
            "htmlFormattedUrl": "https://history.stackexchange.com/tags/ancient-<b>greece</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQMzVYw8uWU58Xq8IoQWAFkuvTCTNUIApzgvuzmH413i0S5rHVVIW7GILi2",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://cdn.sstatic.net/Sites/history/Img/apple-touch-icon@2.png?v=7ab66c156165",
                        "og:type": "website",
                        "twitter:card": "summary",
                        "twitter:title": "'ancient-greece' Tag Synonyms",
                        "og:site_name": "History Stack Exchange",
                        "twitter:site": "@StackHistory",
                        "twitter:domain": "history.stackexchange.com",
                        "twitter:description": "Q&A for historians and history buffs",
                        "og:url": "https://history.stackexchange.com/tags/ancient-greece/synonyms",
                        "twitter:creator": "@StackHistory"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://cdn.sstatic.net/Sites/history/Img/apple-touch-icon@2.png?v=7ab66c156165"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Greece synonyms with definition | Macmillan Thesaurus",
            "htmlTitle": "<b>Greece synonyms</b> with definition | Macmillan Thesaurus",
            "link": "https://www.macmillanthesaurus.com/us/greece",
            "displayLink": "www.macmillanthesaurus.com",
            "snippet": "Related terms for 'Greece': Afghanistan, Albania, Algeria, American Samoa, \nAndorra, Angola, Anguilla, Antigua and Barbuda, Argentina, Armenia, Aruba.",
            "htmlSnippet": "Related terms for &#39;<b>Greece</b>&#39;: Afghanistan, Albania, Algeria, American Samoa, <br>\nAndorra, Angola, Anguilla, Antigua and Barbuda, Argentina, Armenia, Aruba.",
            "cacheId": "IGlYTDm4pjkJ",
            "formattedUrl": "https://www.macmillanthesaurus.com/us/greece",
            "htmlFormattedUrl": "https://www.macmillanthesaurus.com/us/<b>greece</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcT9goPP7Zsa9scePPEN31vGhsjx4a85XENV70Z_OC89XKojY-ytUZBZdA",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/us/external/images/logoThesaurus.png?version=1.0.43",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no",
                        "og:title": "Greece synonyms with definition | Macmillan Thesaurus",
                        "title": "Greece synonyms with definition | Macmillan Thesaurus",
                        "og:url": "https://www.macmillanthesaurus.com/us/greece",
                        "og:description": "Related terms for 'Greece': Afghanistan, Albania, Algeria, American Samoa, Andorra, Angola, Anguilla, Antigua and Barbuda, Argentina, Armenia, Aruba"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillanthesaurus.com/us/external/images/bg-wave.svg?version=1.0.43"
                    }
                ]
            }
        }
    ]
}
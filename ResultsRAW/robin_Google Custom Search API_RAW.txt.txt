{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - robin  synonyms",
                "totalResults": "7820000",
                "searchTerms": "robin  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - robin  synonyms",
                "totalResults": "7820000",
                "searchTerms": "robin  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.264377,
        "formattedSearchTime": "0.26",
        "totalResults": "7820000",
        "formattedTotalResults": "7,820,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Robin Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Robin Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/robin",
            "displayLink": "www.synonyms.com",
            "snippet": "Find all the synonyms and alternative words for robin at Synonyms.com, the \nlargest free online thesaurus, antonyms, definitions and translations resource on \nthe ...",
            "htmlSnippet": "Find all the <b>synonyms</b> and alternative words for <b>robin</b> at <b>Synonyms</b>.com, the <br>\nlargest free online thesaurus, antonyms, definitions and translations resource on <br>\nthe&nbsp;...",
            "cacheId": "P8tczdp4isoJ",
            "formattedUrl": "https://www.synonyms.com/synonym/robin",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>robin</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/robin",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Round robin Synonyms, Round robin Antonyms | Thesaurus.com",
            "htmlTitle": "Round <b>robin Synonyms</b>, Round robin Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/round%20robin",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for round robin at Thesaurus.com with free online thesaurus, \nantonyms, and definitions. Find descriptive alternatives for round robin.",
            "htmlSnippet": "<b>Synonyms</b> for round <b>robin</b> at Thesaurus.com with free online thesaurus, <br>\nantonyms, and definitions. Find descriptive alternatives for round <b>robin</b>.",
            "cacheId": "yIRgt7xg82AJ",
            "formattedUrl": "https://www.thesaurus.com/browse/round%20robin",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/round%20<b>robin</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of round robin | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for round robin from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for robin | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> for <b>robin</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/robin",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions. ... Synonyms. robin redbreast Erithacus ... 2. early wake-robin. noun.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions. ... <b>Synonyms</b>. <b>robin</b> redbreast Erithacus ... 2. early wake-<b>robin</b>. noun.",
            "cacheId": "c8O8VlSg4eEJ",
            "formattedUrl": "https://www.synonym.com/synonyms/robin",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>robin</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSNPaQwyj2t_vB9GfyNeHSwmvM09gteOLaZBnBpO4dPbZHGXuZcGOBvQKFb",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for robin",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms for robin",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "robin | definition: small Old World songbird with a reddish breast | synonyms: robin redbreast, Erithacus rubecola, thrush, Old World robin, Erithacus, redbreast, genus Erithacus",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "robin | definition: small Old World songbird with a reddish breast | synonyms: robin redbreast, Erithacus rubecola, thrush, Old World robin, Erithacus, redbreast, genus Erithacus",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/robin"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/dist/img/wordtigo-logo.987687df.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Robin hood Synonyms, Robin hood Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Robin</b> hood <b>Synonyms</b>, <b>Robin</b> hood Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/robin%20hood",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for Robin Hood at Thesaurus.com with free online thesaurus, \nantonyms, and definitions. Find descriptive alternatives for Robin Hood.",
            "htmlSnippet": "<b>Synonyms</b> for <b>Robin</b> Hood at Thesaurus.com with free online thesaurus, <br>\nantonyms, and definitions. Find descriptive alternatives for <b>Robin</b> Hood.",
            "cacheId": "RI11z--bVjoJ",
            "formattedUrl": "https://www.thesaurus.com/browse/robin%20hood",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>robin</b>%20hood",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of Robin Hood | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for Robin Hood from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for ROBIN - Thesaurus.net",
            "htmlTitle": "<b>Synonyms</b> for <b>ROBIN</b> - Thesaurus.net",
            "link": "https://www.thesaurus.net/robin",
            "displayLink": "www.thesaurus.net",
            "snippet": "Synonyms for ROBIN: old world robin, Turdus Migratorius, attack, prayer, imp, \nrequisition, goblin, troll, nix, spotted flycatcher, communiqu, notice, cry down, \ndecry ...",
            "htmlSnippet": "<b>Synonyms</b> for <b>ROBIN</b>: old world <b>robin</b>, Turdus Migratorius, attack, prayer, imp, <br>\nrequisition, goblin, troll, nix, spotted flycatcher, communiqu, notice, cry down, <br>\ndecry&nbsp;...",
            "cacheId": "brz5DfmXYnkJ",
            "formattedUrl": "https://www.thesaurus.net/robin",
            "htmlFormattedUrl": "https://www.thesaurus.net/<b>robin</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTHmPbL6L3tWwemgnxXK_eIzcEGEcFI39SJD35kF79sVRInWi-xNtX9sw",
                        "width": "312",
                        "height": "53"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.net/public/desktop/images/logo.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for ROBIN - Thesaurus.net",
                        "og:site_name": "Thesaurus.net",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "robin | synonyms: old world robin, Turdus Migratorius, attack, prayer, imp, requisition, goblin, troll, nix, spotted flycatcher, communiqu, notice, cry down, decry, Barghest, Deev, Nixie, representation, dwarf, war, european robin, Ouphe, request, blacklist, catbird, leprechaun, bad fairy, redbreast, suit, revolt, remonstrate, oppose, strike, communication, Afreet, hiss, gin, hobgoblin, lamia, disobey, robin redbreast, enunciation, bogeyman, Bogle, vampire, clamor, boy wonder, ghoul, demand, Kobold, Pigwidgeon, Nis, claim, elf, urchin, ogress, Robins, ogre, petition, mob, kick, flibbertigibbet, bogie, announcement, hoot, gnome, presentment, ostracize, sprite, annunciation, Jinn, warbler, intimation, pixy, recalcitrate, notification, fairy, brownie, American Robin, Erithacus Rubecola, budgerigar, bantam, bluejay, bird of paradise, bobwhite, budgie, robin, hummingbird, buzzard, road runner, sparrow, oriole, swan, bald eagle, flamingo, squab, duck, sea gull, swift, falcon, cardinal, dodo, dove, mockingbird, shel",
                        "og:title": "Synonyms for ROBIN - Thesaurus.net",
                        "og:url": "https://www.thesaurus.net/robin",
                        "og:description": "robin | synonyms: old world robin, Turdus Migratorius, attack, prayer, imp, requisition, goblin, troll, nix, spotted flycatcher, communiqu, notice, cry down, decry, Barghest, Deev, Nixie, representation, dwarf, war, european robin, Ouphe, request, blacklist, catbird, leprechaun, bad fairy, redbreast, suit, revolt, remonstrate, oppose, strike, communication, Afreet, hiss, gin, hobgoblin, lamia, disobey, robin redbreast, enunciation, bogeyman, Bogle, vampire, clamor, boy wonder, ghoul, demand, Kobold, Pigwidgeon, Nis, claim, elf, urchin, ogress, Robins, ogre, petition, mob, kick, flibbertigibbet, bogie, announcement, hoot, gnome, presentment, ostracize, sprite, annunciation, Jinn, warbler, intimation, pixy, recalcitrate, notification, fairy, brownie, American Robin, Erithacus Rubecola, budgerigar, bantam, bluejay, bird of paradise, bobwhite, budgie, robin, hummingbird, buzzard, road runner, sparrow, oriole, swan, bald eagle, flamingo, squab, duck, sea gull, swift, falcon, cardinal, dodo, dove, mockingbird, shel"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.net/public/desktop/images/logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "249 Robin Synonyms | Robin in Thesaurus",
            "htmlTitle": "249 <b>Robin Synonyms</b> | Robin in Thesaurus",
            "link": "https://www.powerthesaurus.org/robin",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Synonyms (Other Words) for Robin & Antonyms (Opposite Meaning) for Robin.",
            "htmlSnippet": "<b>Synonyms</b> (Other Words) for <b>Robin</b> &amp; Antonyms (Opposite Meaning) for <b>Robin</b>.",
            "cacheId": "YqiFf2PrjwEJ",
            "formattedUrl": "https://www.powerthesaurus.org/robin",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>robin</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "249 Robin Synonyms | Robin in Thesaurus",
                        "og:description": "Synonyms (Other Words) for Robin & Antonyms (Opposite Meaning) for Robin.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/robin",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Round-robin Synonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "Round-<b>robin Synonyms</b> | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/round-robin",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms of round-robin · colloquy, · conference, · council, · forum, · panel, · \npanel discussion, · parley, · powwow ...",
            "htmlSnippet": "<b>Synonyms</b> of round-<b>robin</b> &middot; colloquy, &middot; conference, &middot; council, &middot; forum, &middot; panel, &middot; <br>\npanel discussion, &middot; parley, &middot; powwow&nbsp;...",
            "cacheId": "lXrJMARomgwJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/round-robin",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/round-<b>robin</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for ROUND-ROBIN",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/round-robin",
                        "og:title": "Thesaurus results for ROUND-ROBIN",
                        "twitter:aria-text": "Share more words for round-robin on Twitter",
                        "og:aria-text": "Post more words for round-robin to Facebook",
                        "og:description": "Round-robin: a meeting featuring a group discussion. Synonyms: colloquy, conference, council… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Round-robin: a meeting featuring a group discussion. Synonyms: colloquy, conference, council… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/round-robin"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "ROBIN (noun) definition and synonyms | Macmillan Dictionary",
            "htmlTitle": "<b>ROBIN</b> (noun) definition and <b>synonyms</b> | Macmillan Dictionary",
            "link": "https://www.macmillandictionary.com/us/dictionary/american/robin",
            "displayLink": "www.macmillandictionary.com",
            "snippet": "Definition of ROBIN (noun): small brown European bird with red chest. ... robin ​\nDefinitions and Synonyms. ​noun countable. UK /ˈrɒbɪn/ ...",
            "htmlSnippet": "Definition of <b>ROBIN</b> (noun): small brown European bird with red chest. ... <b>robin</b> ​<br>\nDefinitions and <b>Synonyms</b>. ​noun countable. UK   /ˈrɒbɪn/&nbsp;...",
            "cacheId": "LTunwZh6QFYJ",
            "formattedUrl": "https://www.macmillandictionary.com/us/dictionary/american/robin",
            "htmlFormattedUrl": "https://www.macmillandictionary.com/us/dictionary/american/<b>robin</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQq1XwZHyhsplz2gqTWfOMdrRK_HTPsdInhEZEectSt8unMIf5yPEGbNA",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "imageobject": [
                    {
                        "image": "https://www.macmillandictionary.com/external/slideshow/full/European Robin_full.jpg",
                        "caption": "European robin",
                        "description": "European robin"
                    },
                    {
                        "image": "https://www.macmillandictionary.com/external/slideshow/full/American Robin_full.jpg",
                        "caption": "American robin",
                        "description": "American robin"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.macmillandictionary.com/external/slideshow/thumb/European Robin_thumb.jpg",
                        "og:type": "website",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=10.0, minimum-scale=0.1",
                        "og:title": "ROBIN (noun) definition and synonyms | Macmillan Dictionary",
                        "og:url": "https://www.macmillandictionary.com/dictionary/british/robin",
                        "og:description": "Definition of ROBIN (noun): small brown European bird with red chest"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillandictionary.com/external/images/bg-wave.svg?version=4.0.27"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for Entries with Robin | Merriam-Webster ...",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for Entries with <b>Robin</b> | Merriam-Webster ...",
            "link": "https://www.merriam-webster.com/thesaurus/ROBIN",
            "displayLink": "www.merriam-webster.com",
            "snippet": "ROBIN is not entered in the thesaurus. Here is 1 entry in the thesaurus containing \nROBIN. Find Synonyms and Antonyms for entries with ROBIN.",
            "htmlSnippet": "<b>ROBIN</b> is not entered in the thesaurus. Here is 1 entry in the thesaurus containing <br>\n<b>ROBIN</b>. Find <b>Synonyms</b> and Antonyms for entries with <b>ROBIN</b>.",
            "cacheId": "ylKP1ZJIyxMJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/ROBIN",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>ROBIN</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for ROBIN",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/ROBIN",
                        "og:title": "Thesaurus results for ROBIN",
                        "twitter:aria-text": "Share Thesaurus results for ROBIN on Twitter",
                        "og:aria-text": "Post Thesaurus results for ROBIN to Facebook",
                        "og:description": "There are 1 entry in the thesaurus containing ROBIN. See the full list.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "There are 1 entry in the thesaurus containing ROBIN. See the full list.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/ROBIN"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Robin synonyms | Best 7 synonyms for robin",
            "htmlTitle": "<b>Robin synonyms</b> | Best 7 synonyms for robin",
            "link": "https://thesaurus.yourdictionary.com/robin",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "In this page you can discover 7 synonyms, antonyms, idiomatic expressions, and \nrelated words for robin, like: robert, american-robin, turdus-migratorius, redbreast,\n ...",
            "htmlSnippet": "In this page you can discover 7 <b>synonyms</b>, antonyms, idiomatic expressions, and <br>\nrelated words for <b>robin</b>, like: robert, american-<b>robin</b>, turdus-migratorius, redbreast,<br>\n&nbsp;...",
            "cacheId": "2hCZ7oASYMkJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/robin",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>robin</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        }
    ]
}
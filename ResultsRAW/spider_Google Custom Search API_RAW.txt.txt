{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - spider  synonyms",
                "totalResults": "5120000",
                "searchTerms": "spider  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - spider  synonyms",
                "totalResults": "5120000",
                "searchTerms": "spider  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.29181,
        "formattedSearchTime": "0.29",
        "totalResults": "5120000",
        "formattedTotalResults": "5,120,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Spider Synonyms, Spider Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Spider Synonyms</b>, Spider Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/spider",
            "displayLink": "www.thesaurus.com",
            "snippet": "RELATED WORDS AND SYNONYMS FOR SPIDER. frying pan. nounskillet. fry \npan · gridiron ...",
            "htmlSnippet": "RELATED WORDS AND <b>SYNONYMS</b> FOR <b>SPIDER</b>. frying pan. nounskillet. fry <br>\npan &middot; gridiron&nbsp;...",
            "cacheId": "-owYFFB91QYJ",
            "formattedUrl": "https://www.thesaurus.com/browse/spider",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>spider</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of spider | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for spider from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for spider | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>spider</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/spider",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "vaM-HoRMfYMJ",
            "formattedUrl": "https://www.synonym.com/synonyms/spider",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>spider</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for spider",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for spider",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "spider | definition: predatory arachnid with eight legs, two poison fangs, two feelers, and usually two silk-spinning organs at the back end of the body; they spin silk to make cocoons for eggs or traps for prey | synonyms: Argiope aurantia, Latrodectus mactans, barn spider, arachnoid, black and gold garden spider, tarantula, order Araneae, wolf spider, orb-weaving spider, garden spider, hunting spider, black widow, arachnid, Araneae, Araneida, order Araneida, Araneus cavaticus, trap-door spider, Aranea diademata, comb-footed spider, theridiid| antonyms: source program, hardware, object program",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "spider | definition: predatory arachnid with eight legs, two poison fangs, two feelers, and usually two silk-spinning organs at the back end of the body; they spin silk to make cocoons for eggs or traps for prey | synonyms: Argiope aurantia, Latrodectus mactans, barn spider, arachnoid, black and gold garden spider, tarantula, order Araneae, wolf spider, orb-weaving spider, garden spider, hunting spider, black widow, arachnid, Araneae, Araneida, order Araneida, Araneus cavaticus, trap-door spider, Aranea diademata, comb-footed spider, theridiid| antonyms: source program, hardware, object program",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/spider"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Spider synonyms | Best 6 synonyms for spider",
            "htmlTitle": "<b>Spider synonyms</b> | Best 6 synonyms for spider",
            "link": "https://thesaurus.yourdictionary.com/spider",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 6 synonyms for spider, including: wanderer, http (hypertext transfer \nprotocol), frying-pan, bot or robot, electronic mail or email, spammers and more.",
            "htmlSnippet": "The best 6 <b>synonyms</b> for <b>spider</b>, including: wanderer, http (hypertext transfer <br>\nprotocol), frying-pan, bot or robot, electronic mail or email, spammers and more.",
            "cacheId": "HJ29z4KhT_MJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/spider",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>spider</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "332 Spider Synonyms | Spider in Thesaurus",
            "htmlTitle": "332 <b>Spider Synonyms</b> | Spider in Thesaurus",
            "link": "https://www.powerthesaurus.org/spider",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Synonyms (Other Words) for Spider & Antonyms (Opposite Meaning) for Spider.",
            "htmlSnippet": "<b>Synonyms</b> (Other Words) for <b>Spider</b> &amp; Antonyms (Opposite Meaning) for <b>Spider</b>.",
            "cacheId": "q58xXaLZxqwJ",
            "formattedUrl": "https://www.powerthesaurus.org/spider",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>spider</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "332 Spider Synonyms | Spider in Thesaurus",
                        "og:description": "Synonyms (Other Words) for Spider & Antonyms (Opposite Meaning) for Spider.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/spider",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Spider Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Spider Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/spider",
            "displayLink": "www.synonyms.com",
            "snippet": "Find all the synonyms and alternative words for spider at Synonyms.com, the \nlargest free online thesaurus, antonyms, definitions and translations resource on\n ...",
            "htmlSnippet": "Find all the <b>synonyms</b> and alternative words for <b>spider</b> at <b>Synonyms</b>.com, the <br>\nlargest free online thesaurus, antonyms, definitions and translations resource on<br>\n&nbsp;...",
            "cacheId": "1cWrmZKNFjYJ",
            "formattedUrl": "https://www.synonyms.com/synonym/spider",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>spider</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/spider",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for spider? | Spider Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for spider? | <b>Spider Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/spider.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 509 synonyms for spider and other similar words that you can use instead \nbased on 7 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 509 <b>synonyms</b> for <b>spider</b> and other similar words that you can use instead <br>\nbased on 7 separate contexts from our thesaurus.",
            "cacheId": "zHJzhvRzrAAJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/spider.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>spider</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for SPIDER - Thesaurus.net",
            "htmlTitle": "<b>Synonyms</b> for <b>SPIDER</b> - Thesaurus.net",
            "link": "https://www.thesaurus.net/spider",
            "displayLink": "www.thesaurus.net",
            "snippet": "Synonyms for SPIDER: bulb, jenny, lota, adapter, schooner, tardigrade, wayfarer, \nfirework, drifter, spider crab, pot, creepy-crawly, tramp, Hexapod, mule, larva, ...",
            "htmlSnippet": "<b>Synonyms</b> for <b>SPIDER</b>: bulb, jenny, lota, adapter, schooner, tardigrade, wayfarer, <br>\nfirework, drifter, <b>spider</b> crab, pot, creepy-crawly, tramp, Hexapod, mule, larva,&nbsp;...",
            "cacheId": "VqTfFDxSKD0J",
            "formattedUrl": "https://www.thesaurus.net/spider",
            "htmlFormattedUrl": "https://www.thesaurus.net/<b>spider</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTHmPbL6L3tWwemgnxXK_eIzcEGEcFI39SJD35kF79sVRInWi-xNtX9sw",
                        "width": "312",
                        "height": "53"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.net/public/desktop/images/logo.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for SPIDER - Thesaurus.net",
                        "og:site_name": "Thesaurus.net",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "spider | synonyms: bulb, jenny, lota, adapter, schooner, tardigrade, wayfarer, firework, drifter, spider crab, pot, creepy-crawly, tramp, Hexapod, mule, larva, crowfoot, charm, cure-all, beaker, spinning jenny, gillnet, poor boy, crow foot, catch basin, Diplopod, pan, rover, throstle, bail, bug, toby, spinning frame, billy, fish, Canakin, laetrile, millepede, snake oil, Redback, Chilopod, casserole, maggot, silkworm, vagabond, brace, nomad, huntsman, nymph, rambler, saucepan, cob, terrine, grub, caterpillar, spiderly, cobweb, spinster, chatti, star, roamer, bot, bootstrap program, browser, architecture, assembler, add-on, applet, client, compiler, beetle, bloodsucker, grass, jumping, diadem, hermit, crab, bird, spinner, brown recluse, harvestman, violin back, Trapdoor, Burrowing, wolf, brown, hunting, water, garden, wanderer, daddy longlegs, scorpion, butterfly, pest, dragonfly, scarab, blowfly, grasshopper, midge, Horntail, bumblebee, katydid, hornet, locust, mite, earwig, yellow jacket, chinch, gadfly, tara",
                        "og:title": "Synonyms for SPIDER - Thesaurus.net",
                        "og:url": "https://www.thesaurus.net/spider",
                        "og:description": "spider | synonyms: bulb, jenny, lota, adapter, schooner, tardigrade, wayfarer, firework, drifter, spider crab, pot, creepy-crawly, tramp, Hexapod, mule, larva, crowfoot, charm, cure-all, beaker, spinning jenny, gillnet, poor boy, crow foot, catch basin, Diplopod, pan, rover, throstle, bail, bug, toby, spinning frame, billy, fish, Canakin, laetrile, millepede, snake oil, Redback, Chilopod, casserole, maggot, silkworm, vagabond, brace, nomad, huntsman, nymph, rambler, saucepan, cob, terrine, grub, caterpillar, spiderly, cobweb, spinster, chatti, star, roamer, bot, bootstrap program, browser, architecture, assembler, add-on, applet, client, compiler, beetle, bloodsucker, grass, jumping, diadem, hermit, crab, bird, spinner, brown recluse, harvestman, violin back, Trapdoor, Burrowing, wolf, brown, hunting, water, garden, wanderer, daddy longlegs, scorpion, butterfly, pest, dragonfly, scarab, blowfly, grasshopper, midge, Horntail, bumblebee, katydid, hornet, locust, mite, earwig, yellow jacket, chinch, gadfly, tara"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.net/public/desktop/images/logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "SPIDER (noun) definition and synonyms | Macmillan Dictionary",
            "htmlTitle": "<b>SPIDER</b> (noun) definition and <b>synonyms</b> | Macmillan Dictionary",
            "link": "https://www.macmillandictionary.com/us/dictionary/american/spider",
            "displayLink": "www.macmillandictionary.com",
            "snippet": "spider ​Definitions and Synonyms ​‌. ​noun countable. UK /ˈspaɪdə(r) ...",
            "htmlSnippet": "<b>spider</b> ​Definitions and <b>Synonyms</b> ​‌. ​noun countable. UK   /ˈspaɪdə(r)&nbsp;...",
            "cacheId": "c--Hs4Ku-mkJ",
            "formattedUrl": "https://www.macmillandictionary.com/us/dictionary/american/spider",
            "htmlFormattedUrl": "https://www.macmillandictionary.com/us/dictionary/american/<b>spider</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQq1XwZHyhsplz2gqTWfOMdrRK_HTPsdInhEZEectSt8unMIf5yPEGbNA",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/external/images/social/social_macmillan.jpg?version=4.0.27",
                        "og:type": "website",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=10.0, minimum-scale=0.1",
                        "og:title": "SPIDER (noun) definition and synonyms | Macmillan Dictionary",
                        "og:url": "https://www.macmillandictionary.com/dictionary/british/spider",
                        "og:description": "Definition of SPIDER (noun): creature with eight legs that weaves web; program that searches for new websites"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillandictionary.com/external/images/bg-wave.svg?version=4.0.27"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Spider Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Spider Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/spider",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for spider: Collins Dictionary Definition | Collins English Thesaurus.",
            "htmlSnippet": "Another word for <b>spider</b>: Collins Dictionary Definition | Collins English Thesaurus.",
            "cacheId": "wDcQEyuT9CQJ",
            "formattedUrl": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/spider",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/<b>spider</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTErRtm81ioo5cAR6Dn8N1IWDT5nl6xu6Eg-vrc3_QJ6OnJgHQi_5O9btGR",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/us/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/us/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Spider Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "faa10c8d-eb9a-43f5-b3ae-cad2da27fe9e",
                        "_csrf_parameter": "_csrf",
                        "title": "Spider Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for spider: Collins Dictionary Definition | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/spider"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/us/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Black widow spider synonyms, Black widow spider antonyms",
            "htmlTitle": "Black widow <b>spider synonyms</b>, Black widow spider antonyms",
            "link": "https://www.freethesaurus.com/Black+widow+spider",
            "displayLink": "www.freethesaurus.com",
            "snippet": "Synonyms for Black widow spider in Free Thesaurus. Antonyms for Black widow \nspider. 1 synonym for spider: wanderer. What are synonyms for Black widow ...",
            "htmlSnippet": "<b>Synonyms</b> for Black widow <b>spider</b> in Free Thesaurus. Antonyms for Black widow <br>\n<b>spider</b>. 1 <b>synonym</b> for <b>spider</b>: wanderer. What are <b>synonyms</b> for Black widow&nbsp;...",
            "cacheId": "OGHGwfTsGuQJ",
            "formattedUrl": "https://www.freethesaurus.com/Black+widow+spider",
            "htmlFormattedUrl": "https://www.freethesaurus.com/Black+widow+<b>spider</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYdkuI0uE9gcqSkj_ayX2U9j3w9PMpjL9KOS4mDt6h2s29lFotin4do3Q",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-packagefamilyname": "Farlex.581429F59E1D8_wyegy4e46y996",
                        "apple-itunes-app": "app-id=379450383, app-argument=thefreedictionary://search/Black+widow+spider?",
                        "og:image": "http://img.tfd.com/TFDlogo1200x1200.png",
                        "og:type": "article",
                        "og:image:width": "1200",
                        "og:site_name": "TheFreeDictionary.com",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Black widow spider",
                        "og:image:height": "1200",
                        "og:url": "https://www.freethesaurus.com/Black+widow+spider",
                        "msapplication-id": "Farlex.581429F59E1D8",
                        "og:description": "Black widow spider synonyms, antonyms, and related words in the Free Thesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "http://img.tfd.com/TFDlogo1200x1200.png"
                    }
                ]
            }
        }
    ]
}
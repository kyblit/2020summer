{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - agent  synonyms",
                "totalResults": "63900000",
                "searchTerms": "agent  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - agent  synonyms",
                "totalResults": "63900000",
                "searchTerms": "agent  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.397644,
        "formattedSearchTime": "0.40",
        "totalResults": "63900000",
        "formattedTotalResults": "63,900,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Agent Synonyms, Agent Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Agent Synonyms</b>, Agent Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/agent",
            "displayLink": "www.thesaurus.com",
            "snippet": "RELATED WORDS AND SYNONYMS FOR AGENT. agitator. nounperson who \ndisturbs, causes trouble. adjy ...",
            "htmlSnippet": "RELATED WORDS AND <b>SYNONYMS</b> FOR <b>AGENT</b>. agitator. nounperson who <br>\ndisturbs, causes trouble. adjy&nbsp;...",
            "cacheId": "JOC9zHvxgx0J",
            "formattedUrl": "https://www.thesaurus.com/browse/agent",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>agent</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of agent | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for agent from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Agent Synonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Agent Synonyms</b> | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/agent",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms of agent · 1 something used to achieve an end · 2 a person who acts \nor does business for another · 3 a person sent on a mission to represent another · \n4 a ...",
            "htmlSnippet": "<b>Synonyms</b> of <b>agent</b> &middot; 1 something used to achieve an end &middot; 2 a person who acts <br>\nor does business for another &middot; 3 a person sent on a mission to represent another &middot; <br>\n4 a&nbsp;...",
            "cacheId": "oFymDl4SNpQJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/agent",
            "htmlFormattedUrl": "https://www.merriam-webster.com/<b>thesaurus</b>/<b>agent</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for AGENT",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/agent",
                        "og:title": "Thesaurus results for AGENT",
                        "twitter:aria-text": "Share more words for agent on Twitter",
                        "og:aria-text": "Post more words for agent to Facebook",
                        "og:description": "Agent: something used to achieve an end. Synonyms: agency, instrument, instrumentality… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Agent: something used to achieve an end. Synonyms: agency, instrument, instrumentality… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/agent"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Agents Synonyms, Agents Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Agents Synonyms</b>, Agents Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/agents",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for agents at Thesaurus.com with free online thesaurus, antonyms, \nand definitions. Find descriptive alternatives for agents.",
            "htmlSnippet": "<b>Synonyms</b> for <b>agents</b> at <b>Thesaurus</b>.com with free online <b>thesaurus</b>, antonyms, <br>\nand definitions. Find descriptive alternatives for <b>agents</b>.",
            "cacheId": "PABcVi1hrhYJ",
            "formattedUrl": "https://www.thesaurus.com/browse/agents",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>agents</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of agents | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for agents from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for agent? | Agent Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for agent? | <b>Agent Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/agent.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 4032 synonyms for agent and other similar words that you can use instead \nbased on 31 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 4032 <b>synonyms</b> for <b>agent</b> and other similar words that you can use instead <br>\nbased on 31 separate contexts from our <b>thesaurus</b>.",
            "cacheId": "BuY8hjt13u0J",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/agent.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>agent</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Agent synonyms | Best 121 synonyms for agent",
            "htmlTitle": "<b>Agent synonyms</b> | Best 121 synonyms for agent",
            "link": "https://thesaurus.yourdictionary.com/agent",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "Find another word for agent. In this page you can discover 121 synonyms, \nantonyms, idiomatic expressions, and related words for agent, like: broker, \nassistant, ...",
            "htmlSnippet": "Find another word for <b>agent</b>. In this page you can discover 121 <b>synonyms</b>, <br>\nantonyms, idiomatic expressions, and related words for <b>agent</b>, like: broker, <br>\nassistant,&nbsp;...",
            "cacheId": "McrBQSPRWLEJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/agent",
            "htmlFormattedUrl": "https://<b>thesaurus</b>.yourdictionary.com/<b>agent</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for agent | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>agent</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/agent",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "uF-bwA-SM34J",
            "formattedUrl": "https://www.synonym.com/synonyms/agent",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>agent</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for agent",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for agent",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "agent | definition: an active and efficient cause; capable of producing a certain effect | synonyms: transmitter, vasoconstrictive, mutagen, causal agent, retardation, impairer, retardent, biological agent, weakener, pressor, causal agency, invigorator, satisfier, cause, infective agent, repressor, vector, motor, represser, vasoconstrictor, shortener, enlivener, eliminator, quickener, relaxer, retardant, teratogen, biohazard, stressor, infectious agent, lethal agent| antonyms: brand-name drug, synergist, abstain, bring to, septic",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "agent | definition: an active and efficient cause; capable of producing a certain effect | synonyms: transmitter, vasoconstrictive, mutagen, causal agent, retardation, impairer, retardent, biological agent, weakener, pressor, causal agency, invigorator, satisfier, cause, infective agent, repressor, vector, motor, represser, vasoconstrictor, shortener, enlivener, eliminator, quickener, relaxer, retardant, teratogen, biohazard, stressor, infectious agent, lethal agent| antonyms: brand-name drug, synergist, abstain, bring to, septic",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/agent"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "2 442 Agent Synonyms and 104 Agent Antonyms | Agent in Thesaurus",
            "htmlTitle": "2 442 <b>Agent Synonyms</b> and 104 Agent Antonyms | Agent in Thesaurus",
            "link": "https://www.powerthesaurus.org/agent",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Synonyms (Other Words) for Agent & Antonyms (Opposite Meaning) for Agent.",
            "htmlSnippet": "<b>Synonyms</b> (Other Words) for <b>Agent</b> &amp; Antonyms (Opposite Meaning) for <b>Agent</b>.",
            "cacheId": "OJL1zMvkz0cJ",
            "formattedUrl": "https://www.powerthesaurus.org/agent",
            "htmlFormattedUrl": "https://www.power<b>thesaurus</b>.org/<b>agent</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#ffffff",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "2 442 Agent Synonyms and 104 Agent Antonyms | Agent in Thesaurus",
                        "og:description": "Synonyms (Other Words) for Agent & Antonyms (Opposite Meaning) for Agent.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/agent",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for disease-causing agent",
            "htmlTitle": "<b>Synonyms</b> for disease-causing <b>agent</b>",
            "link": "https://thesaurus.com/browse/disease+causing+agent?qsrc=2446",
            "displayLink": "thesaurus.com",
            "snippet": "Synonyms for disease-causing agent at Thesaurus.com with free online \nthesaurus, antonyms, and definitions. Find descriptive alternatives for disease-\ncausing ...",
            "htmlSnippet": "<b>Synonyms</b> for disease-causing <b>agent</b> at <b>Thesaurus</b>.com with free online <br>\n<b>thesaurus</b>, antonyms, and definitions. Find descriptive alternatives for disease-<br>\ncausing&nbsp;...",
            "cacheId": "qakI6b25fQEJ",
            "formattedUrl": "https://thesaurus.com/browse/disease+causing+agent?qsrc=2446",
            "htmlFormattedUrl": "https://<b>thesaurus</b>.com/browse/disease+causing+<b>agent</b>?qsrc=2446",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "og:title": "Synonyms of disease-causing agent | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for disease-causing agent from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Agent Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Agent Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/agent",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for agent: representative, deputy, substitute, advocate, rep | Collins \nEnglish Thesaurus.",
            "htmlSnippet": "Another word for <b>agent</b>: representative, deputy, substitute, advocate, rep | Collins <br>\nEnglish <b>Thesaurus</b>.",
            "cacheId": "O95adpN8MNoJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/agent",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-<b>thesaurus</b>/<b>agent</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Agent Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "ca5451a3-f1a1-48ea-898e-18d94fc34e56",
                        "_csrf_parameter": "_csrf",
                        "title": "Agent Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for agent: representative, deputy, substitute, advocate, rep | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/agent"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Agent Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Agent Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/agent",
            "displayLink": "www.synonyms.com",
            "snippet": "Dictionary of English Synonymes(0.00 / 0 votes)Rate these synonyms: agent(n.) \nSynonyms: actor, doer, operator, performer, executor. agent ...",
            "htmlSnippet": "Dictionary of English Synonymes(0.00 / 0 votes)Rate these <b>synonyms</b>: <b>agent</b>(n.) <br>\n<b>Synonyms</b>: actor, doer, operator, performer, executor. <b>agent</b>&nbsp;...",
            "cacheId": "Tos1mpkdJbMJ",
            "formattedUrl": "https://www.synonyms.com/synonym/agent",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>agent</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/agent",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        }
    ]
}
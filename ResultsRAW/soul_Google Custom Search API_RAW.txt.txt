{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - soul  synonyms",
                "totalResults": "36800000",
                "searchTerms": "soul  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - soul  synonyms",
                "totalResults": "36800000",
                "searchTerms": "soul  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.498426,
        "formattedSearchTime": "0.50",
        "totalResults": "36800000",
        "formattedTotalResults": "36,800,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Soul Synonyms, Soul Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Soul Synonyms</b>, Soul Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/soul",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · conscience · heart · intellect · intelligence · life · mind · stuff · vitality\n.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; conscience &middot; heart &middot; intellect &middot; intelligence &middot; life &middot; mind &middot; stuff &middot; vitality<br>\n.",
            "cacheId": "XSH6yzTpebgJ",
            "formattedUrl": "https://www.thesaurus.com/browse/soul",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>soul</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of soul | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for soul from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Soul Synonyms, Soul Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Soul Synonyms</b>, Soul Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/soul",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms & Antonyms of soul · 1 an immaterial force within a human being \nthought to give the body life, energy, and power many religions teach that the \nsoul is ...",
            "htmlSnippet": "<b>Synonyms</b> &amp; Antonyms of <b>soul</b> &middot; 1 an immaterial force within a human being <br>\nthought to give the body life, energy, and power many religions teach that the <br>\n<b>soul</b> is&nbsp;...",
            "cacheId": "scBgK9P1nYMJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/soul",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>soul</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for SOUL",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/soul",
                        "og:title": "Thesaurus results for SOUL",
                        "twitter:aria-text": "Share more words for soul on Twitter",
                        "og:aria-text": "Post more words for soul to Facebook",
                        "og:description": "Soul: an immaterial force within a human being thought to give the body life, energy, and power. Synonyms: psyche, spirit, baby… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Soul: an immaterial force within a human being thought to give the body life, energy, and power. Synonyms: psyche, spirit, baby… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/soul"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Souls Synonyms, Souls Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Souls Synonyms</b>, Souls Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/souls",
            "displayLink": "www.thesaurus.com",
            "snippet": "SWAP souls IN A SENTENCE. Join our early testers! See how your sentence \nlooks with different synonyms. TRY NOW. Characters ...",
            "htmlSnippet": "SWAP <b>souls</b> IN A SENTENCE. Join our early testers! See how your sentence <br>\nlooks with different <b>synonyms</b>. TRY NOW. Characters&nbsp;...",
            "cacheId": "X1yMRI6TplIJ",
            "formattedUrl": "https://www.thesaurus.com/browse/souls",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>souls</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of souls | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for souls from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for soul? | Soul Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for soul? | <b>Soul Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/soul.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 4440 synonyms for soul and other similar words that you can use instead \nbased on 17 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 4440 <b>synonyms</b> for <b>soul</b> and other similar words that you can use instead <br>\nbased on 17 separate contexts from our thesaurus.",
            "cacheId": "JUWS345eqAwJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/soul.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>soul</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Soul synonyms | Best 95 synonyms for soul",
            "htmlTitle": "<b>Soul synonyms</b> | Best 95 synonyms for soul",
            "link": "https://thesaurus.yourdictionary.com/soul",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 95 synonyms for soul, including: specter, spirit, love, nub, shadow, \nvision, force, phantom, hant, substance, intellect and more... Find another word \nfor ...",
            "htmlSnippet": "The best 95 <b>synonyms</b> for <b>soul</b>, including: specter, spirit, love, nub, shadow, <br>\nvision, force, phantom, hant, substance, intellect and more... Find another word <br>\nfor&nbsp;...",
            "cacheId": "JuD-lw-IG3cJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/soul",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>soul</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "2 277 Soul synonyms - Other Words for Soul",
            "htmlTitle": "2 277 <b>Soul synonyms</b> - Other Words for Soul",
            "link": "https://www.powerthesaurus.org/soul/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Soul synonyms. Top synonyms for soul (other words for soul) are spirit, individual \nand heart.",
            "htmlSnippet": "<b>Soul synonyms</b>. Top synonyms for soul (other words for soul) are spirit, individual <br>\nand heart.",
            "cacheId": "Ujub5kV2kk4J",
            "formattedUrl": "https://www.powerthesaurus.org/soul/synonyms",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>soul</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "2 277 Soul synonyms - Other Words for Soul",
                        "og:description": "Soul synonyms. Top synonyms for soul (other words for soul) are spirit, individual and heart.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/soul/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Soul | Synonyms of Soul by Lexico",
            "htmlTitle": "<b>Soul</b> | <b>Synonyms</b> of <b>Soul</b> by Lexico",
            "link": "https://www.lexico.com/synonym/soul",
            "displayLink": "www.lexico.com",
            "snippet": "What is the definition of soul? What is the meaning of soul? How do you use soul \nin a sentence? What are synonyms for soul?",
            "htmlSnippet": "What is the definition of <b>soul</b>? What is the meaning of <b>soul</b>? How do you use <b>soul</b> <br>\nin a sentence? What are <b>synonyms</b> for <b>soul</b>?",
            "cacheId": "0gpIWJAJT1MJ",
            "formattedUrl": "https://www.lexico.com/synonym/soul",
            "htmlFormattedUrl": "https://www.lexico.com/<b>synonym</b>/<b>soul</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT-fHqiHsELFzBLFkvhF3MUNoGyDc02pwOeICq9JD-vhUo5TV_0a-_rYOwh",
                        "width": "291",
                        "height": "173"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#50b46c",
                        "og:image": "https://www.lexico.com/lexico-logo.png",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Soul | Synonyms of Soul by Lexico",
                        "og:type": "website",
                        "theme-color": "#50b46c",
                        "og:site_name": "Lexico Dictionaries | English",
                        "og:title": "Soul | Synonyms of Soul by Lexico",
                        "msapplication-tileimage": "/mstile-144x144.png",
                        "csrf-param": "authenticity_token",
                        "og:description": "What is the definition of soul? What is the meaning of soul? How do you use soul in a sentence? What are synonyms for soul?",
                        "twitter:image": "https://www.lexico.com/lexico-logo.png",
                        "viewport": "width=device-width, initial-scale=1, shrink-to-fit=no",
                        "twitter:description": "What is the definition of soul? What is the meaning of soul? How do you use soul in a sentence? What are synonyms for soul?",
                        "csrf-token": "E11FZXRVv4jUz0ePcV9Fb0aI98HSPU9GJmvmKQwR6Siql1Qp7zJkJ5qNwUoaHXI6nQeNt8pkInGfpelkrSX5kA==",
                        "og:url": "https://www.lexico.com/synonym/soul"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.lexico.com/lexico-logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Soul Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Soul Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/soul",
            "displayLink": "www.synonyms.com",
            "snippet": "Princeton's WordNet(0.00 / 0 votes)Rate these synonyms · soul, psyche(noun) \nthe immaterial part of a person; the actuating cause of an individual life · person,\n ...",
            "htmlSnippet": "Princeton&#39;s WordNet(0.00 / 0 votes)Rate these <b>synonyms</b> &middot; <b>soul</b>, psyche(noun) <br>\nthe immaterial part of a person; the actuating cause of an individual life &middot; person,<br>\n&nbsp;...",
            "cacheId": "4Tr-sslJPK8J",
            "formattedUrl": "https://www.synonyms.com/synonym/soul",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>soul</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/soul",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for SOUL - Thesaurus.net",
            "htmlTitle": "<b>Synonyms</b> for <b>SOUL</b> - Thesaurus.net",
            "link": "https://www.thesaurus.net/soul",
            "displayLink": "www.thesaurus.net",
            "snippet": "Synonyms for SOUL: vital force, life force, Gravamen, divine spark, human being, \npith, gist, stuff, vitality, quintessence, nub, creature, breath, man, bosom, ...",
            "htmlSnippet": "<b>Synonyms</b> for <b>SOUL</b>: vital force, life force, Gravamen, divine spark, human being, <br>\npith, gist, stuff, vitality, quintessence, nub, creature, breath, man, bosom,&nbsp;...",
            "cacheId": "imAWyrwD9bEJ",
            "formattedUrl": "https://www.thesaurus.net/soul",
            "htmlFormattedUrl": "https://www.thesaurus.net/<b>soul</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTHmPbL6L3tWwemgnxXK_eIzcEGEcFI39SJD35kF79sVRInWi-xNtX9sw",
                        "width": "312",
                        "height": "53"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.net/public/desktop/images/logo.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for SOUL, Antonyms for SOUL - Thesaurus.net",
                        "og:site_name": "Thesaurus.net",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "soul | synonyms: vital force, life force, Gravamen, divine spark, human being, pith, gist, stuff, vitality, quintessence, nub, creature, breath, man, bosom, personage, root, temper, frame of mind, sensation, undercurrent, passion, your inner self, mood, emotion, breast, party, sap, individual, nephesh, jet, anima humana, anima, person, buddhi, somebody, someone, chat, human, heartstrings, spiritus, tempestuous, spook, soulful, vision, apparition, Phantasm, ghost, shade, umbra, ardent, intense, histrionic, wraith, haunt, passionate, impassioned, emotional, shadow, hysterical, torrid, phantom, specter, genius, blasphemous, article of faith, ego, force, angelic, archangel, blasphemy, blaspheme, cause, substance, blessing, angel, life, principle, individuality, bless, personality, absolution, poetry, philosophy, idealism, acid jazz, affection, bhangra, backing, heroism, acid house, ambient, courage, bebop, background music, backup, culture, blues, art, honor, bluegrass, reverence, duty, love, feature, be, factor,",
                        "og:title": "Synonyms for SOUL, Antonyms for SOUL - Thesaurus.net",
                        "og:url": "https://www.thesaurus.net/soul",
                        "og:description": "soul | synonyms: vital force, life force, Gravamen, divine spark, human being, pith, gist, stuff, vitality, quintessence, nub, creature, breath, man, bosom, personage, root, temper, frame of mind, sensation, undercurrent, passion, your inner self, mood, emotion, breast, party, sap, individual, nephesh, jet, anima humana, anima, person, buddhi, somebody, someone, chat, human, heartstrings, spiritus, tempestuous, spook, soulful, vision, apparition, Phantasm, ghost, shade, umbra, ardent, intense, histrionic, wraith, haunt, passionate, impassioned, emotional, shadow, hysterical, torrid, phantom, specter, genius, blasphemous, article of faith, ego, force, angelic, archangel, blasphemy, blaspheme, cause, substance, blessing, angel, life, principle, individuality, bless, personality, absolution, poetry, philosophy, idealism, acid jazz, affection, bhangra, backing, heroism, acid house, ambient, courage, bebop, background music, backup, culture, blues, art, honor, bluegrass, reverence, duty, love, feature, be, factor,"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.net/public/desktop/images/logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Soul Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Soul Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/soul",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for soul: spirit, essence, psyche, life, mind | Collins English \nThesaurus.",
            "htmlSnippet": "Another word for <b>soul</b>: spirit, essence, psyche, life, mind | Collins English <br>\nThesaurus.",
            "cacheId": "RyYhditgn8cJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/soul",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/<b>soul</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Soul Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "77a27d36-9950-40b3-b331-c72bd22fb924",
                        "_csrf_parameter": "_csrf",
                        "title": "Soul Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for soul: spirit, essence, psyche, life, mind | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/soul"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        }
    ]
}
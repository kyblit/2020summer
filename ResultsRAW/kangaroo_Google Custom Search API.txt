{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - kangaroo  synonyms",
                "totalResults": "1210000",
                "searchTerms": "kangaroo  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - kangaroo  synonyms",
                "totalResults": "1210000",
                "searchTerms": "kangaroo  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.2301,
        "formattedSearchTime": "0.23",
        "totalResults": "1210000",
        "formattedTotalResults": "1,210,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Kangaroo Synonyms, Kangaroo Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Kangaroo Synonyms</b>, Kangaroo Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/kangaroo",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for kangaroo. bandicoot · euro · koala · opossum · possum · wallaby ...",
            "htmlSnippet": "<b>Synonyms</b> for <b>kangaroo</b>. bandicoot &middot; euro &middot; koala &middot; opossum &middot; possum &middot; wallaby&nbsp;...",
            "cacheId": "XsVE1pTrE98J",
            "formattedUrl": "https://www.thesaurus.com/browse/kangaroo",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>kangaroo</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of kangaroo | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for kangaroo from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for kangaroo | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> for <b>kangaroo</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/kangaroo",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "dq3mRn9sDbkJ",
            "formattedUrl": "https://www.synonym.com/synonyms/kangaroo",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>kangaroo</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for kangaroo",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms for kangaroo",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "kangaroo | definition: any of several herbivorous leaping marsupials of Australia and New Guinea having large powerful hind legs and a long thick tail | synonyms: Macropus giganteus, giant kangaroo, Macropodidae, family Macropodidae, pouched mammal, musk kangaroo, wallaby, Hypsiprymnodon moschatus, great grey kangaroo, rat kangaroo, kangaroo rat, marsupial, brush kangaroo",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "kangaroo | definition: any of several herbivorous leaping marsupials of Australia and New Guinea having large powerful hind legs and a long thick tail | synonyms: Macropus giganteus, giant kangaroo, Macropodidae, family Macropodidae, pouched mammal, musk kangaroo, wallaby, Hypsiprymnodon moschatus, great grey kangaroo, rat kangaroo, kangaroo rat, marsupial, brush kangaroo",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/kangaroo"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for kangaroo? | Kangaroo Synonyms ...",
            "htmlTitle": "What is another word for kangaroo? | <b>Kangaroo Synonyms</b> ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/kangaroo.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "kangaroo · Noun · A large marsupial with strong hind legs for hopping, native to \nAustralia · (Canada) A hooded jacket with a front pocket, usually of fleece \nmaterial.",
            "htmlSnippet": "<b>kangaroo</b> &middot; Noun &middot; A large marsupial with strong hind legs for hopping, native to <br>\nAustralia &middot; (Canada) A hooded jacket with a front pocket, usually of fleece <br>\nmaterial.",
            "cacheId": "l1qmohVu188J",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/kangaroo.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>kangaroo</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Kangaroo Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Kangaroo Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/kangaroo",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "a subtropical parthenocarpic tree, Carica pentagona , originating in South \nAmerica , cultivated for its fruit: family Caricaceae. SEE FULL DEFINITION.",
            "htmlSnippet": "a subtropical parthenocarpic tree, Carica pentagona , originating in South <br>\nAmerica , cultivated for its fruit: family Caricaceae. SEE FULL DEFINITION.",
            "cacheId": "-ZIlctMg9EMJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/kangaroo",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/<b>kangaroo</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Kangaroo Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "dfd21487-0f48-453e-a1a1-4ea8b809ca4d",
                        "_csrf_parameter": "_csrf",
                        "title": "Kangaroo Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for kangaroo: Collins Dictionary Definition | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/kangaroo"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Kangaroo Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Kangaroo Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/kangaroo",
            "displayLink": "www.synonyms.com",
            "snippet": "Aug 15, 2019 ... Find all the synonyms and alternative words for kangaroo at Synonyms.com, the \nlargest free online thesaurus, antonyms, definitions and ...",
            "htmlSnippet": "Aug 15, 2019 <b>...</b> Find all the <b>synonyms</b> and alternative words for <b>kangaroo</b> at <b>Synonyms</b>.com, the <br>\nlargest free online thesaurus, antonyms, definitions and&nbsp;...",
            "cacheId": "CpBEdULe1GgJ",
            "formattedUrl": "https://www.synonyms.com/synonym/kangaroo",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>kangaroo</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/kangaroo",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "KANGAROO (noun) definition and synonyms | Macmillan Dictionary",
            "htmlTitle": "<b>KANGAROO</b> (noun) definition and <b>synonyms</b> | Macmillan Dictionary",
            "link": "https://www.macmillandictionary.com/us/dictionary/american/kangaroo",
            "displayLink": "www.macmillandictionary.com",
            "snippet": "Definition of KANGAROO (noun): Australian animal carrying its baby in a pouch.",
            "htmlSnippet": "Definition of <b>KANGAROO</b> (noun): Australian animal carrying its baby in a pouch.",
            "cacheId": "XQCUBoA_rvYJ",
            "formattedUrl": "https://www.macmillandictionary.com/us/dictionary/american/kangaroo",
            "htmlFormattedUrl": "https://www.macmillandictionary.com/us/dictionary/american/<b>kangaroo</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcR0HWKXgLtrjwcEhr9W3R55DSkf0mNkq-IJh0fXydES94LYatkazhUucdrF",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/external/images/social/social_macmillan.jpg?version=4.0.27",
                        "og:type": "website",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=10.0, minimum-scale=0.1",
                        "og:title": "KANGAROO (noun) definition and synonyms | Macmillan Dictionary",
                        "og:url": "https://www.macmillandictionary.com/dictionary/british/kangaroo",
                        "og:description": "Definition of KANGAROO (noun): Australian animal carrying its baby in a pouch"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillandictionary.com/external/images/games/poppy-sidebar.jpg?version=4.0.27"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "294 Kangaroo synonyms - Other Words for Kangaroo",
            "htmlTitle": "294 <b>Kangaroo synonyms</b> - Other Words for Kangaroo",
            "link": "https://www.powerthesaurus.org/kangaroo/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Kangaroo synonyms. Top synonyms for kangaroo (other words for kangaroo) are \nwallaby, wallaroo and leaping marsupial.",
            "htmlSnippet": "<b>Kangaroo synonyms</b>. Top synonyms for kangaroo (other words for kangaroo) are <br>\nwallaby, wallaroo and leaping marsupial.",
            "cacheId": "qoKtkwziZOUJ",
            "formattedUrl": "https://www.powerthesaurus.org/kangaroo/synonyms",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>kangaroo</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "294 Kangaroo synonyms - Other Words for Kangaroo",
                        "og:description": "Kangaroo synonyms. Top synonyms for kangaroo (other words for kangaroo) are wallaby, wallaroo and leaping marsupial.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/kangaroo/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for KANGAROO - Thesaurus.net",
            "htmlTitle": "<b>Synonyms</b> for <b>KANGAROO</b> - Thesaurus.net",
            "link": "https://www.thesaurus.net/kangaroo",
            "displayLink": "www.thesaurus.net",
            "snippet": "Synonyms for KANGAROO: duckbilled platypus, ROO, goat antelope, aoudad, \nbrush wolf, peludo, aurochs, suslik, urus, red squirrel, flea, mule deer, mountain ...",
            "htmlSnippet": "<b>Synonyms</b> for <b>KANGAROO</b>: duckbilled platypus, ROO, goat antelope, aoudad, <br>\nbrush wolf, peludo, aurochs, suslik, urus, red squirrel, flea, mule deer, mountain&nbsp;...",
            "cacheId": "RrauoykWOjsJ",
            "formattedUrl": "https://www.thesaurus.net/kangaroo",
            "htmlFormattedUrl": "https://www.thesaurus.net/<b>kangaroo</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTHmPbL6L3tWwemgnxXK_eIzcEGEcFI39SJD35kF79sVRInWi-xNtX9sw",
                        "width": "312",
                        "height": "53"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.net/public/desktop/images/logo.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for KANGAROO - Thesaurus.net",
                        "og:site_name": "Thesaurus.net",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "kangaroo | synonyms: duckbilled platypus, ROO, goat antelope, aoudad, brush wolf, peludo, aurochs, suslik, urus, red squirrel, flea, mule deer, mountain lion, zoril, brown bear, springbok, timber wolf, tatouay, black buck, shrew mole, vaulter, armadillo, takin, grasshopper, great gray kangaroo, tiger cat, badger, black cat, cotton rat, hopper, fox squirrel, bat, carabao, wharf rat, musk ox, serval, binturong, meerkat, mammal, capybara, pine mouse, nilgai, Wallaroo, jackass, woolly mammoth, jerboa, eyra, pronghorn, woodchuck, wildebeest, sitter, roe, kit fox, mammoth, glutton, animal, musk deer, roe deer, harnessed antelope, prairie dog, bettong, black sheep, tamandua, whistler, cinnamon bear, muskrat, tapir, dormouse, ground squirrel, water buffalo, cat-a-mountain, coypu, zebu, brush kangaroo, cotton mouse, ounce, tatou, musk hog, musk kangaroo, carpincho, Jerboas, apar, babysitter, genet, pangolin, salmon, mouflon, kangaroo rat, leopard cat, hurdler, mountain goat, walker, painter, waterbuck, mountain sheep,",
                        "og:title": "Synonyms for KANGAROO - Thesaurus.net",
                        "og:url": "https://www.thesaurus.net/kangaroo",
                        "og:description": "kangaroo | synonyms: duckbilled platypus, ROO, goat antelope, aoudad, brush wolf, peludo, aurochs, suslik, urus, red squirrel, flea, mule deer, mountain lion, zoril, brown bear, springbok, timber wolf, tatouay, black buck, shrew mole, vaulter, armadillo, takin, grasshopper, great gray kangaroo, tiger cat, badger, black cat, cotton rat, hopper, fox squirrel, bat, carabao, wharf rat, musk ox, serval, binturong, meerkat, mammal, capybara, pine mouse, nilgai, Wallaroo, jackass, woolly mammoth, jerboa, eyra, pronghorn, woodchuck, wildebeest, sitter, roe, kit fox, mammoth, glutton, animal, musk deer, roe deer, harnessed antelope, prairie dog, bettong, black sheep, tamandua, whistler, cinnamon bear, muskrat, tapir, dormouse, ground squirrel, water buffalo, cat-a-mountain, coypu, zebu, brush kangaroo, cotton mouse, ounce, tatou, musk hog, musk kangaroo, carpincho, Jerboas, apar, babysitter, genet, pangolin, salmon, mouflon, kangaroo rat, leopard cat, hurdler, mountain goat, walker, painter, waterbuck, mountain sheep,"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.net/public/desktop/images/logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Kangaroo Courts Synonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Kangaroo</b> Courts <b>Synonyms</b> | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/kangaroo%20courts",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Feb 24, 2020 ... 14 synonyms and near synonyms of kangaroo courts from the Merriam-Webster \nThesaurus. Find another word for kangaroo courts.",
            "htmlSnippet": "Feb 24, 2020 <b>...</b> 14 <b>synonyms</b> and near <b>synonyms</b> of <b>kangaroo</b> courts from the Merriam-Webster <br>\nThesaurus. Find another word for <b>kangaroo</b> courts.",
            "cacheId": "bjCLnIX55boJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/kangaroo%20courts",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>kangaroo</b>%20courts",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for KANGAROO COURTS",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/kangaroo+courts",
                        "og:title": "Thesaurus results for KANGAROO COURTS",
                        "twitter:aria-text": "Share more words for kangaroo courts on Twitter",
                        "og:aria-text": "Post more words for kangaroo courts to Facebook",
                        "og:description": "Kangaroo courts: as in inquisitions. Synonyms: inquisitions, courts-martial, drumhead court-martials… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Kangaroo courts: as in inquisitions. Synonyms: inquisitions, courts-martial, drumhead court-martials… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/kangaroo+courts"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Kangaroo Words | Definition and Example - Hitbullseye",
            "htmlTitle": "<b>Kangaroo</b> Words | Definition and Example - Hitbullseye",
            "link": "https://www.hitbullseye.com/Vocab/Kangaroo-Words.php",
            "displayLink": "www.hitbullseye.com",
            "snippet": "Learn more about the kangaroo words with the help of examples. ... For example: \nThe word 'acrid' contains the word 'acid', which is a synonym of 'acrid'; ...",
            "htmlSnippet": "Learn more about the <b>kangaroo</b> words with the help of examples. ... For example: <br>\nThe word &#39;acrid&#39; contains the word &#39;acid&#39;, which is a <b>synonym</b> of &#39;acrid&#39;;&nbsp;...",
            "cacheId": "Ds76e6p5VlQJ",
            "formattedUrl": "https://www.hitbullseye.com/Vocab/Kangaroo-Words.php",
            "htmlFormattedUrl": "https://www.hitbullseye.com/Vocab/<b>Kangaroo</b>-Words.php",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT_OnXIhVQD6trHcg8w7Ctk4H9yS25jwfwwy3omPRKurVEuUvweJWIsUcc",
                        "width": "130",
                        "height": "130"
                    }
                ],
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1",
                        "author": "Bullseye",
                        "keyword": "kangaroo word, kangaroo words list, kangaroo words examples, kangaroo words list pdf, what are kangaroo words"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://s3service.hitbullseye.com/s3fs-public/500x500_freedom_0.jpg?null"
                    }
                ]
            }
        }
    ]
}
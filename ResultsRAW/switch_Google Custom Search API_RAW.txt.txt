{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - switch  synonyms",
                "totalResults": "210000000",
                "searchTerms": "switch  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - switch  synonyms",
                "totalResults": "210000000",
                "searchTerms": "switch  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.364929,
        "formattedSearchTime": "0.36",
        "totalResults": "210000000",
        "formattedTotalResults": "210,000,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Switch Synonyms, Switch Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Switch Synonyms</b>, Switch Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/switch",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for switch · about-face · alteration · reversal · shift · transformation · \nsubstitution · swap · change of direction.",
            "htmlSnippet": "<b>Synonyms</b> for <b>switch</b> &middot; about-face &middot; alteration &middot; reversal &middot; shift &middot; transformation &middot; <br>\nsubstitution &middot; swap &middot; change of direction.",
            "cacheId": "J9S64w-FmRIJ",
            "formattedUrl": "https://www.thesaurus.com/browse/switch",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>switch</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of switch | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for switch from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Switch Synonyms, Switch Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Switch Synonyms</b>, Switch Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/switch",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms & Antonyms of switch · 1 to give up (something) and take something \nelse in return switched the real grapes for fake ones · 2 to move from side to side \nor ...",
            "htmlSnippet": "<b>Synonyms</b> &amp; Antonyms of <b>switch</b> &middot; 1 to give up (something) and take something <br>\nelse in return <b>switched</b> the real grapes for fake ones &middot; 2 to move from side to side <br>\nor&nbsp;...",
            "cacheId": "01cW-ixPjqkJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/switch",
            "htmlFormattedUrl": "https://www.merriam-webster.com/<b>thesaurus</b>/<b>switch</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for SWITCH",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/switch",
                        "og:title": "Thesaurus results for SWITCH",
                        "twitter:aria-text": "Share more words for switch on Twitter",
                        "og:aria-text": "Post more words for switch to Facebook",
                        "og:description": "Switch: to give up (something) and take something else in return. Synonyms: change, commute, exchange… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Switch: to give up (something) and take something else in return. Synonyms: change, commute, exchange… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/switch"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Switching Synonyms, Switching Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Switching Synonyms</b>, Switching Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/switching",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for switching at Thesaurus.com with free online thesaurus, antonyms, \nand definitions. Find descriptive alternatives for switching.",
            "htmlSnippet": "<b>Synonyms</b> for <b>switching</b> at <b>Thesaurus</b>.com with free online <b>thesaurus</b>, antonyms, <br>\nand definitions. Find descriptive alternatives for <b>switching</b>.",
            "cacheId": "BK2_CDuJGCsJ",
            "formattedUrl": "https://www.thesaurus.com/browse/switching",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>switching</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of switching | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for switching from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Switch synonyms | Best 43 synonyms for switch",
            "htmlTitle": "<b>Switch synonyms</b> | Best 43 synonyms for switch",
            "link": "https://thesaurus.yourdictionary.com/switch",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 43 synonyms for switch, including: turnabout, shift, rearrange, change, \nturn, divert, exchange, flog, reversal, shunt, sidetrack and more... Find another ...",
            "htmlSnippet": "The best 43 <b>synonyms</b> for <b>switch</b>, including: turnabout, shift, rearrange, change, <br>\nturn, divert, exchange, flog, reversal, shunt, sidetrack and more... Find another&nbsp;...",
            "cacheId": "0YBSK3SkSewJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/switch",
            "htmlFormattedUrl": "https://<b>thesaurus</b>.yourdictionary.com/<b>switch</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for switch? | Switch Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for switch? | <b>Switch Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/switch.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 6610 synonyms for switch and other similar words that you can use instead \nbased on 33 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 6610 <b>synonyms</b> for <b>switch</b> and other similar words that you can use instead <br>\nbased on 33 separate contexts from our <b>thesaurus</b>.",
            "cacheId": "o8pQa_jJTR4J",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/switch.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>switch</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for switch | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>switch</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/switch",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "kTI8uTubPmsJ",
            "formattedUrl": "https://www.synonym.com/synonyms/switch",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>switch</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for switch",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for switch",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "switch | definition: change over, change around, as to a new order or sequence | synonyms: turn, switch over, change by reversal, exchange, reverse| antonyms: awaken, brighten, personalize, occidentalise, unstring",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "switch | definition: change over, change around, as to a new order or sequence | synonyms: turn, switch over, change by reversal, exchange, reverse| antonyms: awaken, brighten, personalize, occidentalise, unstring",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/switch"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "3 596 Switch Synonyms and 153 Switch Antonyms | Switch in ...",
            "htmlTitle": "3 596 <b>Switch Synonyms</b> and 153 Switch Antonyms | Switch in ...",
            "link": "https://www.powerthesaurus.org/switch",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Synonyms (Other Words) for Switch & Antonyms (Opposite Meaning) for Switch.",
            "htmlSnippet": "<b>Synonyms</b> (Other Words) for <b>Switch</b> &amp; Antonyms (Opposite Meaning) for <b>Switch</b>.",
            "cacheId": "_8NwlX4pXA8J",
            "formattedUrl": "https://www.powerthesaurus.org/switch",
            "htmlFormattedUrl": "https://www.power<b>thesaurus</b>.org/<b>switch</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "3 596 Switch Synonyms and 153 Switch Antonyms | Switch in Thesaurus",
                        "og:description": "Synonyms (Other Words) for Switch & Antonyms (Opposite Meaning) for Switch.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/switch",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Switch Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Switch Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/switch",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for switch: control, button, lever, on/off device | Collins English \nThesaurus.",
            "htmlSnippet": "Another word for <b>switch</b>: control, button, lever, on/off device | Collins English <br>\n<b>Thesaurus</b>.",
            "cacheId": "cHkCs-LdAVUJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/switch",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-<b>thesaurus</b>/<b>switch</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Switch Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "d2210d0c-3aad-45ca-924c-6fbc22d33ff9",
                        "_csrf_parameter": "_csrf",
                        "title": "Switch Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for switch: control, button, lever, on/off device | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/switch"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "switch synonyms with definition | Macmillan Thesaurus",
            "htmlTitle": "<b>switch synonyms</b> with definition | Macmillan Thesaurus",
            "link": "https://www.macmillanthesaurus.com/switch",
            "displayLink": "www.macmillanthesaurus.com",
            "snippet": "Synonyms for 'switch': change, make into, convert, transform, modify, turn into, \nalter, adapt, reverse, revise, adjust.",
            "htmlSnippet": "<b>Synonyms</b> for &#39;<b>switch</b>&#39;: change, make into, convert, transform, modify, turn into, <br>\nalter, adapt, reverse, revise, adjust.",
            "cacheId": "DPULXMTWG5EJ",
            "formattedUrl": "https://www.macmillanthesaurus.com/switch",
            "htmlFormattedUrl": "https://www.macmillan<b>thesaurus</b>.com/<b>switch</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTvi2fKGyVfH8J_NZJPZvttg3WD5erxxyfmRQT9xohcKNV8Oa4QTCKfDg",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/external/images/logoThesaurus.png?version=1.0.46",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no",
                        "og:title": "switch synonyms with definition | Macmillan Thesaurus",
                        "title": "switch synonyms with definition | Macmillan Thesaurus",
                        "og:url": "https://www.macmillanthesaurus.com/switch",
                        "og:description": "Synonyms for 'switch': change, make into, convert, transform, modify, turn into, alter, adapt, reverse, revise, adjust"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillanthesaurus.com/external/images/bg-wave.svg?version=1.0.46"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Reverso - switch between synonym | English synonyms dictionary",
            "htmlTitle": "Reverso - switch between synonym | English synonyms dictionary",
            "link": "https://dictionary.reverso.net/english-synonyms/switch+between",
            "displayLink": "dictionary.reverso.net",
            "snippet": "switch between synonyms and antonyms in the English synonyms dictionary, see \nalso 'swift',swish',sit',swiftly', definition. Understand switch between meaning ...",
            "htmlSnippet": "<b>switch</b> between <b>synonyms</b> and antonyms in the English <b>synonyms</b> dictionary, see <br>\nalso &#39;swift&#39;,swish&#39;,sit&#39;,swiftly&#39;, definition. Understand <b>switch</b> between meaning&nbsp;...",
            "cacheId": "5LNvqZfOcdIJ",
            "formattedUrl": "https://dictionary.reverso.net/english-synonyms/switch+between",
            "htmlFormattedUrl": "https://dictionary.reverso.net/english-<b>synonyms</b>/<b>switch</b>+between",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width,maximum-scale=1.0"
                    }
                ]
            }
        }
    ]
}
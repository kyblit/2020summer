{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - moscow  synonyms",
                "totalResults": "2560000",
                "searchTerms": "moscow  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - moscow  synonyms",
                "totalResults": "2560000",
                "searchTerms": "moscow  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.228182,
        "formattedSearchTime": "0.23",
        "totalResults": "2560000",
        "formattedTotalResults": "2,560,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Synonyms for moscow | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> for <b>moscow</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/moscow",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "VpyU7vFDntMJ",
            "formattedUrl": "https://www.synonym.com/synonyms/moscow",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>moscow</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSNPaQwyj2t_vB9GfyNeHSwmvM09gteOLaZBnBpO4dPbZHGXuZcGOBvQKFb",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for moscow",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms for moscow",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "moscow | definition: a city of central European Russia; formerly capital of both the Soviet Union and Soviet Russia; since 1991 the capital of the Russian Federation | synonyms: capital of the Russian Federation, Kremlin, Russian capital, Russian Federation, Russia",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "moscow | definition: a city of central European Russia; formerly capital of both the Soviet Union and Soviet Russia; since 1991 the capital of the Russian Federation | synonyms: capital of the Russian Federation, Kremlin, Russian capital, Russian Federation, Russia",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/moscow"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/dist/img/wordtigo-logo.987687df.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "62 Moscow synonyms - Other Words for Moscow",
            "htmlTitle": "62 <b>Moscow synonyms</b> - Other Words for Moscow",
            "link": "https://www.powerthesaurus.org/moscow/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Moscow synonyms. Top synonyms for moscow (other words for moscow) are \nmoskva, moskow and muscovy.",
            "htmlSnippet": "<b>Moscow synonyms</b>. Top synonyms for moscow (other words for moscow) are <br>\nmoskva, moskow and muscovy.",
            "cacheId": "SpOoqt-HQhAJ",
            "formattedUrl": "https://www.powerthesaurus.org/moscow/synonyms",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>moscow</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "62 Moscow synonyms - Other Words for Moscow",
                        "og:description": "Moscow synonyms. Top synonyms for moscow (other words for moscow) are moskva, moskow and muscovy.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/moscow/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Moscow Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Moscow Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/moscow",
            "displayLink": "www.synonyms.com",
            "snippet": "Find all the synonyms and alternative words for moscow at Synonyms.com, the \nlargest free online thesaurus, antonyms, definitions and translations resource on\n ...",
            "htmlSnippet": "Find all the <b>synonyms</b> and alternative words for <b>moscow</b> at <b>Synonyms</b>.com, the <br>\nlargest free online thesaurus, antonyms, definitions and translations resource on<br>\n&nbsp;...",
            "cacheId": "rM4LpJkiQ9UJ",
            "formattedUrl": "https://www.synonyms.com/synonym/moscow",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>moscow</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/moscow",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for Moscow? | Moscow Synonyms ...",
            "htmlTitle": "What is another word for Moscow? | <b>Moscow Synonyms</b> ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/Moscow.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 10 synonyms for Moscow and other similar words that you can use instead \nfrom our thesaurus.",
            "htmlSnippet": "Find 10 <b>synonyms</b> for <b>Moscow</b> and other similar words that you can use instead <br>\nfrom our thesaurus.",
            "cacheId": "sSJ3ALbyUNwJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/Moscow.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>Moscow</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Moscow synonyms | Best 3 synonyms for moscow",
            "htmlTitle": "<b>Moscow synonyms</b> | Best 3 synonyms for moscow",
            "link": "https://thesaurus.yourdictionary.com/moscow",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "Find another word for moscow. In this page you can discover 3 synonyms, \nantonyms, idiomatic expressions, and related words for moscow, like: moskva, \ncapital ...",
            "htmlSnippet": "Find another word for <b>moscow</b>. In this page you can discover 3 <b>synonyms</b>, <br>\nantonyms, idiomatic expressions, and related words for <b>moscow</b>, like: moskva, <br>\ncapital&nbsp;...",
            "cacheId": "l2kFLx58xiUJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/moscow",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>moscow</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Moscow synonyms, Moscow antonyms - FreeThesaurus.com",
            "htmlTitle": "<b>Moscow synonyms</b>, Moscow antonyms - FreeThesaurus.com",
            "link": "https://www.freethesaurus.com/Moscow",
            "displayLink": "www.freethesaurus.com",
            "snippet": "Synonyms for Moscow in Free Thesaurus. Antonyms for Moscow. 2 synonyms for \nMoscow: capital of the Russian Federation, Russian capital. What are ...",
            "htmlSnippet": "<b>Synonyms</b> for <b>Moscow</b> in Free Thesaurus. Antonyms for <b>Moscow</b>. 2 <b>synonyms</b> for <br>\n<b>Moscow</b>: capital of the Russian Federation, Russian capital. What are&nbsp;...",
            "cacheId": "WRaXYIX1NTYJ",
            "formattedUrl": "https://www.freethesaurus.com/Moscow",
            "htmlFormattedUrl": "https://www.freethesaurus.com/<b>Moscow</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYdkuI0uE9gcqSkj_ayX2U9j3w9PMpjL9KOS4mDt6h2s29lFotin4do3Q",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "http://img.tfd.com/TFDlogo1200x1200.png",
                        "msapplication-packagefamilyname": "Farlex.581429F59E1D8_wyegy4e46y996",
                        "apple-itunes-app": "app-id=379450383, app-argument=thefreedictionary://search/Moscow?",
                        "og:type": "article",
                        "og:image:width": "1200",
                        "og:site_name": "TheFreeDictionary.com",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Moscow",
                        "og:image:height": "1200",
                        "og:url": "https://www.freethesaurus.com/Moscow",
                        "og:description": "Moscow synonyms, antonyms, and related words in the Free Thesaurus",
                        "msapplication-id": "Farlex.581429F59E1D8"
                    }
                ],
                "cse_image": [
                    {
                        "src": "http://img.tfd.com/TFDlogo1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for MOSCOW MULE - Thesaurus.net",
            "htmlTitle": "<b>Synonyms</b> for <b>MOSCOW</b> MULE - Thesaurus.net",
            "link": "https://www.thesaurus.net/moscow%20mule",
            "displayLink": "www.thesaurus.net",
            "snippet": "Synonyms for: cocktail, catapult, cut, deep-dish pie, hopper, ice chest, rum \ncocktail, scarf bandage, shoemaker, slingshot, tank, triangular bandage, \nslingback, ...",
            "htmlSnippet": "<b>Synonyms</b> for: cocktail, catapult, cut, deep-dish pie, hopper, ice chest, rum <br>\ncocktail, scarf bandage, shoemaker, slingshot, tank, triangular bandage, <br>\nslingback,&nbsp;...",
            "cacheId": "dDwhaUiyD7IJ",
            "formattedUrl": "https://www.thesaurus.net/moscow%20mule",
            "htmlFormattedUrl": "https://www.thesaurus.net/<b>moscow</b>%20mule",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTHmPbL6L3tWwemgnxXK_eIzcEGEcFI39SJD35kF79sVRInWi-xNtX9sw",
                        "width": "312",
                        "height": "53"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.net/public/desktop/images/logo.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for MOSCOW MULE - Thesaurus.net",
                        "og:site_name": "Thesaurus.net",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Moscow mule | synonyms: cocktail, catapult, cut, deep-dish pie, hopper, ice chest, rum cocktail, scarf bandage, shoemaker, slingshot, tank, triangular bandage, slingback, collins, hot toddy, pink lady, Cuba Libre, brandy alexander, Bloody Mary, Irish Coffee, julep, rusty nail, cobbler, mixed drink, martini, planter's punch, Tom Collins, stinger, manhattan, sling, mint julep, Rob Roy, daiquiri, highball, buttered rum, grasshopper, cooler, lime rickey, gin fizz, eggnog, toddy, cocktail",
                        "og:title": "Synonyms for MOSCOW MULE - Thesaurus.net",
                        "og:url": "https://www.thesaurus.net/moscow%20mule",
                        "og:description": "Moscow mule | synonyms: cocktail, catapult, cut, deep-dish pie, hopper, ice chest, rum cocktail, scarf bandage, shoemaker, slingshot, tank, triangular bandage, slingback, collins, hot toddy, pink lady, Cuba Libre, brandy alexander, Bloody Mary, Irish Coffee, julep, rusty nail, cobbler, mixed drink, martini, planter's punch, Tom Collins, stinger, manhattan, sling, mint julep, Rob Roy, daiquiri, highball, buttered rum, grasshopper, cooler, lime rickey, gin fizz, eggnog, toddy, cocktail"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.net/public/desktop/images/logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Moscow: Synonyms in English",
            "htmlTitle": "<b>Moscow</b>: <b>Synonyms</b> in English",
            "link": "https://www.interglot.com/dictionary/en/en/translate/Moscow",
            "displayLink": "www.interglot.com",
            "snippet": "the Moscow. – a city of central European Russia; formerly capital of both the \nSoviet Union and Soviet Russia; since 1991 the capital of the Russian Federation \n1.",
            "htmlSnippet": "the <b>Moscow</b>. – a city of central European Russia; formerly capital of both the <br>\nSoviet Union and Soviet Russia; since 1991 the capital of the Russian Federation <br>\n<sup>1</sup>.",
            "cacheId": "cslVgrkwrKcJ",
            "formattedUrl": "https://www.interglot.com/dictionary/en/en/translate/Moscow",
            "htmlFormattedUrl": "https://www.interglot.com/dictionary/en/en/translate/<b>Moscow</b>",
            "pagemap": {
                "metatags": [
                    {
                        "msapplication-config": "none",
                        "apple-itunes-app": "app-id=903830725",
                        "viewport": "width=1000",
                        "msvalidate.01": "DA42BABE7755F305855F807430FB2801",
                        "language": "en",
                        "y_key": "80132db1b98eee2f",
                        "format-detection": "telephone=no"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Church of Moscow synonyms, Church of Moscow antonyms ...",
            "htmlTitle": "Church of <b>Moscow synonyms</b>, Church of Moscow antonyms ...",
            "link": "https://www.freethesaurus.com/Church+of+Moscow",
            "displayLink": "www.freethesaurus.com",
            "snippet": "Synonyms for Church of Moscow in Free Thesaurus. Antonyms for Church of \nMoscow. 5 words related to Russian Orthodox Church: Eastern Church, Eastern ...",
            "htmlSnippet": "<b>Synonyms</b> for Church of <b>Moscow</b> in Free Thesaurus. Antonyms for Church of <br>\n<b>Moscow</b>. 5 words related to Russian Orthodox Church: Eastern Church, Eastern&nbsp;...",
            "cacheId": "12eEuUbW3xcJ",
            "formattedUrl": "https://www.freethesaurus.com/Church+of+Moscow",
            "htmlFormattedUrl": "https://www.freethesaurus.com/Church+of+<b>Moscow</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYdkuI0uE9gcqSkj_ayX2U9j3w9PMpjL9KOS4mDt6h2s29lFotin4do3Q",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-packagefamilyname": "Farlex.581429F59E1D8_wyegy4e46y996",
                        "apple-itunes-app": "app-id=379450383, app-argument=thefreedictionary://search/Church+of+Moscow?",
                        "og:image": "http://img.tfd.com/TFDlogo1200x1200.png",
                        "og:type": "article",
                        "og:image:width": "1200",
                        "og:site_name": "TheFreeDictionary.com",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Church of Moscow",
                        "og:image:height": "1200",
                        "og:url": "https://www.freethesaurus.com/Church+of+Moscow",
                        "msapplication-id": "Farlex.581429F59E1D8",
                        "og:description": "Church of Moscow synonyms, antonyms, and related words in the Free Thesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "http://img.tfd.com/TFDlogo1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for Moscow | Moscow synonyms - ISYNONYM.COM",
            "htmlTitle": "Synonyms for Moscow | <b>Moscow synonyms</b> - ISYNONYM.COM",
            "link": "http://isynonym.com/en/moscow",
            "displayLink": "isynonym.com",
            "snippet": "On this page you will find all the synonyms for the word to Moscow. Synonyms for \nMoscow - moskva, russian capital and others.",
            "htmlSnippet": "On this page you will find all the synonyms for the word to <b>Moscow</b>. <b>Synonyms</b> for <br>\nMoscow - moskva, russian capital and others.",
            "cacheId": "RxYyKYVFCW8J",
            "formattedUrl": "isynonym.com/en/moscow",
            "htmlFormattedUrl": "i<b>synonym</b>.com/en/<b>moscow</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTo2NC8iOxec16hEe4fTjLc_VnsjafE8vh0u1rPqcq2sqrLvosnfpn5IBk",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "viewport": "initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no"
                    }
                ],
                "cse_image": [
                    {
                        "src": "http://dummyimage.com/800x800/fafafa/d9534f.png&text=Synonyms+for+Moscow"
                    }
                ],
                "listitem": [
                    {
                        "item": "Home",
                        "name": "Home"
                    },
                    {
                        "name": "Moscow"
                    }
                ]
            }
        }
    ]
}
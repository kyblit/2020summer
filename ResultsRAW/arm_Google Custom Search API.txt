{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - arm  synonyms",
                "totalResults": "66100000",
                "searchTerms": "arm  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - arm  synonyms",
                "totalResults": "66100000",
                "searchTerms": "arm  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.287566,
        "formattedSearchTime": "0.29",
        "totalResults": "66100000",
        "formattedTotalResults": "66,100,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Arm Synonyms, Arm Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Arm Synonyms</b>, Arm Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/arm",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · branch · rod · wing · bough · bow · handle · projection · stump.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; branch &middot; rod &middot; wing &middot; bough &middot; bow &middot; handle &middot; projection &middot; stump.",
            "cacheId": "gSBdYvaYktQJ",
            "formattedUrl": "https://www.thesaurus.com/browse/arm",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>arm</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of arm | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for arm from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Arm Synonyms, Arm Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Arm Synonyms</b>, Arm Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/arm",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms of arm · 1 a large unit of a governmental, business, or educational \norganization · 2 a part of a body of water that extends beyond the general \nshoreline · 3 ...",
            "htmlSnippet": "<b>Synonyms</b> of <b>arm</b> &middot; 1 a large unit of a governmental, business, or educational <br>\norganization &middot; 2 a part of a body of water that extends beyond the general <br>\nshoreline &middot; 3&nbsp;...",
            "cacheId": "d2yVS32cuqQJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/arm",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>arm</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for ARM",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/arm",
                        "og:title": "Thesaurus results for ARM",
                        "twitter:aria-text": "Share more words for arm on Twitter",
                        "og:aria-text": "Post more words for arm to Facebook",
                        "og:description": "Arm: a portable weapon from which a shot is discharged by gunpowder. Synonyms: firearm, gun, heat… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Arm: a portable weapon from which a shot is discharged by gunpowder. Synonyms: firearm, gun, heat… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/arm"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Arms Synonyms, Arms Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Arms Synonyms</b>, Arms Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/arms",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · armaments · artillery · equipment · firearms · munitions · ordnance \n· panoply · weapons.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; armaments &middot; artillery &middot; equipment &middot; firearms &middot; munitions &middot; ordnance <br>\n&middot; panoply &middot; weapons.",
            "cacheId": "rYGnQpmypk8J",
            "formattedUrl": "https://www.thesaurus.com/browse/arms",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>arms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of arms | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for arms from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Arm synonyms | Best 100 synonyms for arm",
            "htmlTitle": "<b>Arm synonyms</b> | Best 100 synonyms for arm",
            "link": "https://thesaurus.yourdictionary.com/arm",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 100 synonyms for arm, including: soupbone, gun, appendage, forelimb, \nforearm, fin, bend, crook, projection, cylinder, branch and more... Find another ...",
            "htmlSnippet": "The best 100 <b>synonyms</b> for <b>arm</b>, including: soupbone, gun, appendage, forelimb, <br>\nforearm, fin, bend, crook, projection, cylinder, branch and more... Find another&nbsp;...",
            "cacheId": "zjJYgbBqG1YJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/arm",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>arm</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Arming Synonyms, Arming Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Arming Synonyms</b>, Arming Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/arming",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for arming at Thesaurus.com with free online thesaurus, antonyms, \nand definitions. Find descriptive alternatives for arming.",
            "htmlSnippet": "<b>Synonyms</b> for <b>arming</b> at Thesaurus.com with free online thesaurus, antonyms, <br>\nand definitions. Find descriptive alternatives for <b>arming</b>.",
            "cacheId": "xvd_olhKuG8J",
            "formattedUrl": "https://www.thesaurus.com/browse/arming",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>arming</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of arming | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for arming from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for arm? | Arm Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for arm? | <b>Arm Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/arm.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 3765 synonyms for arm and other similar words that you can use instead \nbased on 28 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 3765 <b>synonyms</b> for <b>arm</b> and other similar words that you can use instead <br>\nbased on 28 separate contexts from our thesaurus.",
            "cacheId": "imFITMV8bq0J",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/arm.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>arm</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for arm | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>arm</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/arm",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "2RaxwOKA-WEJ",
            "formattedUrl": "https://www.synonym.com/synonyms/arm",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>arm</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for arm",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for arm",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "arm | definition: a human limb; technically the part of the superior limb between the shoulder and the elbow but commonly used to refer to the whole superior limb | synonyms: elbow, ulnar nerve, man, articulatio radiocarpea, humerus, carpus, cubital joint, biceps brachii, manus, limb, arm bone, arteria brachialis, elbow joint, nervus ulnaris, musculus biceps brachii, vena cephalica, brachial artery, radiocarpal joint, body, human being, cubital nerve, triceps brachii, homo, paw, cephalic vein, human, human elbow, mitt, musculus triceps brachii, wrist, articulatio cubiti, wrist joint, physical structure, cubitus, forearm, hand, organic structure, biceps humeri| antonyms: thick, thin, unbreakableness, breakableness, solidity",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "arm | definition: a human limb; technically the part of the superior limb between the shoulder and the elbow but commonly used to refer to the whole superior limb | synonyms: elbow, ulnar nerve, man, articulatio radiocarpea, humerus, carpus, cubital joint, biceps brachii, manus, limb, arm bone, arteria brachialis, elbow joint, nervus ulnaris, musculus biceps brachii, vena cephalica, brachial artery, radiocarpal joint, body, human being, cubital nerve, triceps brachii, homo, paw, cephalic vein, human, human elbow, mitt, musculus triceps brachii, wrist, articulatio cubiti, wrist joint, physical structure, cubitus, forearm, hand, organic structure, biceps humeri| antonyms: thick, thin, unbreakableness, breakableness, solidity",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/arm"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Arm arm Synonyms, Arm arm Antonyms | Thesaurus.com",
            "htmlTitle": "Arm <b>arm Synonyms</b>, Arm arm Antonyms | Thesaurus.com",
            "link": "http://www.thesaurus.com/browse/arm%20arm",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for arm arm at Thesaurus.com with free online thesaurus, antonyms, \nand definitions. Find descriptive alternatives for arm arm.",
            "htmlSnippet": "<b>Synonyms</b> for <b>arm arm</b> at Thesaurus.com with free online thesaurus, antonyms, <br>\nand definitions. Find descriptive alternatives for <b>arm arm</b>.",
            "cacheId": "YwcfnuCS0oAJ",
            "formattedUrl": "www.thesaurus.com/browse/arm%20arm",
            "htmlFormattedUrl": "www.thesaurus.com/browse/<b>arm</b>%20<b>arm</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcREL5kkrBHJzGmuPD739-12O18ac02ZD57UD_aqlfEB_myDUi85pa4r3g",
                        "width": "64",
                        "height": "64"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-208ba0c138c78ab3718720567519407e.png",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "og:title": "I found great synonyms for \"arm arm\" on the new Thesaurus.com!",
                        "fb:admins": "100000304287730,109125464873"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-208ba0c138c78ab3718720567519407e.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Arm Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Arm Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/arm",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for arm: upper limb, limb, appendage | Collins English Thesaurus.",
            "htmlSnippet": "Another word for <b>arm</b>: upper limb, limb, appendage | Collins English Thesaurus.",
            "cacheId": "I72IkjE08oYJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/arm",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/<b>arm</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Arm Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "cb46bc99-b678-42d1-b3af-d054ab6dab6e",
                        "_csrf_parameter": "_csrf",
                        "title": "Arm Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for arm: upper limb, limb, appendage | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/arm"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Soldier at arm Synonyms, Soldier at arm Antonyms | Thesaurus.com",
            "htmlTitle": "Soldier at <b>arm Synonyms</b>, Soldier at arm Antonyms | Thesaurus.com",
            "link": "https://thesaurus.com/browse/soldier%20at%20arm",
            "displayLink": "thesaurus.com",
            "snippet": "Synonyms for soldier at arm at Thesaurus.com with free online thesaurus, \nantonyms, and definitions. Find descriptive alternatives for soldier at arm.",
            "htmlSnippet": "<b>Synonyms</b> for soldier at <b>arm</b> at Thesaurus.com with free online thesaurus, <br>\nantonyms, and definitions. Find descriptive alternatives for soldier at <b>arm</b>.",
            "cacheId": "8CzEsxu8AY8J",
            "formattedUrl": "https://thesaurus.com/browse/soldier%20at%20arm",
            "htmlFormattedUrl": "https://thesaurus.com/browse/soldier%20at%20<b>arm</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcREL5kkrBHJzGmuPD739-12O18ac02ZD57UD_aqlfEB_myDUi85pa4r3g",
                        "width": "64",
                        "height": "64"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-208ba0c138c78ab3718720567519407e.png",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "og:title": "I found great synonyms for \"soldier at arm\" on the new Thesaurus.com!",
                        "fb:admins": "100000304287730,109125464873"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-208ba0c138c78ab3718720567519407e.png"
                    }
                ]
            }
        }
    ]
}
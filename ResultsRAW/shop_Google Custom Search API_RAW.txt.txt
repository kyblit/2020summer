{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - shop  synonyms",
                "totalResults": "105000000",
                "searchTerms": "shop  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - shop  synonyms",
                "totalResults": "105000000",
                "searchTerms": "shop  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.302663,
        "formattedSearchTime": "0.30",
        "totalResults": "105000000",
        "formattedTotalResults": "105,000,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Shop Synonyms, Shop Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Shop Synonyms</b>, Shop Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/shop",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · boutique · chain · department store · emporium · market · mill · \noutlet · store.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; boutique &middot; chain &middot; department store &middot; emporium &middot; market &middot; mill &middot; <br>\noutlet &middot; store.",
            "cacheId": "fkCAqngIGBYJ",
            "formattedUrl": "https://www.thesaurus.com/browse/shop",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>shop</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of shop | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for shop from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Shop Synonyms, Shop Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Shop Synonyms</b>, Shop Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/shop",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms of shop · 1 also shoppe an establishment where goods are sold to \nconsumers the only shop that has that video game in stock is halfway across the \nstate.",
            "htmlSnippet": "<b>Synonyms</b> of <b>shop</b> &middot; 1 also shoppe an establishment where goods are sold to <br>\nconsumers the only <b>shop</b> that has that video game in stock is halfway across the <br>\nstate.",
            "cacheId": "q1IDL3HX2_wJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/shop",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>shop</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for SHOP",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/shop",
                        "og:title": "Thesaurus results for SHOP",
                        "twitter:aria-text": "Share more words for shop on Twitter",
                        "og:aria-text": "Post more words for shop to Facebook",
                        "og:description": "Shop: an establishment where goods are sold to consumers. Synonyms: bazaar, emporium, store… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Shop: an establishment where goods are sold to consumers. Synonyms: bazaar, emporium, store… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/shop"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for shop? | Shop Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for shop? | <b>Shop Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/shop.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 3248 synonyms for shop and other similar words that you can use instead \nbased on 15 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 3248 <b>synonyms</b> for <b>shop</b> and other similar words that you can use instead <br>\nbased on 15 separate contexts from our thesaurus.",
            "cacheId": "_rrWHriZeYYJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/shop.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>shop</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Shop synonyms | Best 45 synonyms for shop",
            "htmlTitle": "<b>Shop synonyms</b> | Best 45 synonyms for shop",
            "link": "https://thesaurus.yourdictionary.com/shop",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 45 synonyms for shop, including: studio, office, sell, patronize, buy at, \nstore, department-store, retail store, novelty shop, workshop, repair shop and ...",
            "htmlSnippet": "The best 45 <b>synonyms</b> for <b>shop</b>, including: studio, office, sell, patronize, buy at, <br>\nstore, department-store, retail store, novelty <b>shop</b>, workshop, repair <b>shop</b> and&nbsp;...",
            "cacheId": "AtxjE-QODGQJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/shop",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>shop</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "2 272 Shop synonyms - Other Words for Shop",
            "htmlTitle": "2 272 <b>Shop synonyms</b> - Other Words for Shop",
            "link": "https://www.powerthesaurus.org/shop/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Shop synonyms. Top synonyms for shop (other words for shop) are store, \nshopping and market.",
            "htmlSnippet": "<b>Shop synonyms</b>. Top synonyms for shop (other words for shop) are store, <br>\nshopping and market.",
            "cacheId": "539TLMBdWn0J",
            "formattedUrl": "https://www.powerthesaurus.org/shop/synonyms",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>shop</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "2 272 Shop synonyms - Other Words for Shop",
                        "og:description": "Shop synonyms. Top synonyms for shop (other words for shop) are store, shopping and market.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/shop/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "types of shop - synonyms and related words | Macmillan Dictionary",
            "htmlTitle": "types of <b>shop</b> - <b>synonyms</b> and related words | Macmillan Dictionary",
            "link": "https://www.macmillandictionary.com/us/thesaurus-category/american/types-of-shop",
            "displayLink": "www.macmillandictionary.com",
            "snippet": "butcher. noun. someone whose job is to sell meat and sometimes also to kill \nanimals for meat. The shop they work in is called a butcher's or a butcher's shop.",
            "htmlSnippet": "butcher. noun. someone whose job is to sell meat and sometimes also to kill <br>\nanimals for meat. The <b>shop</b> they work in is called a butcher&#39;s or a butcher&#39;s <b>shop</b>.",
            "cacheId": "xR4h5TYmacsJ",
            "formattedUrl": "https://www.macmillandictionary.com/us/thesaurus-category/.../types-of-shop",
            "htmlFormattedUrl": "https://www.macmillandictionary.com/us/thesaurus-category/.../types-of-<b>shop</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQq1XwZHyhsplz2gqTWfOMdrRK_HTPsdInhEZEectSt8unMIf5yPEGbNA",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/external/images/social/social_macmillan.jpg?version=4.0.27",
                        "og:type": "website",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=10.0, minimum-scale=0.1",
                        "og:title": "types of shop - synonyms and related words | Macmillan Dictionary",
                        "og:url": "https://www.macmillandictionary.com/thesaurus-category/british/types-of-shop",
                        "og:description": "Comprehensive list of synonyms for types of shop, by Macmillan Dictionary and Thesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillandictionary.com/external/images/bg-wave.svg?version=4.0.27"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "shop synonyms with definition | Macmillan Thesaurus",
            "htmlTitle": "<b>shop synonyms</b> with definition | Macmillan Thesaurus",
            "link": "https://www.macmillanthesaurus.com/shop",
            "displayLink": "www.macmillanthesaurus.com",
            "snippet": "Synonyms for 'shop': pop-up, outlet, kiosk, store, emporium, mart, exchange, point \nof sale, parlour, parlor, shoppe, spaza, stockist.",
            "htmlSnippet": "<b>Synonyms</b> for &#39;<b>shop</b>&#39;: pop-up, outlet, kiosk, store, emporium, mart, exchange, point <br>\nof sale, parlour, parlor, shoppe, spaza, stockist.",
            "cacheId": "MhYmOyI0LfMJ",
            "formattedUrl": "https://www.macmillanthesaurus.com/shop",
            "htmlFormattedUrl": "https://www.macmillanthesaurus.com/<b>shop</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTvi2fKGyVfH8J_NZJPZvttg3WD5erxxyfmRQT9xohcKNV8Oa4QTCKfDg",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/external/images/logoThesaurus.png?version=1.0.46",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no",
                        "og:title": "shop synonyms with definition | Macmillan Thesaurus",
                        "title": "shop synonyms with definition | Macmillan Thesaurus",
                        "og:url": "https://www.macmillanthesaurus.com/shop",
                        "og:description": "Synonyms for 'shop': pop-up, outlet, kiosk, store, emporium, mart, exchange, point of sale, parlour, parlor, shoppe, spaza, stockist"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillanthesaurus.com/external/images/bg-wave.svg?version=1.0.46"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for like a bull in a china shop",
            "htmlTitle": "<b>Synonyms</b> for like a bull in a china <b>shop</b>",
            "link": "https://thesaurus.com/browse/like+bull+in+a+china+shop?jss=1",
            "displayLink": "thesaurus.com",
            "snippet": "Synonyms for like a bull in a china shop at Thesaurus.com with free online \nthesaurus, antonyms, and definitions. Find descriptive alternatives for like a bull \nin a ...",
            "htmlSnippet": "<b>Synonyms</b> for like a bull in a china <b>shop</b> at Thesaurus.com with free online <br>\nthesaurus, antonyms, and definitions. Find descriptive alternatives for like a bull <br>\nin a&nbsp;...",
            "cacheId": "inuyAQ0RM5cJ",
            "formattedUrl": "https://thesaurus.com/browse/like+bull+in+a+china+shop?jss=1",
            "htmlFormattedUrl": "https://thesaurus.com/browse/like+bull+in+a+china+<b>shop</b>?jss=1",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "og:title": "Synonyms of like a bull in a china shop | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for like a bull in a china shop from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Shop | Synonyms of Shop by Lexico",
            "htmlTitle": "<b>Shop</b> | <b>Synonyms</b> of <b>Shop</b> by Lexico",
            "link": "https://www.lexico.com/synonym/shop",
            "displayLink": "www.lexico.com",
            "snippet": "establishment, emporium, department store, supermarket, hypermarket, \nsuperstore, warehouse club, warehouse, factory outlet, chain store, mall, \nshopping mall, ...",
            "htmlSnippet": "establishment, emporium, department store, supermarket, hypermarket, <br>\nsuperstore, warehouse club, warehouse, factory outlet, chain store, mall, <br>\n<b>shopping</b> mall,&nbsp;...",
            "cacheId": "lyKkNeg5uAIJ",
            "formattedUrl": "https://www.lexico.com/synonym/shop",
            "htmlFormattedUrl": "https://www.lexico.com/<b>synonym</b>/<b>shop</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT-fHqiHsELFzBLFkvhF3MUNoGyDc02pwOeICq9JD-vhUo5TV_0a-_rYOwh",
                        "width": "291",
                        "height": "173"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#50b46c",
                        "og:image": "https://www.lexico.com/lexico-logo.png",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Shop | Synonyms of Shop by Lexico",
                        "og:type": "website",
                        "theme-color": "#50b46c",
                        "og:site_name": "Lexico Dictionaries | English",
                        "og:title": "Shop | Synonyms of Shop by Lexico",
                        "msapplication-tileimage": "/mstile-144x144.png",
                        "csrf-param": "authenticity_token",
                        "og:description": "What is the definition of shop? What is the meaning of shop? How do you use shop in a sentence? What are synonyms for shop?",
                        "twitter:image": "https://www.lexico.com/lexico-logo.png",
                        "viewport": "width=device-width, initial-scale=1, shrink-to-fit=no",
                        "twitter:description": "What is the definition of shop? What is the meaning of shop? How do you use shop in a sentence? What are synonyms for shop?",
                        "csrf-token": "JBVc/cWmYdLVq+9lYihDz2vsXtPrRkQwCiWpwWWWiZfSUGfnLHRQ/YwtDt2+MKEDm4aJrw0r1bP/WwJT133qSA==",
                        "og:url": "https://www.lexico.com/synonym/shop"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.lexico.com/lexico-logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for shop | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>shop</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/shop",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "sVHLZ81kSBYJ",
            "formattedUrl": "https://www.synonym.com/synonyms/shop",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>shop</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for shop",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for shop",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "shop | definition: a mercantile establishment for the retail sale of goods or services | synonyms: second-hand store, toyshop, sales outlet, salon, ironmonger's shop, chain store, pizzeria, confectionery, shopfront, pizza shop, bazar, loan office, tobacconist, package store, beauty parlor, clothing store, retail store, flower store, beauty salon, computer store, bodega, bakehouse, mercantile establishment, dry cleaners, gift shop, outfitter, commissary, hat shop, barbershop, canteen, deli, bazaar, junk shop, chemist's, chemist's shop, building supply house, ironmonger, betting shop, thriftshop, meat market, butcher shop, pharmacy, storefront, bookshop, millinery, shoe-shop, store, repair shop, novelty shop, hardware store, tobacconist shop, dress shop, shoe store, candy store, bakeshop, cleaners, fix-it shop, head shop, pawnshop, food shop, beauty parlour, building supply store, off-licence, pizza parlor, perfumery, outlet, florist shop, specialty store, bookstall, bookstore, pet shop, confectionary, delicate",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "shop | definition: a mercantile establishment for the retail sale of goods or services | synonyms: second-hand store, toyshop, sales outlet, salon, ironmonger's shop, chain store, pizzeria, confectionery, shopfront, pizza shop, bazar, loan office, tobacconist, package store, beauty parlor, clothing store, retail store, flower store, beauty salon, computer store, bodega, bakehouse, mercantile establishment, dry cleaners, gift shop, outfitter, commissary, hat shop, barbershop, canteen, deli, bazaar, junk shop, chemist's, chemist's shop, building supply house, ironmonger, betting shop, thriftshop, meat market, butcher shop, pharmacy, storefront, bookshop, millinery, shoe-shop, store, repair shop, novelty shop, hardware store, tobacconist shop, dress shop, shoe store, candy store, bakeshop, cleaners, fix-it shop, head shop, pawnshop, food shop, beauty parlour, building supply store, off-licence, pizza parlor, perfumery, outlet, florist shop, specialty store, bookstall, bookstore, pet shop, confectionary, delicate",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/shop"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        }
    ]
}
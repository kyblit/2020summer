{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - cycle  synonyms",
                "totalResults": "44200000",
                "searchTerms": "cycle  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - cycle  synonyms",
                "totalResults": "44200000",
                "searchTerms": "cycle  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.221684,
        "formattedSearchTime": "0.22",
        "totalResults": "44200000",
        "formattedTotalResults": "44,200,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Cycle Synonyms, Cycle Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Cycle Synonyms</b>, Cycle Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/cycle",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · course · period · revolution · rhythm · round · series · alternation · \nwheel.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; course &middot; period &middot; revolution &middot; rhythm &middot; round &middot; series &middot; alternation &middot; <br>\nwheel.",
            "cacheId": "ocFY-w42g3AJ",
            "formattedUrl": "https://www.thesaurus.com/browse/cycle",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>cycle</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of cycle | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for cycle from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Cycle Synonyms, Cycle Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Cycle Synonyms</b>, Cycle Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/cycle",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms & Antonyms of cycle · 1 a series of events or actions that repeat \nthemselves regularly and in the same order · 2 a long or seemingly long period of \ntime · 3 ...",
            "htmlSnippet": "<b>Synonyms</b> &amp; Antonyms of <b>cycle</b> &middot; 1 a series of events or actions that repeat <br>\nthemselves regularly and in the same order &middot; 2 a long or seemingly long period of <br>\ntime &middot; 3&nbsp;...",
            "cacheId": "rskOe3MG_1gJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/cycle",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>cycle</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for CYCLE",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/cycle",
                        "og:title": "Thesaurus results for CYCLE",
                        "twitter:aria-text": "Share more words for cycle on Twitter",
                        "og:aria-text": "Post more words for cycle to Facebook",
                        "og:description": "Cycle: a series of events or actions that repeat themselves regularly and in the same order. Synonyms: circle, merry-go-round, round… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Cycle: a series of events or actions that repeat themselves regularly and in the same order. Synonyms: circle, merry-go-round, round… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/cycle"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Cycles Synonyms, Cycles Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Cycles Synonyms</b>, Cycles Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/cycles",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for cycles at Thesaurus.com with free online thesaurus, antonyms, and \ndefinitions. Find descriptive alternatives for cycles.",
            "htmlSnippet": "<b>Synonyms</b> for <b>cycles</b> at Thesaurus.com with free online thesaurus, antonyms, and <br>\ndefinitions. Find descriptive alternatives for <b>cycles</b>.",
            "cacheId": "9Roz9j2tFSQJ",
            "formattedUrl": "https://www.thesaurus.com/browse/cycles",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>cycles</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of cycles | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for cycles from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Cycle synonyms | Best 47 synonyms for cycle",
            "htmlTitle": "<b>Cycle synonyms</b> | Best 47 synonyms for cycle",
            "link": "https://thesaurus.yourdictionary.com/cycle",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 47 synonyms for cycle, including: circle, round, course, era, time, \nrevolution of time, sequence, series, ontogenetic, aeon, age and more... Find \nanother ...",
            "htmlSnippet": "The best 47 <b>synonyms</b> for <b>cycle</b>, including: circle, round, course, era, time, <br>\nrevolution of time, sequence, series, ontogenetic, aeon, age and more... Find <br>\nanother&nbsp;...",
            "cacheId": "istjuRAdEjcJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/cycle",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>cycle</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for cycle? | Cycle Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for cycle? | <b>Cycle Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/cycle.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 1595 synonyms for cycle and other similar words that you can use instead \nbased on 14 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 1595 <b>synonyms</b> for <b>cycle</b> and other similar words that you can use instead <br>\nbased on 14 separate contexts from our thesaurus.",
            "cacheId": "sertjbnovG0J",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/cycle.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>cycle</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Cycle Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Cycle Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/cycle",
            "displayLink": "www.synonyms.com",
            "snippet": "Princeton's WordNet(1.00 / 1 vote)Rate these synonyms: · cycle, rhythm, round(\nnoun) an interval during which a recurring sequence of events occurs · cycle(\nnoun).",
            "htmlSnippet": "Princeton&#39;s WordNet(1.00 / 1 vote)Rate these <b>synonyms</b>: &middot; <b>cycle</b>, rhythm, round(<br>\nnoun) an interval during which a recurring sequence of events occurs &middot; <b>cycle</b>(<br>\nnoun).",
            "cacheId": "qYSaMugXXxUJ",
            "formattedUrl": "https://www.synonyms.com/synonym/cycle",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>cycle</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/cycle",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Cycle Synonyms, Cycle Antonyms: Page 3 of 29 | Thesaurus.com",
            "htmlTitle": "<b>Cycle Synonyms</b>, Cycle Antonyms: Page 3 of 29 | Thesaurus.com",
            "link": "http://www.thesaurus.com/browse/cycle/3",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for cycle at Thesaurus.com with free online thesaurus, antonyms, and \ndefinitions. Find descriptive alternatives for cycle.",
            "htmlSnippet": "<b>Synonyms</b> for <b>cycle</b> at Thesaurus.com with free online thesaurus, antonyms, and <br>\ndefinitions. Find descriptive alternatives for <b>cycle</b>.",
            "cacheId": "dWxqJcAwQiMJ",
            "formattedUrl": "www.thesaurus.com/browse/cycle/3",
            "htmlFormattedUrl": "www.thesaurus.com/browse/<b>cycle</b>/3",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of cycle | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for cycle from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "1 514 Cycle Synonyms and 2 Cycle Antonyms | Cycle in Thesaurus",
            "htmlTitle": "1 514 <b>Cycle Synonyms</b> and 2 Cycle Antonyms | Cycle in Thesaurus",
            "link": "https://www.powerthesaurus.org/cycle",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Synonyms (Other Words) for Cycle & Antonyms (Opposite Meaning) for Cycle.",
            "htmlSnippet": "<b>Synonyms</b> (Other Words) for <b>Cycle</b> &amp; Antonyms (Opposite Meaning) for <b>Cycle</b>.",
            "cacheId": "ZqnOzWyovucJ",
            "formattedUrl": "https://www.powerthesaurus.org/cycle",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>cycle</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#ffffff",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "1 514 Cycle Synonyms and 2 Cycle Antonyms | Cycle in Thesaurus",
                        "og:description": "Synonyms (Other Words) for Cycle & Antonyms (Opposite Meaning) for Cycle.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/cycle",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Cycle Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Cycle Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/cycle",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for cycle: series of events, round, circle, revolution, rotation | Collins \nEnglish Thesaurus.",
            "htmlSnippet": "Another word for <b>cycle</b>: series of events, round, circle, revolution, rotation | Collins <br>\nEnglish Thesaurus.",
            "cacheId": "40-0fQd9oc4J",
            "formattedUrl": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/cycle",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/<b>cycle</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Cycle Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "cb51f865-740c-4b38-934a-d24ac5700640",
                        "_csrf_parameter": "_csrf",
                        "title": "Cycle Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for cycle: series of events, round, circle, revolution, rotation | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/cycle"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Spin Cycle: Synonyms, sodas and more – East Bay Times",
            "htmlTitle": "Spin <b>Cycle</b>: <b>Synonyms</b>, sodas and more – East Bay Times",
            "link": "https://www.eastbaytimes.com/2010/04/29/spin-cycle-synonyms-sodas-and-more/",
            "displayLink": "www.eastbaytimes.com",
            "snippet": "Apr 29, 2010 ... Fluffing and folding the news. Exoteric musings. We were browsing Peter \nMeltzer's new book, “The Thinker's Thesaurus: Sophisticated ...",
            "htmlSnippet": "Apr 29, 2010 <b>...</b> Fluffing and folding the news. Exoteric musings. We were browsing Peter <br>\nMeltzer&#39;s new book, “The Thinker&#39;s Thesaurus: Sophisticated&nbsp;...",
            "cacheId": "QRqFPW8r4O4J",
            "formattedUrl": "https://www.eastbaytimes.com/2010/.../spin-cycle-synonyms-sodas-and-more/",
            "htmlFormattedUrl": "https://www.eastbaytimes.com/2010/.../spin-<b>cycle</b>-<b>synonyms</b>-sodas-and-more/",
            "pagemap": {
                "metatags": [
                    {
                        "application-name": "eastbaytimes",
                        "og:image": "https://www.eastbaytimes.com/wp-content/themes/eastbaytimes/static/images/eastbaytimes.jpg",
                        "og:type": "article",
                        "article:published_time": "2010-04-29T18:42:23+00:00",
                        "twitter:card": "summary",
                        "wp-parsely_version": "1.14",
                        "og:site_name": "East Bay Times",
                        "msvalidate.01": "4B535F7EB2971D1FCBA5D1D3E3E292C3",
                        "og:title": "Spin Cycle: Synonyms, sodas and more",
                        "msapplication-tileimage": "https://www.eastbaytimes.com/wp-content/uploads/2016/10/32x32-ebt.png?w=32",
                        "fb:pages": "17088540698",
                        "og:description": "Spin Cycle: Synonyms, sodas and more",
                        "twitter:image": "https://www.eastbaytimes.com/wp-content/themes/eastbaytimes/static/images/eastbaytimes.jpg",
                        "twitter:text:title": "Spin Cycle: Synonyms, sodas and more",
                        "twitter:site": "@eastbaytimes",
                        "article:modified_time": "2016-08-15T16:20:34+00:00",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:locale": "en_US",
                        "og:url": "https://www.eastbaytimes.com/2010/04/29/spin-cycle-synonyms-sodas-and-more/"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.eastbaytimes.com/wp-content/themes/eastbaytimes/static/images/eastbaytimes.jpg"
                    }
                ],
                "hatomfeed": [
                    {}
                ]
            }
        }
    ]
}
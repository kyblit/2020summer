{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - pitch  synonyms",
                "totalResults": "9670000",
                "searchTerms": "pitch  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - pitch  synonyms",
                "totalResults": "9670000",
                "searchTerms": "pitch  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.278829,
        "formattedSearchTime": "0.28",
        "totalResults": "9670000",
        "formattedTotalResults": "9,670,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Pitch Synonyms, Pitch Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Pitch Synonyms</b>, Pitch Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/pitch",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for pitch at Thesaurus.com with free online thesaurus, antonyms, and \ndefinitions. Find descriptive alternatives for pitch.",
            "htmlSnippet": "<b>Synonyms</b> for <b>pitch</b> at <b>Thesaurus</b>.com with free online <b>thesaurus</b>, antonyms, and <br>\ndefinitions. Find descriptive alternatives for <b>pitch</b>.",
            "cacheId": "Y9g-tUZxPSUJ",
            "formattedUrl": "https://www.thesaurus.com/browse/pitch",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>pitch</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of pitch | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for pitch from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Pitch Synonyms, Pitch Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Pitch Synonyms</b>, Pitch Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/pitch",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms & Antonyms of pitch · 1 to fix in an upright position · 2 to cast oneself \nhead first into deep water · 3 to make a series of unsteady side-to-side motions · 4 \nto ...",
            "htmlSnippet": "<b>Synonyms</b> &amp; Antonyms of <b>pitch</b> &middot; 1 to fix in an upright position &middot; 2 to cast oneself <br>\nhead first into deep water &middot; 3 to make a series of unsteady side-to-side motions &middot; 4 <br>\nto&nbsp;...",
            "cacheId": "8GvyNvBUxWMJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/pitch",
            "htmlFormattedUrl": "https://www.merriam-webster.com/<b>thesaurus</b>/<b>pitch</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for PITCH",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/pitch",
                        "og:title": "Thesaurus results for PITCH",
                        "twitter:aria-text": "Share more words for pitch on Twitter",
                        "og:aria-text": "Post more words for pitch to Facebook",
                        "og:description": "Pitch: an act or instance of diving. Synonyms: dive, plunge, cant… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Pitch: an act or instance of diving. Synonyms: dive, plunge, cant… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/pitch"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Sales pitch Synonyms, Sales pitch Antonyms | Thesaurus.com",
            "htmlTitle": "Sales <b>pitch Synonyms</b>, Sales pitch Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/sales%20pitch",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for sales pitch at Thesaurus.com with free online thesaurus, antonyms, \nand definitions. Find descriptive alternatives for sales pitch.",
            "htmlSnippet": "<b>Synonyms</b> for sales <b>pitch</b> at <b>Thesaurus</b>.com with free online <b>thesaurus</b>, antonyms, <br>\nand definitions. Find descriptive alternatives for sales <b>pitch</b>.",
            "cacheId": "ABK4yebhYoUJ",
            "formattedUrl": "https://www.thesaurus.com/browse/sales%20pitch",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/sales%20<b>pitch</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of sales pitch | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for sales pitch from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for pitch? | Pitch Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for pitch? | <b>Pitch Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/pitch.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 9938 synonyms for pitch and other similar words that you can use instead \nbased on 54 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 9938 <b>synonyms</b> for <b>pitch</b> and other similar words that you can use instead <br>\nbased on 54 separate contexts from our <b>thesaurus</b>.",
            "cacheId": "_rU7AMqQ5t4J",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/pitch.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>pitch</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Pitching Synonyms, Pitching Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Pitching Synonyms</b>, Pitching Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/pitching",
            "displayLink": "www.thesaurus.com",
            "snippet": "pitching · verbthrow, hurl · verbput up, erect · verbdive, roll ...",
            "htmlSnippet": "<b>pitching</b> &middot; verbthrow, hurl &middot; verbput up, erect &middot; verbdive, roll&nbsp;...",
            "cacheId": "bI8jugMdKqMJ",
            "formattedUrl": "https://www.thesaurus.com/browse/pitching",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>pitching</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of pitching | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for pitching from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Pitching Synonyms, Pitching Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Pitching Synonyms</b>, Pitching Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/pitching",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms & Antonyms of pitching · 1 to fix in an upright position · 2 to cast \noneself head first into deep water · 3 to make a series of unsteady side-to-side \nmotions · 4 ...",
            "htmlSnippet": "<b>Synonyms</b> &amp; Antonyms of <b>pitching</b> &middot; 1 to fix in an upright position &middot; 2 to cast <br>\noneself head first into deep water &middot; 3 to make a series of unsteady side-to-side <br>\nmotions &middot; 4&nbsp;...",
            "cacheId": "mWbLx4lQSy0J",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/pitching",
            "htmlFormattedUrl": "https://www.merriam-webster.com/<b>thesaurus</b>/<b>pitching</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for PITCHING",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/pitching",
                        "og:title": "Thesaurus results for PITCHING",
                        "twitter:aria-text": "Share more words for pitching on Twitter",
                        "og:aria-text": "Post more words for pitching to Facebook",
                        "og:description": "Pitching: to fix in an upright position. Synonyms: erecting, putting up, raising… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Pitching: to fix in an upright position. Synonyms: erecting, putting up, raising… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/pitching"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Pitch in Synonyms, Pitch in Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Pitch</b> in <b>Synonyms</b>, <b>Pitch</b> in Antonyms | <b>Thesaurus</b>.com",
            "link": "https://www.thesaurus.com/browse/pitch%20in",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · come through · aid · attack · begin · buckle down · chip in · \ncommence · contribute.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; come through &middot; aid &middot; attack &middot; begin &middot; buckle down &middot; chip in &middot; <br>\ncommence &middot; contribute.",
            "cacheId": "T090tB8t9GUJ",
            "formattedUrl": "https://www.thesaurus.com/browse/pitch%20in",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>pitch</b>%20in",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of pitch in | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for pitch in from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Pitch Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Pitch Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/pitch",
            "displayLink": "www.synonyms.com",
            "snippet": "Princeton's WordNet(1.00 / 4 votes)Rate these synonyms: · pitch(noun) the \nproperty of sound that varies with variation in the frequency of vibration · pitch, \ndelivery( ...",
            "htmlSnippet": "Princeton&#39;s WordNet(1.00 / 4 votes)Rate these <b>synonyms</b>: &middot; <b>pitch</b>(noun) the <br>\nproperty of sound that varies with variation in the frequency of vibration &middot; <b>pitch</b>, <br>\ndelivery(&nbsp;...",
            "cacheId": "YzOZgt24vUwJ",
            "formattedUrl": "https://www.synonyms.com/synonym/pitch",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>pitch</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/pitch",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Pitch Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Pitch Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/pitch",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for pitch: sports field, ground, stadium, arena, park | Collins English \nThesaurus.",
            "htmlSnippet": "Another word for <b>pitch</b>: sports field, ground, stadium, arena, park | Collins English <br>\n<b>Thesaurus</b>.",
            "cacheId": "UuB2jzIn0bcJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/pitch",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-<b>thesaurus</b>/<b>pitch</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Pitch Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "c776a1a4-6485-450c-8f79-ef14038fbd2b",
                        "_csrf_parameter": "_csrf",
                        "title": "Pitch Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for pitch: sports field, ground, stadium, arena, park | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/pitch"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Pitch synonyms | Best 127 synonyms for pitch",
            "htmlTitle": "<b>Pitch synonyms</b> | Best 127 synonyms for pitch",
            "link": "https://thesaurus.yourdictionary.com/pitch",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 127 synonyms for pitch, including: slant, incline, angle, toss, cast, fling, \nhurl, heave, pitched ball, ball, strike and more... Find another word for pitch at ...",
            "htmlSnippet": "The best 127 <b>synonyms</b> for <b>pitch</b>, including: slant, incline, angle, toss, cast, fling, <br>\nhurl, heave, <b>pitched</b> ball, ball, strike and more... Find another word for <b>pitch</b> at&nbsp;...",
            "cacheId": "DTe6z-A4OPQJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/pitch",
            "htmlFormattedUrl": "https://<b>thesaurus</b>.yourdictionary.com/<b>pitch</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        }
    ]
}
{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - tower  synonyms",
                "totalResults": "8170000",
                "searchTerms": "tower  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - tower  synonyms",
                "totalResults": "8170000",
                "searchTerms": "tower  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.34656,
        "formattedSearchTime": "0.35",
        "totalResults": "8170000",
        "formattedTotalResults": "8,170,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Tower Synonyms, Tower Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Tower Synonyms</b>, Tower Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/tower",
            "displayLink": "www.thesaurus.com",
            "snippet": "RELATED WORDS AND SYNONYMS FOR TOWER. arise. verbget, stand, or go \nup. ascend ...",
            "htmlSnippet": "RELATED WORDS AND <b>SYNONYMS</b> FOR <b>TOWER</b>. arise. verbget, stand, or go <br>\nup. ascend&nbsp;...",
            "cacheId": "Ezfb9bxBkcQJ",
            "formattedUrl": "https://www.thesaurus.com/browse/tower",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>tower</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of tower | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for tower from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Tower Synonyms, Tower Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Tower Synonyms</b>, Tower Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/tower",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms of tower · castle, · château, · countryseat, · estate, · hacienda, · manor, · \nmanor house, · mansion, ...",
            "htmlSnippet": "<b>Synonyms</b> of <b>tower</b> &middot; castle, &middot; château, &middot; countryseat, &middot; estate, &middot; hacienda, &middot; manor, &middot; <br>\nmanor house, &middot; mansion,&nbsp;...",
            "cacheId": "ThGJMD0CPTYJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/tower",
            "htmlFormattedUrl": "https://www.merriam-webster.com/<b>thesaurus</b>/<b>tower</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for TOWER",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/tower",
                        "og:title": "Thesaurus results for TOWER",
                        "twitter:aria-text": "Share more words for tower on Twitter",
                        "og:aria-text": "Post more words for tower to Facebook",
                        "og:description": "Tower: a large, magnificent, or massive building. Synonyms: cathedral, edifice, hall… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Tower: a large, magnificent, or massive building. Synonyms: cathedral, edifice, hall… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/tower"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Tower synonyms | Best 44 synonyms for tower",
            "htmlTitle": "<b>Tower synonyms</b> | Best 44 synonyms for tower",
            "link": "https://thesaurus.yourdictionary.com/tower",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 44 synonyms for tower, including: bell-tower, spire, mast, steeple, \ncampanile, dungeon, keep, belfry, monolith, radio-tower, lookout-tower and more.",
            "htmlSnippet": "The best 44 <b>synonyms</b> for <b>tower</b>, including: bell-<b>tower</b>, spire, mast, steeple, <br>\ncampanile, dungeon, keep, belfry, monolith, radio-<b>tower</b>, lookout-<b>tower</b> and more.",
            "cacheId": "WafPZWo38k0J",
            "formattedUrl": "https://thesaurus.yourdictionary.com/tower",
            "htmlFormattedUrl": "https://<b>thesaurus</b>.yourdictionary.com/<b>tower</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for tower? | Tower Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for tower? | <b>Tower Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/tower.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 4517 synonyms for tower and other similar words that you can use instead \nbased on 23 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 4517 <b>synonyms</b> for <b>tower</b> and other similar words that you can use instead <br>\nbased on 23 separate contexts from our <b>thesaurus</b>.",
            "cacheId": "xw5PKPC1CXgJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/tower.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>tower</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "1 554 Tower synonyms - Other Words for Tower",
            "htmlTitle": "1 554 <b>Tower synonyms</b> - Other Words for Tower",
            "link": "https://www.powerthesaurus.org/tower/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Tower synonyms. Top synonyms for tower (other words for tower) are loom, soar \nand column.",
            "htmlSnippet": "<b>Tower synonyms</b>. Top synonyms for tower (other words for tower) are loom, soar <br>\nand column.",
            "cacheId": "62IIH5gQj3kJ",
            "formattedUrl": "https://www.powerthesaurus.org/tower/synonyms",
            "htmlFormattedUrl": "https://www.power<b>thesaurus</b>.org/<b>tower</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "1 554 Tower synonyms - Other Words for Tower",
                        "og:description": "Tower synonyms. Top synonyms for tower (other words for tower) are loom, soar and column.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/tower/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for most ivory-tower",
            "htmlTitle": "<b>Synonyms</b> for most ivory-<b>tower</b>",
            "link": "http://www.thesaurus.com/browse/most+ivory-tower",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for most ivory-tower at Thesaurus.com with free online thesaurus, \nantonyms, and definitions. Find descriptive alternatives for most ivory-tower.",
            "htmlSnippet": "<b>Synonyms</b> for most ivory-<b>tower</b> at <b>Thesaurus</b>.com with free online <b>thesaurus</b>, <br>\nantonyms, and definitions. Find descriptive alternatives for most ivory-<b>tower</b>.",
            "cacheId": "5gRIt-1v_Y4J",
            "formattedUrl": "www.thesaurus.com/browse/most+ivory-tower",
            "htmlFormattedUrl": "www.<b>thesaurus</b>.com/browse/most+ivory-<b>tower</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "og:title": "Synonyms of most ivory-tower | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for most ivory-tower from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Tower Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Tower Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/tower",
            "displayLink": "www.synonyms.com",
            "snippet": "Princeton's WordNet(1.50 / 2 votes)Rate these synonyms: · tower(noun) a \nstructure taller than its diameter; can stand alone or be attached to a larger \nbuilding.",
            "htmlSnippet": "Princeton&#39;s WordNet(1.50 / 2 votes)Rate these <b>synonyms</b>: &middot; <b>tower</b>(noun) a <br>\nstructure taller than its diameter; can stand alone or be attached to a larger <br>\nbuilding.",
            "cacheId": "nON0y2T78l8J",
            "formattedUrl": "https://www.synonyms.com/synonym/tower",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>tower</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/tower",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Tower Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Tower Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/tower",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for tower: column, pillar, turret, belfry, steeple | Collins English \nThesaurus.",
            "htmlSnippet": "Another word for <b>tower</b>: column, pillar, turret, belfry, steeple | Collins English <br>\n<b>Thesaurus</b>.",
            "cacheId": "SFyFF5NLDssJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/tower",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-<b>thesaurus</b>/<b>tower</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Tower Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "1d2634c2-0218-4eb2-9893-be1a547618c6",
                        "_csrf_parameter": "_csrf",
                        "title": "Tower Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for tower: column, pillar, turret, belfry, steeple | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/tower"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for tower | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>tower</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/tower",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "VfF8Iz7iu20J",
            "formattedUrl": "https://www.synonym.com/synonyms/tower",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>tower</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for tower",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for tower",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "tower | definition: a structure taller than its diameter; can stand alone or be attached to a larger building | synonyms: tower block, supporting tower, beacon light, beacon, steeple, watchtower, spire, barbacan, silo, control tower, mooring tower, power pylon, bell tower, turret, pharos, church tower, lighthouse, barbican, mooring mast, shot tower, pylon, construction, minaret, structure, clock tower, high-rise| antonyms: natural object, disassembly, misconstruction, low-rise, lower",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "tower | definition: a structure taller than its diameter; can stand alone or be attached to a larger building | synonyms: tower block, supporting tower, beacon light, beacon, steeple, watchtower, spire, barbacan, silo, control tower, mooring tower, power pylon, bell tower, turret, pharos, church tower, lighthouse, barbican, mooring mast, shot tower, pylon, construction, minaret, structure, clock tower, high-rise| antonyms: natural object, disassembly, misconstruction, low-rise, lower",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/tower"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "tower synonyms with definition | Macmillan Thesaurus",
            "htmlTitle": "<b>tower synonyms</b> with definition | Macmillan Thesaurus",
            "link": "https://www.macmillanthesaurus.com/us/tower",
            "displayLink": "www.macmillanthesaurus.com",
            "snippet": "Related terms for 'tower': annexe, awning, carport, dirty kitchen, encampment, \nfolly, lapa, marquee, outbuilding, outhouse, outside toilet/stairs etc.",
            "htmlSnippet": "Related terms for &#39;<b>tower</b>&#39;: annexe, awning, carport, dirty kitchen, encampment, <br>\nfolly, lapa, marquee, outbuilding, outhouse, outside toilet/stairs etc.",
            "cacheId": "ZYDzmBKePXsJ",
            "formattedUrl": "https://www.macmillanthesaurus.com/us/tower",
            "htmlFormattedUrl": "https://www.macmillan<b>thesaurus</b>.com/us/<b>tower</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRu4knELk1zAuP1o_6qYG9N8wS3QGS-A7cIYqn929xoH6UJR26QGBoXkuE",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/us/external/images/logoThesaurus.png?version=1.0.46",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no",
                        "og:title": "tower synonyms with definition | Macmillan Thesaurus",
                        "title": "tower synonyms with definition | Macmillan Thesaurus",
                        "og:url": "https://www.macmillanthesaurus.com/us/tower",
                        "og:description": "Related terms for 'tower': annexe, awning, carport, dirty kitchen, encampment, folly, lapa, marquee, outbuilding, outhouse, outside toilet/stairs etc"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillanthesaurus.com/us/external/images/bg-wave.svg?version=1.0.46"
                    }
                ]
            }
        }
    ]
}
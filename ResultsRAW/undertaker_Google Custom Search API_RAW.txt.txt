{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - undertaker  synonyms",
                "totalResults": "630000",
                "searchTerms": "undertaker  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - undertaker  synonyms",
                "totalResults": "630000",
                "searchTerms": "undertaker  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.322786,
        "formattedSearchTime": "0.32",
        "totalResults": "630000",
        "formattedTotalResults": "630,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Undertaker Synonyms, Undertaker Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Undertaker Synonyms</b>, Undertaker Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/undertaker",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for undertaker. embalmer · mortician · grave digger. TRY undertaker \nIN A SENTENCE BELOW.",
            "htmlSnippet": "<b>Synonyms</b> for <b>undertaker</b>. embalmer &middot; mortician &middot; grave digger. TRY <b>undertaker</b> <br>\nIN A SENTENCE BELOW.",
            "cacheId": "uhMlEO9W1_kJ",
            "formattedUrl": "https://www.thesaurus.com/browse/undertaker",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>undertaker</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of undertaker | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for undertaker from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Undertaker Synonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Undertaker Synonyms</b> | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/undertaker",
            "displayLink": "www.merriam-webster.com",
            "snippet": "a person who manages funerals and prepares the dead for burial or cremation. \nthe undertaker wore black clothes and a solemn expression. Synonyms for ...",
            "htmlSnippet": "a person who manages funerals and prepares the dead for burial or cremation. <br>\nthe <b>undertaker</b> wore black clothes and a solemn expression. <b>Synonyms</b> for&nbsp;...",
            "cacheId": "qoAZCzMwVY4J",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/undertaker",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>undertaker</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for UNDERTAKER",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/undertaker",
                        "og:title": "Thesaurus results for UNDERTAKER",
                        "twitter:aria-text": "Share more words for undertaker on Twitter",
                        "og:aria-text": "Post more words for undertaker to Facebook",
                        "og:description": "Undertaker: a person who manages funerals and prepares the dead for burial or cremation. Synonyms: funeral director, mortician… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Undertaker: a person who manages funerals and prepares the dead for burial or cremation. Synonyms: funeral director, mortician… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/undertaker"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Undertaker synonyms | Best 6 synonyms for undertaker",
            "htmlTitle": "<b>Undertaker synonyms</b> | Best 6 synonyms for undertaker",
            "link": "https://thesaurus.yourdictionary.com/undertaker",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 6 synonyms for undertaker, including: mortician, funeral-director, \nembalmer, cremator, body-snatcher, funeral undertaker and more... Find another \nword ...",
            "htmlSnippet": "The best 6 <b>synonyms</b> for <b>undertaker</b>, including: mortician, funeral-director, <br>\nembalmer, cremator, body-snatcher, funeral <b>undertaker</b> and more... Find another <br>\nword&nbsp;...",
            "cacheId": "Zo-5VPbmO8YJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/undertaker",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>undertaker</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "121 Undertaker Synonyms and 4 Undertaker Antonyms | Undertaker ...",
            "htmlTitle": "121 <b>Undertaker Synonyms</b> and 4 Undertaker Antonyms | Undertaker ...",
            "link": "https://www.powerthesaurus.org/undertaker",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Synonyms (Other Words) for Undertaker & Antonyms (Opposite Meaning) for \nUndertaker.",
            "htmlSnippet": "<b>Synonyms</b> (Other Words) for <b>Undertaker</b> &amp; Antonyms (Opposite Meaning) for <br>\n<b>Undertaker</b>.",
            "cacheId": "ihuxQfdh1bAJ",
            "formattedUrl": "https://www.powerthesaurus.org/undertaker",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>undertaker</b>",
            "pagemap": {
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "121 Undertaker Synonyms and 4 Undertaker Antonyms | Undertaker in Thesaurus",
                        "og:description": "Synonyms (Other Words) for Undertaker & Antonyms (Opposite Meaning) for Undertaker.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/undertaker",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for undertaker? | Undertaker Synonyms ...",
            "htmlTitle": "What is another word for undertaker? | <b>Undertaker Synonyms</b> ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/undertaker.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 148 synonyms for undertaker and other similar words that you can use \ninstead based on 2 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 148 <b>synonyms</b> for <b>undertaker</b> and other similar words that you can use <br>\ninstead based on 2 separate contexts from our thesaurus.",
            "cacheId": "Dl3Sh1CL45kJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/undertaker.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>undertaker</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for UNDERTAKER - Thesaurus.net",
            "htmlTitle": "<b>Synonyms</b> for <b>UNDERTAKER</b> - Thesaurus.net",
            "link": "https://www.thesaurus.net/undertaker",
            "displayLink": "www.thesaurus.net",
            "snippet": "Synonyms for UNDERTAKER: grim, sponsor, staid, sedate, rueful, grim-faced, \nskilled worker, serious, elegy, industrialist, sexton, gravedigger, subscriber, ...",
            "htmlSnippet": "<b>Synonyms</b> for <b>UNDERTAKER</b>: grim, sponsor, staid, sedate, rueful, grim-faced, <br>\nskilled worker, serious, elegy, industrialist, sexton, gravedigger, subscriber,&nbsp;...",
            "cacheId": "x2OQr6mqzOAJ",
            "formattedUrl": "https://www.thesaurus.net/undertaker",
            "htmlFormattedUrl": "https://www.thesaurus.net/<b>undertaker</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTHmPbL6L3tWwemgnxXK_eIzcEGEcFI39SJD35kF79sVRInWi-xNtX9sw",
                        "width": "312",
                        "height": "53"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.net/public/desktop/images/logo.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for UNDERTAKER - Thesaurus.net",
                        "og:site_name": "Thesaurus.net",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "undertaker | synonyms: grim, sponsor, staid, sedate, rueful, grim-faced, skilled worker, serious, elegy, industrialist, sexton, gravedigger, subscriber, demure, funeral, Stayed, dirge, Tolling, cypress, boss, sober, mortuary, trained worker, Funeral Rite, builder, kneel, surety, wan, long-faced, solemn, undertakers, mute, undertaking, contractor, lamentation, guarantor, dead march, manufacturer, businessman, guarantee, funeral parlor, orbit, epitaph, Funeral Parlour, Ashes, coffin, cremate, burial, cremains, casket, bier, cortege, bury, box, founders, promoter, IMPRESSARIO, funeral director, mortician, embalmers, embalmer, grave digger, funeral undertaker, body snatcher, Cremator, grim",
                        "og:title": "Synonyms for UNDERTAKER - Thesaurus.net",
                        "og:url": "https://www.thesaurus.net/undertaker",
                        "og:description": "undertaker | synonyms: grim, sponsor, staid, sedate, rueful, grim-faced, skilled worker, serious, elegy, industrialist, sexton, gravedigger, subscriber, demure, funeral, Stayed, dirge, Tolling, cypress, boss, sober, mortuary, trained worker, Funeral Rite, builder, kneel, surety, wan, long-faced, solemn, undertakers, mute, undertaking, contractor, lamentation, guarantor, dead march, manufacturer, businessman, guarantee, funeral parlor, orbit, epitaph, Funeral Parlour, Ashes, coffin, cremate, burial, cremains, casket, bier, cortege, bury, box, founders, promoter, IMPRESSARIO, funeral director, mortician, embalmers, embalmer, grave digger, funeral undertaker, body snatcher, Cremator, grim"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.net/public/desktop/images/logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Undertaker | Synonyms of Undertaker by Lexico",
            "htmlTitle": "<b>Undertaker</b> | <b>Synonyms</b> of <b>Undertaker</b> by Lexico",
            "link": "https://www.lexico.com/synonym/undertaker",
            "displayLink": "www.lexico.com",
            "snippet": "1'the undertaker brought the coffin to our house'. SYNONYMS. funeral director. \nNorth American mortician. archaic upholder, blackmaster, cold cook, death- ...",
            "htmlSnippet": "1&#39;the <b>undertaker</b> brought the coffin to our house&#39;. <b>SYNONYMS</b>. funeral director. <br>\nNorth American mortician. archaic upholder, blackmaster, cold cook, death-&nbsp;...",
            "cacheId": "fiGt9qyqAfwJ",
            "formattedUrl": "https://www.lexico.com/synonym/undertaker",
            "htmlFormattedUrl": "https://www.lexico.com/<b>synonym</b>/<b>undertaker</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT-fHqiHsELFzBLFkvhF3MUNoGyDc02pwOeICq9JD-vhUo5TV_0a-_rYOwh",
                        "width": "291",
                        "height": "173"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#50b46c",
                        "og:image": "https://www.lexico.com/lexico-logo.png",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Undertaker | Synonyms of Undertaker by Lexico",
                        "og:type": "website",
                        "theme-color": "#50b46c",
                        "og:site_name": "Lexico Dictionaries | English",
                        "og:title": "Undertaker | Synonyms of Undertaker by Lexico",
                        "msapplication-tileimage": "/mstile-144x144.png",
                        "csrf-param": "authenticity_token",
                        "og:description": "What is the definition of undertaker? What is the meaning of undertaker? How do you use undertaker in a sentence? What are synonyms for undertaker?",
                        "twitter:image": "https://www.lexico.com/lexico-logo.png",
                        "viewport": "width=device-width, initial-scale=1, shrink-to-fit=no",
                        "twitter:description": "What is the definition of undertaker? What is the meaning of undertaker? How do you use undertaker in a sentence? What are synonyms for undertaker?",
                        "csrf-token": "tQgNQMsPfQp+F0JcUV5P68oG7bx9yfos+5769LNoRc7jLZT+GmBFnbmxKj+JjpeXTroZDqwBuozwYUWJNgBioA==",
                        "og:url": "https://www.lexico.com/synonym/undertaker"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.lexico.com/lexico-logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "39 Undertaker Synonyms. Similar words for Undertaker.",
            "htmlTitle": "39 <b>Undertaker Synonyms</b>. Similar words for Undertaker.",
            "link": "https://thesaurus.plus/synonyms/undertaker",
            "displayLink": "thesaurus.plus",
            "snippet": "What is another word for Undertaker? mortician. funeral director.",
            "htmlSnippet": "What is another word for <b>Undertaker</b>? mortician. funeral director.",
            "cacheId": "WNieFj0QcgQJ",
            "formattedUrl": "https://thesaurus.plus/synonyms/undertaker",
            "htmlFormattedUrl": "https://thesaurus.plus/<b>synonyms</b>/<b>undertaker</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQPU49Z3cj1GRxHSn6-6NdiRThozYijSoph32RvNNCI6l5Ka7QEHA8BDN5D",
                        "width": "286",
                        "height": "176"
                    }
                ],
                "website": [
                    {
                        "url": "https://thesaurus.plus/"
                    }
                ],
                "imageobject": [
                    {
                        "contenturl": "https://thesaurus.plus/img/synonyms/527/undertaker.png",
                        "width": "650 px",
                        "description": "Synonyms for undertaker",
                        "height": "400 px"
                    },
                    {
                        "contenturl": "https://thesaurus.plus/img/synonyms/a527/undertaker.png",
                        "width": "650 px",
                        "description": "Synonyms for undertaker",
                        "height": "400 px"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://thesaurus.plus/img/synonyms/527/undertaker.png",
                        "og:type": "website",
                        "twitter:title": "Top synonyms for Undertaker",
                        "og:site_name": "thesaurus.plus",
                        "og:title": "Undertaker synonyms",
                        "language": "english",
                        "og:description": "Top synonyms for Undertaker on the Thesaurus.plus!",
                        "twitter:creator": "@thesaurus_plus",
                        "twitter:site": "https://thesaurus.plus/synonyms/undertaker",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no",
                        "csrf-token": "Z14CqdcaC8ZIS1lnXkTtaDhuEXoAvnYMu6ZQBc9M",
                        "og:locale": "en_US",
                        "og:url": "https://thesaurus.plus/synonyms/undertaker"
                    }
                ],
                "itemlist": [
                    {
                        "name": "Top synonyms for Undertaker",
                        "itemlistorder": "Descending"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://thesaurus.plus/img/synonyms/527/undertaker.png"
                    }
                ],
                "searchaction": [
                    {
                        "query-input": "name=q required value",
                        "target": "/search?q={q}"
                    }
                ],
                "listitem": [
                    {
                        "name": "mortician",
                        "position": "1",
                        "url": "https://thesaurus.plus/thesaurus/mortician"
                    },
                    {
                        "name": "funeral director",
                        "position": "2",
                        "url": "https://thesaurus.plus/thesaurus/funeral_director"
                    },
                    {
                        "name": "funeral undertaker",
                        "position": "3",
                        "url": "https://thesaurus.plus/thesaurus/funeral_undertaker"
                    },
                    {
                        "name": "entrepreneur",
                        "position": "4",
                        "url": "https://thesaurus.plus/thesaurus/entrepreneur"
                    },
                    {
                        "name": "embalmer",
                        "position": "5",
                        "url": "https://thesaurus.plus/thesaurus/embalmer"
                    },
                    {
                        "name": "industrialist",
                        "position": "6",
                        "url": "https://thesaurus.plus/thesaurus/industrialist"
                    },
                    {
                        "name": "grave digger",
                        "position": "7",
                        "url": "https://thesaurus.plus/thesaurus/grave_digger"
                    },
                    {
                        "name": "contractor",
                        "position": "8",
                        "url": "https://thesaurus.plus/thesaurus/contractor"
                    },
                    {
                        "name": "boss",
                        "position": "9",
                        "url": "https://thesaurus.plus/thesaurus/boss"
                    },
                    {
                        "name": "employer",
                        "position": "10",
                        "url": "https://thesaurus.plus/thesaurus/employer"
                    },
                    {
                        "name": "manufacturer",
                        "position": "11",
                        "url": "https://thesaurus.plus/thesaurus/manufacturer"
                    },
                    {
                        "name": "manager",
                        "position": "12",
                        "url": "https://thesaurus.plus/thesaurus/manager"
                    },
                    {
                        "name": "executive",
                        "position": "13",
                        "url": "https://thesaurus.plus/thesaurus/executive"
                    },
                    {
                        "name": "administrator",
                        "position": "14",
                        "url": "https://thesaurus.plus/thesaurus/administrator"
                    },
                    {
                        "name": "producer",
                        "position": "15",
                        "url": "https://thesaurus.plus/thesaurus/producer"
                    },
                    {
                        "name": "founder",
                        "position": "16",
                        "url": "https://thesaurus.plus/thesaurus/founder"
                    },
                    {
                        "name": "backer",
                        "position": "17",
                        "url": "https://thesaurus.plus/thesaurus/backer"
                    },
                    {
                        "name": "businessperson",
                        "position": "18",
                        "url": "https://thesaurus.plus/thesaurus/businessperson"
                    },
                    {
                        "name": "promoter",
                        "position": "19",
                        "url": "https://thesaurus.plus/thesaurus/promoter"
                    },
                    {
                        "name": "organizer",
                        "position": "20",
                        "url": "https://thesaurus.plus/thesaurus/organizer"
                    },
                    {
                        "name": "impressario",
                        "position": "21",
                        "url": "https://thesaurus.plus/thesaurus/impressario"
                    },
                    {
                        "name": "upholder",
                        "position": "22",
                        "url": "https://thesaurus.plus/thesaurus/upholder"
                    },
                    {
                        "name": "blackmaster",
                        "position": "23",
                        "url": "https://thesaurus.plus/thesaurus/blackmaster"
                    },
                    {
                        "name": "cold cook",
                        "position": "24",
                        "url": "https://thesaurus.plus/thesaurus/cold_cook"
                    },
                    {
                        "name": "death-hunter",
                        "position": "25",
                        "url": "https://thesaurus.plus/thesaurus/death-hunter"
                    },
                    {
                        "name": "thanatologist",
                        "position": "26",
                        "url": "https://thesaurus.plus/thesaurus/thanatologist"
                    },
                    {
                        "name": "coffin maker",
                        "position": "27",
                        "url": "https://thesaurus.plus/thesaurus/coffin_maker"
                    },
                    {
                        "name": "keeper",
                        "position": "28",
                        "url": "https://thesaurus.plus/thesaurus/keeper"
                    },
                    {
                        "name": "employers",
                        "position": "29",
                        "url": "https://thesaurus.plus/thesaurus/employers"
                    },
                    {
                        "name": "bosses",
                        "position": "30",
                        "url": "https://thesaurus.plus/thesaurus/bosses"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "undertaker synonyms with definition | Macmillan Thesaurus",
            "htmlTitle": "<b>undertaker synonyms</b> with definition | Macmillan Thesaurus",
            "link": "https://www.macmillanthesaurus.com/undertaker",
            "displayLink": "www.macmillanthesaurus.com",
            "snippet": "Related terms for 'undertaker': a letter/message/book of condolence, ashes, bier, \nbox, burial, burial at sea, bury, casket, coffin, cortège, cremains.",
            "htmlSnippet": "Related terms for &#39;<b>undertaker</b>&#39;: a letter/message/book of condolence, ashes, bier, <br>\nbox, burial, burial at sea, bury, casket, coffin, cortège, cremains.",
            "cacheId": "awQzbOEVj3QJ",
            "formattedUrl": "https://www.macmillanthesaurus.com/undertaker",
            "htmlFormattedUrl": "https://www.macmillanthesaurus.com/<b>undertaker</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTtI5T6mNk9kUsK_ryO4iLvq6rrISnaRHmePBXMcUXc7Ri4U_owb_qmtA",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/external/images/logoThesaurus.png?version=1.0.43",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no",
                        "og:title": "undertaker synonyms with definition | Macmillan Thesaurus",
                        "title": "undertaker synonyms with definition | Macmillan Thesaurus",
                        "og:url": "https://www.macmillanthesaurus.com/undertaker",
                        "og:description": "Related terms for 'undertaker': a letter/message/book of condolence, ashes, bier, box, burial, burial at sea, bury, casket, coffin, cortège, cremains"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillanthesaurus.com/external/images/bg-wave.svg?version=1.0.43"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Undertaker Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Undertaker Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/undertaker",
            "displayLink": "www.synonyms.com",
            "snippet": "Find all the synonyms and alternative words for undertaker at Synonyms.com, the \nlargest free online thesaurus, antonyms, definitions and translations resource ...",
            "htmlSnippet": "Find all the <b>synonyms</b> and alternative words for <b>undertaker</b> at <b>Synonyms</b>.com, the <br>\nlargest free online thesaurus, antonyms, definitions and translations resource&nbsp;...",
            "cacheId": "LXjZW1bA1CgJ",
            "formattedUrl": "https://www.synonyms.com/synonym/undertaker",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>undertaker</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/undertaker",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        }
    ]
}
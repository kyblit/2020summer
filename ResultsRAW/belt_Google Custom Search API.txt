{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - belt  synonyms",
                "totalResults": "9960000",
                "searchTerms": "belt  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - belt  synonyms",
                "totalResults": "9960000",
                "searchTerms": "belt  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.259573,
        "formattedSearchTime": "0.26",
        "totalResults": "9960000",
        "formattedTotalResults": "9,960,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Belt Synonyms, Belt Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Belt Synonyms</b>, Belt Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/belt",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for belt at Thesaurus.com with free online thesaurus, antonyms, and \ndefinitions. Find descriptive alternatives for belt.",
            "htmlSnippet": "<b>Synonyms</b> for <b>belt</b> at <b>Thesaurus</b>.com with free online <b>thesaurus</b>, antonyms, and <br>\ndefinitions. Find descriptive alternatives for <b>belt</b>.",
            "cacheId": "SzUbxmt5MGUJ",
            "formattedUrl": "https://www.thesaurus.com/browse/belt",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>belt</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of belt | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for belt from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Belt Synonyms, Belt Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Belt Synonyms</b>, Belt Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/belt",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms of belt · 1 to deliver a blow to (someone or something) usually in a \nstrong vigorous manner belted the baseball out of the park · 2 to encircle or bind \nwith ...",
            "htmlSnippet": "<b>Synonyms</b> of <b>belt</b> &middot; 1 to deliver a blow to (someone or something) usually in a <br>\nstrong vigorous manner belted the baseball out of the park &middot; 2 to encircle or bind <br>\nwith&nbsp;...",
            "cacheId": "BKqHK88JcW8J",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/belt",
            "htmlFormattedUrl": "https://www.merriam-webster.com/<b>thesaurus</b>/<b>belt</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for BELT",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/belt",
                        "og:title": "Thesaurus results for BELT",
                        "twitter:aria-text": "Share more words for belt on Twitter",
                        "og:aria-text": "Post more words for belt to Facebook",
                        "og:description": "Belt: a hard strike with a part of the body or an instrument. Synonyms: bang, bash, bat… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Belt: a hard strike with a part of the body or an instrument. Synonyms: bang, bash, bat… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/belt"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Belt synonyms | Best 113 synonyms for belt",
            "htmlTitle": "<b>Belt synonyms</b> | Best 113 synonyms for belt",
            "link": "https://thesaurus.yourdictionary.com/belt",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 113 synonyms for belt, including: area, strait, locality, clout, sash, \ncummerbund, obi, girdle, tract, region, zone and more... Find another word for \nbelt at ...",
            "htmlSnippet": "The best 113 <b>synonyms</b> for <b>belt</b>, including: area, strait, locality, clout, sash, <br>\ncummerbund, obi, girdle, tract, region, zone and more... Find another word for <br>\n<b>belt</b> at&nbsp;...",
            "cacheId": "L_qfUhjQOaYJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/belt",
            "htmlFormattedUrl": "https://<b>thesaurus</b>.yourdictionary.com/<b>belt</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for belt? | Belt Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for belt? | <b>Belt Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/belt.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 3458 synonyms for belt and other similar words that you can use instead \nbased on 20 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 3458 <b>synonyms</b> for <b>belt</b> and other similar words that you can use instead <br>\nbased on 20 separate contexts from our <b>thesaurus</b>.",
            "cacheId": "yaxBGnLaZoUJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/belt.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>belt</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Seat belt Synonyms, Seat belt Antonyms | Thesaurus.com",
            "htmlTitle": "Seat <b>belt Synonyms</b>, Seat belt Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/seat%20belt",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for seat belt at Thesaurus.com with free online thesaurus, antonyms, \nand definitions. Find descriptive alternatives for seat belt.",
            "htmlSnippet": "<b>Synonyms</b> for seat <b>belt</b> at <b>Thesaurus</b>.com with free online <b>thesaurus</b>, antonyms, <br>\nand definitions. Find descriptive alternatives for seat <b>belt</b>.",
            "cacheId": "ZVQkbjisR7gJ",
            "formattedUrl": "https://www.thesaurus.com/browse/seat%20belt",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/seat%20<b>belt</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of seat belt | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for seat belt from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Belt Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Belt Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/belt",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for belt: waistband, band, strap, sash, girdle | Collins English \nThesaurus.",
            "htmlSnippet": "Another word for <b>belt</b>: waistband, band, strap, sash, girdle | Collins English <br>\n<b>Thesaurus</b>.",
            "cacheId": "nOCHHzdZVBAJ",
            "formattedUrl": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/belt",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/us/dictionary/english-<b>thesaurus</b>/<b>belt</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Belt Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "c8e98c3d-1620-489e-bdd3-be70fdb94c54",
                        "_csrf_parameter": "_csrf",
                        "title": "Belt Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for belt: waistband, band, strap, sash, girdle | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/belt"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Belt | Synonyms of Belt by Lexico",
            "htmlTitle": "<b>Belt</b> | <b>Synonyms</b> of <b>Belt</b> by Lexico",
            "link": "https://www.lexico.com/synonym/belt",
            "displayLink": "www.lexico.com",
            "snippet": "noun · 1'she wore a plain raincoat tied with a belt' SYNONYMS girdle, sash, strap, \ncummerbund, waistband, band, girth · 2'a great wheel driven by a leather belt ...",
            "htmlSnippet": "noun &middot; 1&#39;she wore a plain raincoat tied with a <b>belt</b>&#39; <b>SYNONYMS</b> girdle, sash, strap, <br>\ncummerbund, waistband, band, girth &middot; 2&#39;a great wheel driven by a leather <b>belt</b>&nbsp;...",
            "cacheId": "cpA4PDeWjtIJ",
            "formattedUrl": "https://www.lexico.com/synonym/belt",
            "htmlFormattedUrl": "https://www.lexico.com/<b>synonym</b>/<b>belt</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT-fHqiHsELFzBLFkvhF3MUNoGyDc02pwOeICq9JD-vhUo5TV_0a-_rYOwh",
                        "width": "291",
                        "height": "173"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#50b46c",
                        "og:image": "https://www.lexico.com/lexico-logo.png",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Belt | Synonyms of Belt by Lexico",
                        "og:type": "website",
                        "theme-color": "#50b46c",
                        "og:site_name": "Lexico Dictionaries | English",
                        "og:title": "Belt | Synonyms of Belt by Lexico",
                        "msapplication-tileimage": "/mstile-144x144.png",
                        "csrf-param": "authenticity_token",
                        "og:description": "What is the definition of belt? What is the meaning of belt? How do you use belt in a sentence? What are synonyms for belt?",
                        "twitter:image": "https://www.lexico.com/lexico-logo.png",
                        "viewport": "width=device-width, initial-scale=1, shrink-to-fit=no",
                        "twitter:description": "What is the definition of belt? What is the meaning of belt? How do you use belt in a sentence? What are synonyms for belt?",
                        "csrf-token": "hd+dbueu535KcyvcCviJJsBGfyAVIZbdlVwO8XI1dH4BNlO6dWS0OmhzzWPHWCRN9PP/OPLPro8ijRD0LGrV8A==",
                        "og:url": "https://www.lexico.com/synonym/belt"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.lexico.com/lexico-logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for conveyor-belt | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for conveyor-<b>belt</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/conveyor-belt",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "6sKJ2HA_gCQJ",
            "formattedUrl": "https://www.synonym.com/synonyms/conveyor-belt",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/conveyor-<b>belt</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for conveyor-belt",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for conveyor-belt",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "conveyor-belt | definition: a moving belt that transports objects (as in a factory) | synonyms: assembly line, conveyer belt, belt, luggage carousel, transporter, line, production line, conveyor, luggage carrousel, carousel, carrousel, conveyer| antonyms: unfasten, unbelt, inactivity, natural object, straight line",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "conveyor-belt | definition: a moving belt that transports objects (as in a factory) | synonyms: assembly line, conveyer belt, belt, luggage carousel, transporter, line, production line, conveyor, luggage carrousel, carousel, carrousel, conveyer| antonyms: unfasten, unbelt, inactivity, natural object, straight line",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/conveyor-belt"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "get/have something under your belt synonyms with definition ...",
            "htmlTitle": "get/have something under your <b>belt synonyms</b> with definition ...",
            "link": "https://www.macmillanthesaurus.com/us/get-have-something-under-your-belt",
            "displayLink": "www.macmillanthesaurus.com",
            "snippet": "Synonyms for 'get/have something under your belt': progress, get on with, get \nahead, forge ahead, plod along, outpace, get along, advance.",
            "htmlSnippet": "<b>Synonyms</b> for &#39;get/have something under your <b>belt</b>&#39;: progress, get on with, get <br>\nahead, forge ahead, plod along, outpace, get along, advance.",
            "cacheId": "CW-z8SZcqC4J",
            "formattedUrl": "https://www.macmillanthesaurus.com/.../get-have-something-under-your-belt",
            "htmlFormattedUrl": "https://www.macmillan<b>thesaurus</b>.com/.../get-have-something-under-your-<b>belt</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRu4knELk1zAuP1o_6qYG9N8wS3QGS-A7cIYqn929xoH6UJR26QGBoXkuE",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/us/external/images/logoThesaurus.png?version=1.0.46",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no",
                        "og:title": "get/have something under your belt synonyms with definition | Macmillan Thesaurus",
                        "title": "get/have something under your belt synonyms with definition | Macmillan Thesaurus",
                        "og:url": "https://www.macmillanthesaurus.com/us/get-have-something-under-your-belt",
                        "og:description": "Synonyms for 'get/have something under your belt': progress, get on with, get ahead, forge ahead, plod along, outpace, get along, advance"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillanthesaurus.com/us/external/images/bg-wave.svg?version=1.0.46"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "39 Waist Belt synonyms - Other Words for Waist Belt",
            "htmlTitle": "39 Waist <b>Belt synonyms</b> - Other Words for Waist Belt",
            "link": "https://www.powerthesaurus.org/waist_belt/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Waist Belt synonyms. Top synonyms for waist belt (other words for waist belt) are \nbelt, waistband and garter belt.",
            "htmlSnippet": "Waist <b>Belt synonyms</b>. Top synonyms for waist belt (other words for waist belt) are <br>\nbelt, waistband and garter belt.",
            "cacheId": "LKyY32lsXVwJ",
            "formattedUrl": "https://www.powerthesaurus.org/waist_belt/synonyms",
            "htmlFormattedUrl": "https://www.power<b>thesaurus</b>.org/waist_<b>belt</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "39 Waist Belt synonyms - Other Words for Waist Belt",
                        "og:description": "Waist Belt synonyms. Top synonyms for waist belt (other words for waist belt) are belt, waistband and garter belt.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/waist_belt/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        }
    ]
}
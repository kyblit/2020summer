{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - brush  synonyms",
                "totalResults": "6350000",
                "searchTerms": "brush  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - brush  synonyms",
                "totalResults": "6350000",
                "searchTerms": "brush  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.265216,
        "formattedSearchTime": "0.27",
        "totalResults": "6350000",
        "formattedTotalResults": "6,350,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Brush Synonyms, Brush Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Brush Synonyms</b>, Brush Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/brush",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · broom · toothbrush · besom · hairbrush · mop · polisher · sweeper · \nwhisk.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; broom &middot; toothbrush &middot; besom &middot; hairbrush &middot; mop &middot; polisher &middot; sweeper &middot; <br>\nwhisk.",
            "cacheId": "B0hiROunREMJ",
            "formattedUrl": "https://www.thesaurus.com/browse/brush",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>brush</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of brush | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for brush from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Brush Synonyms, Brush Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Brush Synonyms</b>, Brush Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/brush",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms of brush · bowl, · breeze, · coast, · cruise, · drift, · flow, · glide, · roll, ...",
            "htmlSnippet": "<b>Synonyms</b> of <b>brush</b> &middot; bowl, &middot; breeze, &middot; coast, &middot; cruise, &middot; drift, &middot; flow, &middot; glide, &middot; roll,&nbsp;...",
            "cacheId": "pZxw7kKXCHsJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/brush",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>brush</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for BRUSH",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/brush",
                        "og:title": "Thesaurus results for BRUSH",
                        "twitter:aria-text": "Share more words for brush on Twitter",
                        "og:aria-text": "Post more words for brush to Facebook",
                        "og:description": "Brush: a brief clash between enemies or rivals. Synonyms: encounter, hassle, run-in… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Brush: a brief clash between enemies or rivals. Synonyms: encounter, hassle, run-in… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/brush"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for brush? | Brush Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for brush? | <b>Brush Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/brush.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 4350 synonyms for brush and other similar words that you can use instead \nbased on 31 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 4350 <b>synonyms</b> for <b>brush</b> and other similar words that you can use instead <br>\nbased on 31 separate contexts from our thesaurus.",
            "cacheId": "UaAckGIYxJoJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/brush.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>brush</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for brush | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>brush</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/brush",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "S_mAmT3bje4J",
            "formattedUrl": "https://www.synonym.com/synonyms/brush",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>brush</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for brush",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for brush",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "brush | definition: a dense growth of bushes | synonyms: brushwood, thicket, brake, undergrowth, underwood, coppice, vegetation, botany, canebrake, flora, underbrush, spinney, copse| antonyms: fauna, starve, stifle, diverge, disengage",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "brush | definition: a dense growth of bushes | synonyms: brushwood, thicket, brake, undergrowth, underwood, coppice, vegetation, botany, canebrake, flora, underbrush, spinney, copse| antonyms: fauna, starve, stifle, diverge, disengage",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/brush"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Brush synonyms | Best 71 synonyms for brush",
            "htmlTitle": "<b>Brush synonyms</b> | Best 71 synonyms for brush",
            "link": "https://thesaurus.yourdictionary.com/brush",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 71 synonyms for brush, including: dingle, rub, stroke, skirmish, scrap, \npatrol action, thicket, boscage, undergrowth, second growth, chaparral and more.",
            "htmlSnippet": "The best 71 <b>synonyms</b> for <b>brush</b>, including: dingle, rub, stroke, skirmish, scrap, <br>\npatrol action, thicket, boscage, undergrowth, second growth, chaparral and more.",
            "cacheId": "-lgVBhjWavgJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/brush",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>brush</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Brush Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Brush Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/brush",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for brush: broom, sweeper, scrubbing brush, besom, sweeping \nbrush | Collins English Thesaurus.",
            "htmlSnippet": "Another word for <b>brush</b>: broom, sweeper, scrubbing <b>brush</b>, besom, sweeping <br>\n<b>brush</b> | Collins English Thesaurus.",
            "cacheId": "5nzPbc_v1kgJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/brush",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/<b>brush</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Brush Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "681e1855-f5a5-40ae-9456-7bdc496901af",
                        "_csrf_parameter": "_csrf",
                        "title": "Brush Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for brush: broom, sweeper, scrubbing brush, besom, sweeping brush | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/brush"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Brush | Synonyms of Brush by Lexico",
            "htmlTitle": "<b>Brush</b> | <b>Synonyms</b> of <b>Brush</b> by Lexico",
            "link": "https://www.lexico.com/synonym/brush",
            "displayLink": "www.lexico.com",
            "snippet": "verb · 1'he spent most of his day brushing the floors' SYNONYMS sweep, clean, \nbuff, scrub · 2'she brushed her long auburn hair' SYNONYMS · 3'she felt his lips ...",
            "htmlSnippet": "verb &middot; 1&#39;he spent most of his day <b>brushing</b> the floors&#39; <b>SYNONYMS</b> sweep, clean, <br>\nbuff, scrub &middot; 2&#39;she <b>brushed</b> her long auburn hair&#39; <b>SYNONYMS</b> &middot; 3&#39;she felt his lips&nbsp;...",
            "cacheId": "FiWdFoc2clEJ",
            "formattedUrl": "https://www.lexico.com/synonym/brush",
            "htmlFormattedUrl": "https://www.lexico.com/<b>synonym</b>/<b>brush</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT-fHqiHsELFzBLFkvhF3MUNoGyDc02pwOeICq9JD-vhUo5TV_0a-_rYOwh",
                        "width": "291",
                        "height": "173"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#50b46c",
                        "og:image": "https://www.lexico.com/lexico-logo.png",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Brush | Synonyms of Brush by Lexico",
                        "og:type": "website",
                        "theme-color": "#50b46c",
                        "og:site_name": "Lexico Dictionaries | English",
                        "og:title": "Brush | Synonyms of Brush by Lexico",
                        "msapplication-tileimage": "/mstile-144x144.png",
                        "csrf-param": "authenticity_token",
                        "og:description": "What is the definition of brush? What is the meaning of brush? How do you use brush in a sentence? What are synonyms for brush?",
                        "twitter:image": "https://www.lexico.com/lexico-logo.png",
                        "viewport": "width=device-width, initial-scale=1, shrink-to-fit=no",
                        "twitter:description": "What is the definition of brush? What is the meaning of brush? How do you use brush in a sentence? What are synonyms for brush?",
                        "csrf-token": "X7aLRpQWFJphJ0iQdv5WvQTPB4HnPkYpbsnNzgepsZflWwC8gpPBpoap8n95F72wSYRZwAHfAxnXtxwK/Q/JsA==",
                        "og:url": "https://www.lexico.com/synonym/brush"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.lexico.com/lexico-logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Brush Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Brush Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/brush",
            "displayLink": "www.synonyms.com",
            "snippet": "Find all the synonyms and alternative words for brush at Synonyms.com, the \nlargest free online thesaurus, antonyms, definitions and translations resource on \nthe ...",
            "htmlSnippet": "Find all the <b>synonyms</b> and alternative words for <b>brush</b> at <b>Synonyms</b>.com, the <br>\nlargest free online thesaurus, antonyms, definitions and translations resource on <br>\nthe&nbsp;...",
            "cacheId": "ihvlTbo4tyAJ",
            "formattedUrl": "https://www.synonyms.com/synonym/brush",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>brush</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/brush",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "brush synonyms with definition | Macmillan Thesaurus",
            "htmlTitle": "<b>brush synonyms</b> with definition | Macmillan Thesaurus",
            "link": "https://www.macmillanthesaurus.com/us/brush",
            "displayLink": "www.macmillanthesaurus.com",
            "snippet": "Synonyms for 'brush': clean up, clear out, clean, sort out, clean out, tidy, pick up \nafter, pick up, straighten up, brush down, change, clean down.",
            "htmlSnippet": "<b>Synonyms</b> for &#39;<b>brush</b>&#39;: clean up, clear out, clean, sort out, clean out, tidy, pick up <br>\nafter, pick up, straighten up, <b>brush</b> down, change, clean down.",
            "cacheId": "UOxQWhKSAz4J",
            "formattedUrl": "https://www.macmillanthesaurus.com/us/brush",
            "htmlFormattedUrl": "https://www.macmillanthesaurus.com/us/<b>brush</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRu4knELk1zAuP1o_6qYG9N8wS3QGS-A7cIYqn929xoH6UJR26QGBoXkuE",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/us/external/images/logoThesaurus.png?version=1.0.46",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no",
                        "og:title": "brush synonyms with definition | Macmillan Thesaurus",
                        "title": "brush synonyms with definition | Macmillan Thesaurus",
                        "og:url": "https://www.macmillanthesaurus.com/us/brush",
                        "og:description": "Synonyms for 'brush': clean up, clear out, clean, sort out, clean out, tidy, pick up after, pick up, straighten up, brush down, change, clean down"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillanthesaurus.com/us/external/images/bg-wave.svg?version=1.0.46"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "BRUSH (verb) definition and synonyms | Macmillan Dictionary",
            "htmlTitle": "<b>BRUSH</b> (verb) definition and <b>synonyms</b> | Macmillan Dictionary",
            "link": "https://www.macmillandictionary.com/dictionary/british/brush_1",
            "displayLink": "www.macmillandictionary.com",
            "snippet": "She hadn't bothered to brush her hair. How often do you brush your teeth? \nSynonyms and related words.",
            "htmlSnippet": "She hadn&#39;t bothered to <b>brush</b> her hair. How often do you <b>brush</b> your teeth? <br>\n<b>Synonyms</b> and related words.",
            "cacheId": "X_lrCjoQplAJ",
            "formattedUrl": "https://www.macmillandictionary.com/dictionary/british/brush_1",
            "htmlFormattedUrl": "https://www.macmillandictionary.com/dictionary/british/<b>brush</b>_1",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQq1XwZHyhsplz2gqTWfOMdrRK_HTPsdInhEZEectSt8unMIf5yPEGbNA",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/external/images/social/social_macmillan.jpg?version=4.0.27",
                        "og:type": "website",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=10.0, minimum-scale=0.1",
                        "og:title": "BRUSH (verb) definition and synonyms | Macmillan Dictionary",
                        "og:url": "https://www.macmillandictionary.com/dictionary/british/brush_1",
                        "og:description": "Definition of BRUSH (verb): make something clean or tidy using brush; touch something slightly as you go past; remove something using quick hand ..."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillandictionary.com/external/images/bg-wave.svg?version=4.0.27"
                    }
                ]
            }
        }
    ]
}
{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - back  synonyms",
                "totalResults": "205000000",
                "searchTerms": "back  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - back  synonyms",
                "totalResults": "205000000",
                "searchTerms": "back  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.289389,
        "formattedSearchTime": "0.29",
        "totalResults": "205000000",
        "formattedTotalResults": "205,000,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Back Synonyms, Back Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Back Synonyms</b>, Back Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/back",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for back at Thesaurus.com with free online thesaurus, antonyms, and \ndefinitions. Find descriptive alternatives for back.",
            "htmlSnippet": "<b>Synonyms</b> for <b>back</b> at <b>Thesaurus</b>.com with free online <b>thesaurus</b>, antonyms, and <br>\ndefinitions. Find descriptive alternatives for <b>back</b>.",
            "cacheId": "q9eyfOoxG9sJ",
            "formattedUrl": "https://www.thesaurus.com/browse/back",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>back</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of back | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for back from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Back Synonyms, Back Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Back Synonyms</b>, Back Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/back",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Some common synonyms of back are advocate, champion, support, and uphold. \nWhile all these words mean \"to favor actively one that meets opposition,\" back ...",
            "htmlSnippet": "Some common <b>synonyms</b> of <b>back</b> are advocate, champion, support, and uphold. <br>\nWhile all these words mean &quot;to favor actively one that meets opposition,&quot; <b>back</b>&nbsp;...",
            "cacheId": "Mww43SvKsjYJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/back",
            "htmlFormattedUrl": "https://www.merriam-webster.com/<b>thesaurus</b>/<b>back</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for BACK",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/back",
                        "og:title": "Thesaurus results for BACK",
                        "twitter:aria-text": "Share more words for back on Twitter",
                        "og:aria-text": "Post more words for back to Facebook",
                        "og:description": "Back: earlier than the present time. Synonyms: ago, agone, since… Antonyms: hence, ahead, along… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Back: earlier than the present time. Synonyms: ago, agone, since… Antonyms: hence, ahead, along… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/back"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Back synonyms | Best 212 synonyms for back",
            "htmlTitle": "<b>Back synonyms</b> | Best 212 synonyms for back",
            "link": "https://thesaurus.yourdictionary.com/back",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 212 synonyms for back, including: after, in-back-of, in-the-wake-of, \nbackward, hindmost, behind, abaft, astern, hind, rearward, aback and more.",
            "htmlSnippet": "The best 212 <b>synonyms</b> for <b>back</b>, including: after, in-<b>back</b>-of, in-the-wake-of, <br>\nbackward, hindmost, behind, abaft, astern, hind, rearward, aback and more.",
            "cacheId": "oB1sGrHYy3UJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/back",
            "htmlFormattedUrl": "https://<b>thesaurus</b>.yourdictionary.com/<b>back</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Laid-back Synonyms, Laid-back Antonyms | Thesaurus.com",
            "htmlTitle": "Laid-<b>back Synonyms</b>, Laid-back Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/laid-back",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for laid-back at Thesaurus.com with free online thesaurus, antonyms, \nand definitions. Find descriptive alternatives for laid-back.",
            "htmlSnippet": "<b>Synonyms</b> for laid-<b>back</b> at <b>Thesaurus</b>.com with free online <b>thesaurus</b>, antonyms, <br>\nand definitions. Find descriptive alternatives for laid-<b>back</b>.",
            "cacheId": "0Id8xVZAlLIJ",
            "formattedUrl": "https://www.thesaurus.com/browse/laid-back",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/laid-<b>back</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of laid-back | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for laid-back from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for back? | Back Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for back? | <b>Back Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/back.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 7016 synonyms for back and other similar words that you can use instead \nbased on 47 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 7016 <b>synonyms</b> for <b>back</b> and other similar words that you can use instead <br>\nbased on 47 separate contexts from our <b>thesaurus</b>.",
            "cacheId": "CI_zz4LoGbEJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/back.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>back</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Back-to-back Synonyms, Back-to-back Antonyms | Thesaurus.com",
            "htmlTitle": "Back-to-<b>back Synonyms</b>, Back-to-back Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/back-to-back",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for back-to-back at Thesaurus.com with free online thesaurus, \nantonyms, and definitions. Find descriptive alternatives for back-to-back.",
            "htmlSnippet": "<b>Synonyms</b> for <b>back</b>-to-<b>back</b> at <b>Thesaurus</b>.com with free online <b>thesaurus</b>, <br>\nantonyms, and definitions. Find descriptive alternatives for <b>back</b>-to-<b>back</b>.",
            "cacheId": "24Id7Cv7QGgJ",
            "formattedUrl": "https://www.thesaurus.com/browse/back-to-back",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>back</b>-to-<b>back</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of back-to-back | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for back-to-back from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Back Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Back Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/back",
            "displayLink": "www.synonyms.com",
            "snippet": "Princeton's WordNet(5.00 / 1 vote)Rate these synonyms: back, dorsum(noun). the \nposterior part of a human (or animal) body from the neck to the end of the ...",
            "htmlSnippet": "Princeton&#39;s WordNet(5.00 / 1 vote)Rate these <b>synonyms</b>: <b>back</b>, dorsum(noun). the <br>\nposterior part of a human (or animal) body from the neck to the end of the&nbsp;...",
            "cacheId": "zDp65FqSZk0J",
            "formattedUrl": "https://www.synonyms.com/synonym/back",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>back</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/back",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Snapped back Synonyms, Snapped back Antonyms | Thesaurus.com",
            "htmlTitle": "Snapped <b>back Synonyms</b>, Snapped back Antonyms | Thesaurus.com",
            "link": "http://thesaurus.com/browse/snapped%20back",
            "displayLink": "thesaurus.com",
            "snippet": "Synonyms for snapped back at Thesaurus.com with free online thesaurus, \nantonyms, and definitions. Dictionary and Word of the Day.",
            "htmlSnippet": "<b>Synonyms</b> for snapped <b>back</b> at <b>Thesaurus</b>.com with free online <b>thesaurus</b>, <br>\nantonyms, and definitions. Dictionary and Word of the Day.",
            "cacheId": "s-naOrGSpSUJ",
            "formattedUrl": "thesaurus.com/browse/snapped%20back",
            "htmlFormattedUrl": "<b>thesaurus</b>.com/browse/snapped%20<b>back</b>",
            "pagemap": {
                "metatags": [
                    {
                        "og:image": "http://static.sfdict.com/thescloud/img/thesaurus_social_logo-208ba.png",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "og:title": "I found great synonyms for \"snapped back\" on the new Thesaurus.com!",
                        "fb:admins": "100000304287730,109125464873"
                    }
                ],
                "cse_image": [
                    {
                        "src": "http://static.sfdict.com/thescloud/img/thesaurus_social_logo-208ba.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "report back synonyms with definition | Macmillan Thesaurus",
            "htmlTitle": "report <b>back synonyms</b> with definition | Macmillan Thesaurus",
            "link": "https://www.macmillanthesaurus.com/us/report-back",
            "displayLink": "www.macmillanthesaurus.com",
            "snippet": "Synonyms for 'report back': tell, deliver, report, point out, brief, relay, divulge, \nadvise, put onto, let someone know, acquaint, alert, apprise.",
            "htmlSnippet": "<b>Synonyms</b> for &#39;report <b>back</b>&#39;: tell, deliver, report, point out, brief, relay, divulge, <br>\nadvise, put onto, let someone know, acquaint, alert, apprise.",
            "cacheId": "_cLIj1970QUJ",
            "formattedUrl": "https://www.macmillanthesaurus.com/us/report-back",
            "htmlFormattedUrl": "https://www.macmillan<b>thesaurus</b>.com/us/report-<b>back</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRu4knELk1zAuP1o_6qYG9N8wS3QGS-A7cIYqn929xoH6UJR26QGBoXkuE",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/us/external/images/logoThesaurus.png?version=1.0.46",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no",
                        "og:title": "report back synonyms with definition | Macmillan Thesaurus",
                        "title": "report back synonyms with definition | Macmillan Thesaurus",
                        "og:url": "https://www.macmillanthesaurus.com/us/report-back",
                        "og:description": "Synonyms for 'report back': tell, deliver, report, point out, brief, relay, divulge, advise, put onto, let someone know, acquaint, alert, apprise"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillanthesaurus.com/us/external/images/bg-wave.svg?version=1.0.46"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Go back Synonyms | Collins English Thesaurus",
            "htmlTitle": "Go <b>back Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/go-back",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for go back: return, revert | Collins English Thesaurus.",
            "htmlSnippet": "Another word for go <b>back</b>: return, revert | Collins English <b>Thesaurus</b>.",
            "cacheId": "Ng0PieEVTVUJ",
            "formattedUrl": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/go-back",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/us/dictionary/english-<b>thesaurus</b>/go-<b>back</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTErRtm81ioo5cAR6Dn8N1IWDT5nl6xu6Eg-vrc3_QJ6OnJgHQi_5O9btGR",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/us/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/us/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Go back Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "d26227ed-9254-4e41-ad13-10d04e150a33",
                        "_csrf_parameter": "_csrf",
                        "title": "Go back Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for go back: return, revert | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/go-back"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/us/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        }
    ]
}
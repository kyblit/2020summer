{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - circle  synonyms",
                "totalResults": "45000000",
                "searchTerms": "circle  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - circle  synonyms",
                "totalResults": "45000000",
                "searchTerms": "circle  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.246696,
        "formattedSearchTime": "0.25",
        "totalResults": "45000000",
        "formattedTotalResults": "45,000,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Circle Synonyms, Circle Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Circle Synonyms</b>, Circle Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/circle",
            "displayLink": "www.thesaurus.com",
            "snippet": "circle · nounorb, loop, round figure · noungroup of close friends, associates · \nverbgo around, circumnavigate ...",
            "htmlSnippet": "<b>circle</b> &middot; nounorb, loop, round figure &middot; noungroup of close friends, associates &middot; <br>\nverbgo around, circumnavigate&nbsp;...",
            "cacheId": "YXCEdJBn84EJ",
            "formattedUrl": "https://www.thesaurus.com/browse/circle",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>circle</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of circle | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for circle from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Circle Synonyms, Circle Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Circle Synonyms</b>, Circle Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/circle",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms & Antonyms of circle · 1 something with a perfectly round \ncircumference · 2 a circular strip · 3 a group of people sharing a common interest \nand relating ...",
            "htmlSnippet": "<b>Synonyms</b> &amp; Antonyms of <b>circle</b> &middot; 1 something with a perfectly round <br>\ncircumference &middot; 2 a circular strip &middot; 3 a group of people sharing a common interest <br>\nand relating&nbsp;...",
            "cacheId": "qboZjFoTCRAJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/circle",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>circle</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for CIRCLE",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/circle",
                        "og:title": "Thesaurus results for CIRCLE",
                        "twitter:aria-text": "Share more words for circle on Twitter",
                        "og:aria-text": "Post more words for circle to Facebook",
                        "og:description": "Circle: something with a perfectly round circumference. Synonyms: cirque, ring, round… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Circle: something with a perfectly round circumference. Synonyms: cirque, ring, round… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/circle"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Circle synonyms | Best 186 synonyms for circle",
            "htmlTitle": "<b>Circle synonyms</b> | Best 186 synonyms for circle",
            "link": "https://thesaurus.yourdictionary.com/circle",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 186 synonyms for circle, including: ring, wheel, sphere, disk, circlet, \ndetour, globe, halo, crown, corona, cirque and more... Find another word for circle\n ...",
            "htmlSnippet": "The best 186 <b>synonyms</b> for <b>circle</b>, including: ring, wheel, sphere, disk, circlet, <br>\ndetour, globe, halo, crown, corona, cirque and more... Find another word for <b>circle</b><br>\n&nbsp;...",
            "cacheId": "onp11KyRz14J",
            "formattedUrl": "https://thesaurus.yourdictionary.com/circle",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>circle</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for circle? | Circle Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for circle? | <b>Circle Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/circle.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 5616 synonyms for circle and other similar words that you can use instead \nbased on 31 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 5616 <b>synonyms</b> for <b>circle</b> and other similar words that you can use instead <br>\nbased on 31 separate contexts from our thesaurus.",
            "cacheId": "aXMjbOuk5MsJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/circle.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>circle</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "2 907 Circle synonyms - Other Words for Circle",
            "htmlTitle": "2 907 <b>Circle synonyms</b> - Other Words for Circle",
            "link": "https://www.powerthesaurus.org/circle/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Circle synonyms. Top synonyms for circle (other words for circle) are ring, \nencircle and group.",
            "htmlSnippet": "<b>Circle synonyms</b>. Top synonyms for circle (other words for circle) are ring, <br>\nencircle and group.",
            "cacheId": "KO88bqXVj-QJ",
            "formattedUrl": "https://www.powerthesaurus.org/circle/synonyms",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>circle</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#ffffff",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "2 907 Circle synonyms - Other Words for Circle",
                        "og:description": "Circle synonyms. Top synonyms for circle (other words for circle) are ring, encircle and group.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/circle/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Circle Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Circle Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/circle",
            "displayLink": "www.synonyms.com",
            "snippet": "Princeton's WordNet(0.00 / 0 votes)Rate these synonyms: · circle(noun) · set, \ncircle, band, lot(noun) · circle(noun) · lap, circle, circuit(noun) · traffic circle, circle, \nrotary, ...",
            "htmlSnippet": "Princeton&#39;s WordNet(0.00 / 0 votes)Rate these <b>synonyms</b>: &middot; <b>circle</b>(noun) &middot; set, <br>\n<b>circle</b>, band, lot(noun) &middot; <b>circle</b>(noun) &middot; lap, <b>circle</b>, circuit(noun) &middot; traffic <b>circle</b>, <b>circle</b>, <br>\nrotary,&nbsp;...",
            "cacheId": "uzF_HgC3uMsJ",
            "formattedUrl": "https://www.synonyms.com/synonym/circle",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>circle</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/circle",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Circle Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Circle Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/circle",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for circle: ring, round, band, disc, loop | Collins English Thesaurus.",
            "htmlSnippet": "Another word for <b>circle</b>: ring, round, band, disc, loop | Collins English Thesaurus.",
            "cacheId": "xnQrAeVbVhAJ",
            "formattedUrl": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/circle",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/us/dictionary/english-thesaurus/<b>circle</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Circle Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "272f41d2-e92b-45e8-bba5-0c99c0ae95a3",
                        "_csrf_parameter": "_csrf",
                        "title": "Circle Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for circle: ring, round, band, disc, loop | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/circle"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Circle Synonyms, Circle Antonyms: Page 3 of 131 | Thesaurus.com",
            "htmlTitle": "<b>Circle Synonyms</b>, Circle Antonyms: Page 3 of 131 | Thesaurus.com",
            "link": "http://www.thesaurus.com/browse/circle/3",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for circle at Thesaurus.com with free online thesaurus, antonyms, and \ndefinitions. Find descriptive alternatives for circle.",
            "htmlSnippet": "<b>Synonyms</b> for <b>circle</b> at Thesaurus.com with free online thesaurus, antonyms, and <br>\ndefinitions. Find descriptive alternatives for <b>circle</b>.",
            "cacheId": "goCv6pz6bl0J",
            "formattedUrl": "www.thesaurus.com/browse/circle/3",
            "htmlFormattedUrl": "www.thesaurus.com/browse/<b>circle</b>/3",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of circle | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for circle from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for circle | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>circle</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/circle",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "WTZYikbmZREJ",
            "formattedUrl": "https://www.synonym.com/synonyms/circle",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>circle</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for circle",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for circle",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "circle | definition: ellipse in which the two axes are of equal length; a plane curve generated by one point moving at a constant distance from a fixed point | synonyms: epicycle, circle of curvature, ellipse, osculating circle, circlet, arc, oval, equator| antonyms: decompress, lack, untie, detach, aged",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "circle | definition: ellipse in which the two axes are of equal length; a plane curve generated by one point moving at a constant distance from a fixed point | synonyms: epicycle, circle of curvature, ellipse, osculating circle, circlet, arc, oval, equator| antonyms: decompress, lack, untie, detach, aged",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/circle"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for CIRCLE - Thesaurus.net",
            "htmlTitle": "<b>Synonyms</b> for <b>CIRCLE</b> - Thesaurus.net",
            "link": "https://www.thesaurus.net/circle",
            "displayLink": "www.thesaurus.net",
            "snippet": "Synonyms for CIRCLE: horde, multitude, engrave, stipple, the silent majority, \nfigure eight, galaxy, huddle, circumscribe, gyre, rabble, age group, weave, scrum,\n ...",
            "htmlSnippet": "<b>Synonyms</b> for <b>CIRCLE</b>: horde, multitude, engrave, stipple, the silent majority, <br>\nfigure eight, galaxy, huddle, circumscribe, gyre, rabble, age group, weave, scrum,<br>\n&nbsp;...",
            "cacheId": "F-wOWfx05tEJ",
            "formattedUrl": "https://www.thesaurus.net/circle",
            "htmlFormattedUrl": "https://www.thesaurus.net/<b>circle</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTHmPbL6L3tWwemgnxXK_eIzcEGEcFI39SJD35kF79sVRInWi-xNtX9sw",
                        "width": "312",
                        "height": "53"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.net/public/desktop/images/logo.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for CIRCLE, Antonyms for CIRCLE - Thesaurus.net",
                        "og:site_name": "Thesaurus.net",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "circle | synonyms: horde, multitude, engrave, stipple, the silent majority, figure eight, galaxy, huddle, circumscribe, gyre, rabble, age group, weave, scrum, squirm, sector, semicircle, sickle, street, melee, swarm, hedge, mark out, twirl, impress, carve, diaspora, waltz, hem, snake, procession, bubble, half-moon, crush, throng, wriggle, hump, table, cylinder, press, twist, legion, bulge, cup, wander, delineate, swirl, section, bag, curl, cone, trumpet, wind, gathering, arc, formation, quarter, department, pattern, girdle, wave, row, crisscross, balloon, construct, outline, imprint, scene, streak, heart, territory, swell, rule, tube, parade, scale, chain, print, mark, refract, tilted, spiraled, detoured, angle away, spiralling, angle off, detouring, loop, Incurvated, spiralled, cambers, zigzags, Zigzagged, camber, Cambering, warp, incurvate, Zigzagging, spiraling, Cambered, lean, fall on, over-run, give a bad time, start on, fell on, give one the business, give the business, infests, fall upon, be set, Infes",
                        "og:title": "Synonyms for CIRCLE, Antonyms for CIRCLE - Thesaurus.net",
                        "og:url": "https://www.thesaurus.net/circle",
                        "og:description": "circle | synonyms: horde, multitude, engrave, stipple, the silent majority, figure eight, galaxy, huddle, circumscribe, gyre, rabble, age group, weave, scrum, squirm, sector, semicircle, sickle, street, melee, swarm, hedge, mark out, twirl, impress, carve, diaspora, waltz, hem, snake, procession, bubble, half-moon, crush, throng, wriggle, hump, table, cylinder, press, twist, legion, bulge, cup, wander, delineate, swirl, section, bag, curl, cone, trumpet, wind, gathering, arc, formation, quarter, department, pattern, girdle, wave, row, crisscross, balloon, construct, outline, imprint, scene, streak, heart, territory, swell, rule, tube, parade, scale, chain, print, mark, refract, tilted, spiraled, detoured, angle away, spiralling, angle off, detouring, loop, Incurvated, spiralled, cambers, zigzags, Zigzagged, camber, Cambering, warp, incurvate, Zigzagging, spiraling, Cambered, lean, fall on, over-run, give a bad time, start on, fell on, give one the business, give the business, infests, fall upon, be set, Infes"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.net/public/desktop/images/logo.png"
                    }
                ]
            }
        }
    ]
}
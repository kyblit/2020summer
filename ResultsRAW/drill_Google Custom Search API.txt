{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - drill  synonyms",
                "totalResults": "4470000",
                "searchTerms": "drill  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - drill  synonyms",
                "totalResults": "4470000",
                "searchTerms": "drill  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.249308,
        "formattedSearchTime": "0.25",
        "totalResults": "4470000",
        "formattedTotalResults": "4,470,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Drill Synonyms, Drill Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Drill Synonyms</b>, Drill Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/drill",
            "displayLink": "www.thesaurus.com",
            "snippet": "RELATED WORDS AND SYNONYMS FOR DRILL. aerobics. noun, adjective\nexercise regime designed to increase heart and lung activity while toning \nmuscles.",
            "htmlSnippet": "RELATED WORDS AND <b>SYNONYMS</b> FOR <b>DRILL</b>. aerobics. noun, adjective<br>\nexercise regime designed to increase heart and lung activity while toning <br>\nmuscles.",
            "cacheId": "-MDSAW7yresJ",
            "formattedUrl": "https://www.thesaurus.com/browse/drill",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>drill</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of drill | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for drill from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Drill Synonyms, Drill Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Drill Synonyms</b>, Drill Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/drill",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms of drill · custom, · fashion, · habit, · practice · (also practise), · trick, · \nwont.",
            "htmlSnippet": "<b>Synonyms</b> of <b>drill</b> &middot; custom, &middot; fashion, &middot; habit, &middot; practice &middot; (also practise), &middot; trick, &middot; <br>\nwont.",
            "cacheId": "qlQvr8Zaf6gJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/drill",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>drill</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for DRILL",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/drill",
                        "og:title": "Thesaurus results for DRILL",
                        "twitter:aria-text": "Share more words for drill on Twitter",
                        "og:aria-text": "Post more words for drill to Facebook",
                        "og:description": "Drill: an established and often automatic or monotonous series of actions followed when engaging in some activity. Synonyms: grind, groove, lockstep… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Drill: an established and often automatic or monotonous series of actions followed when engaging in some activity. Synonyms: grind, groove, lockstep… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/drill"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Drill synonyms | Best 81 synonyms for drill",
            "htmlTitle": "<b>Drill synonyms</b> | Best 81 synonyms for drill",
            "link": "https://thesaurus.yourdictionary.com/drill",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 81 synonyms for drill, including: preparation, repetition, , training, \nmaneuvers, marching, parade, close-order drill, open-order drill, conditioning, \nsurvival ...",
            "htmlSnippet": "The best 81 <b>synonyms</b> for <b>drill</b>, including: preparation, repetition, , training, <br>\nmaneuvers, marching, parade, close-order <b>drill</b>, open-order <b>drill</b>, conditioning, <br>\nsurvival&nbsp;...",
            "cacheId": "SMztmIi4HfkJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/drill",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>drill</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Drill Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Drill Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/drill",
            "displayLink": "www.synonyms.com",
            "snippet": "Princeton's WordNet(0.00 / 0 votes)Rate these synonyms: · drill(noun) · drill, \nMandrillus leucophaeus(noun) · exercise, practice, drill, practice session, \nrecitation( ...",
            "htmlSnippet": "Princeton&#39;s WordNet(0.00 / 0 votes)Rate these <b>synonyms</b>: &middot; <b>drill</b>(noun) &middot; <b>drill</b>, <br>\nMandrillus leucophaeus(noun) &middot; exercise, practice, <b>drill</b>, practice session, <br>\nrecitation(&nbsp;...",
            "cacheId": "BYwLujNTnT4J",
            "formattedUrl": "https://www.synonyms.com/synonym/drill",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>drill</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/drill",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for drill? | Drill Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for drill? | <b>Drill Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/drill.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 4191 synonyms for drill and other similar words that you can use instead \nbased on 23 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 4191 <b>synonyms</b> for <b>drill</b> and other similar words that you can use instead <br>\nbased on 23 separate contexts from our thesaurus.",
            "cacheId": "o0oILV5xW84J",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/drill.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>drill</b>.html",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTOCW06SHzArYvuJWGlv8nOu591mQ3PvkPO1hZUjr2VdWOP3rrVchxm2cqw",
                        "width": "388",
                        "height": "130"
                    }
                ],
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://wordhippo.s3.amazonaws.com/what-is/img/mobile-v2/ios-app-store-304x102.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Drill Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Drill Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/drill",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for drill: bit, borer, brace, gimlet, rotary tool | Collins English \nThesaurus.",
            "htmlSnippet": "Another word for <b>drill</b>: bit, borer, brace, gimlet, rotary tool | Collins English <br>\nThesaurus.",
            "cacheId": "vOov8HJwRcIJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/drill",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/<b>drill</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Drill Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "ed53102e-486b-48d8-ac81-9aa1af375ca2",
                        "_csrf_parameter": "_csrf",
                        "title": "Drill Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for drill: bit, borer, brace, gimlet, rotary tool | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/drill"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "2 433 Drill Synonyms and 19 Drill Antonyms | Drill in Thesaurus",
            "htmlTitle": "2 433 <b>Drill Synonyms</b> and 19 Drill Antonyms | Drill in Thesaurus",
            "link": "https://www.powerthesaurus.org/drill",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Synonyms (Other Words) for Drill & Antonyms (Opposite Meaning) for Drill.",
            "htmlSnippet": "<b>Synonyms</b> (Other Words) for <b>Drill</b> &amp; Antonyms (Opposite Meaning) for <b>Drill</b>.",
            "cacheId": "FXFZRWbqxcsJ",
            "formattedUrl": "https://www.powerthesaurus.org/drill",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>drill</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "2 433 Drill Synonyms and 19 Drill Antonyms | Drill in Thesaurus",
                        "og:description": "Synonyms (Other Words) for Drill & Antonyms (Opposite Meaning) for Drill.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/drill",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for drill | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>drill</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/drill",
            "displayLink": "www.synonym.com",
            "snippet": "1. drill · 2. counter-drill · 3. drill · 4. drill · 5. drill · 6. drill · 7. drill · 8. drill.",
            "htmlSnippet": "1. <b>drill</b> &middot; 2. counter-<b>drill</b> &middot; 3. <b>drill</b> &middot; 4. <b>drill</b> &middot; 5. <b>drill</b> &middot; 6. <b>drill</b> &middot; 7. <b>drill</b> &middot; 8. <b>drill</b>.",
            "cacheId": "UMxL1EVAp-kJ",
            "formattedUrl": "https://www.synonym.com/synonyms/drill",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>drill</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for drill",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for drill",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "drill | definition: make a hole, especially with a pointed power or hand tool | synonyms: bore, trepan, cut, spud, counter-drill| antonyms: interest, inflate, lengthen, deflate, increase",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "drill | definition: make a hole, especially with a pointed power or hand tool | synonyms: bore, trepan, cut, spud, counter-drill| antonyms: interest, inflate, lengthen, deflate, increase",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/drill"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "TOEFL Vocabulary Drill Synonyms 10 (50 questions) - YouTube",
            "htmlTitle": "TOEFL Vocabulary <b>Drill Synonyms</b> 10 (50 questions) - YouTube",
            "link": "https://www.youtube.com/watch?v=Ry6PLZOyGFA",
            "displayLink": "www.youtube.com",
            "snippet": "May 19, 2018 ... VOCABULARY LIST: 1). Proponent 2). Abrupt 3). Debris 4). Albeit 5). Cessation 6\n). Petrified 7). Crater 8). Furnace 9). Usher 10). Exploit 11).",
            "htmlSnippet": "May 19, 2018 <b>...</b> VOCABULARY LIST: 1). Proponent 2). Abrupt 3). Debris 4). Albeit 5). Cessation 6<br>\n). Petrified 7). Crater 8). Furnace 9). Usher 10). Exploit 11).",
            "cacheId": "pkkOGa-O0AsJ",
            "formattedUrl": "https://www.youtube.com/watch?v=Ry6PLZOyGFA",
            "htmlFormattedUrl": "https://www.youtube.com/watch?v=Ry6PLZOyGFA",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT6vXuBBFSiNYMFreT99myB0DtlluPQzFW6j9Q4QifGzRvyQqm5c_45zQg",
                        "width": "259",
                        "height": "194"
                    }
                ],
                "imageobject": [
                    {
                        "width": "480",
                        "url": "https://i.ytimg.com/vi/Ry6PLZOyGFA/hqdefault.jpg",
                        "height": "360"
                    }
                ],
                "person": [
                    {
                        "name": "Genii Juku",
                        "url": "http://www.youtube.com/channel/UCRJUBc42Vcu7ggmZPkz14VQ"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://i.ytimg.com/vi/Ry6PLZOyGFA/hqdefault.jpg",
                        "twitter:app:url:iphone": "vnd.youtube://www.youtube.com/watch?v=Ry6PLZOyGFA&feature=applinks",
                        "twitter:app:id:googleplay": "com.google.android.youtube",
                        "theme-color": "#ff0000",
                        "og:image:width": "480",
                        "twitter:card": "player",
                        "og:site_name": "YouTube",
                        "twitter:url": "https://www.youtube.com/watch?v=Ry6PLZOyGFA",
                        "twitter:app:url:ipad": "vnd.youtube://www.youtube.com/watch?v=Ry6PLZOyGFA&feature=applinks",
                        "al:android:package": "com.google.android.youtube",
                        "twitter:app:name:googleplay": "YouTube",
                        "title": "TOEFL Vocabulary Drill Synonyms 10 (50 questions)",
                        "al:ios:url": "vnd.youtube://www.youtube.com/watch?v=Ry6PLZOyGFA&feature=applinks",
                        "twitter:app:id:iphone": "544007664",
                        "og:description": "VOCABULARY LIST: 1). Proponent 2). Abrupt 3). Debris 4). Albeit 5). Cessation 6). Petrified 7). Crater 8). Furnace 9). Usher 10). Exploit 11). Prior 12). Vit...",
                        "al:ios:app_store_id": "544007664",
                        "twitter:image": "https://i.ytimg.com/vi/Ry6PLZOyGFA/hqdefault.jpg",
                        "twitter:player": "https://www.youtube.com/embed/Ry6PLZOyGFA",
                        "twitter:player:height": "360",
                        "twitter:site": "@youtube",
                        "og:video:type": "text/html",
                        "og:video:height": "360",
                        "og:video:url": "https://www.youtube.com/embed/Ry6PLZOyGFA",
                        "og:type": "video.other",
                        "twitter:title": "TOEFL Vocabulary Drill Synonyms 10 (50 questions)",
                        "al:ios:app_name": "YouTube",
                        "og:title": "TOEFL Vocabulary Drill Synonyms 10 (50 questions)",
                        "og:image:height": "360",
                        "twitter:app:id:ipad": "544007664",
                        "al:web:url": "https://www.youtube.com/watch?v=Ry6PLZOyGFA&feature=applinks",
                        "og:video:secure_url": "https://www.youtube.com/embed/Ry6PLZOyGFA",
                        "og:video:tag": "TOEFL",
                        "og:video:width": "480",
                        "al:android:url": "vnd.youtube://www.youtube.com/watch?v=Ry6PLZOyGFA&feature=applinks",
                        "fb:app_id": "87741124305",
                        "twitter:app:url:googleplay": "https://www.youtube.com/watch?v=Ry6PLZOyGFA",
                        "twitter:app:name:ipad": "YouTube",
                        "twitter:description": "VOCABULARY LIST: 1). Proponent 2). Abrupt 3). Debris 4). Albeit 5). Cessation 6). Petrified 7). Crater 8). Furnace 9). Usher 10). Exploit 11). Prior 12). Vit...",
                        "og:url": "https://www.youtube.com/watch?v=Ry6PLZOyGFA",
                        "twitter:player:width": "480",
                        "al:android:app_name": "YouTube",
                        "twitter:app:name:iphone": "YouTube"
                    }
                ],
                "videoobject": [
                    {
                        "embedurl": "https://www.youtube.com/embed/Ry6PLZOyGFA",
                        "playertype": "HTML5 Flash",
                        "isfamilyfriendly": "True",
                        "uploaddate": "2018-05-19",
                        "description": "VOCABULARY LIST: 1). Proponent 2). Abrupt 3). Debris 4). Albeit 5). Cessation 6). Petrified 7). Crater 8). Furnace 9). Usher 10). Exploit 11). Prior 12). Vit...",
                        "videoid": "Ry6PLZOyGFA",
                        "url": "https://www.youtube.com/watch?v=Ry6PLZOyGFA",
                        "duration": "PT6M14S",
                        "unlisted": "False",
                        "name": "TOEFL Vocabulary Drill Synonyms 10 (50 questions)",
                        "paid": "False",
                        "width": "480",
                        "regionsallowed": "AD,AE,AF,AG,AI,AL,AM,AO,AQ,AR,AS,AT,AU,AW,AX,AZ,BA,BB,BD,BE,BF,BG,BH,BI,BJ,BL,BM,BN,BO,BQ,BR,BS,BT,BV,BW,BY,BZ,CA,CC,CD,CF,CG,CH,CI,CK,CL,CM,CN,CO,CR,CU,CV,CW,CX,CY,CZ,DE,DJ,DK,DM,DO,DZ,EC,EE,EG,EH...",
                        "genre": "Education",
                        "interactioncount": "83",
                        "channelid": "UCRJUBc42Vcu7ggmZPkz14VQ",
                        "datepublished": "2018-05-19",
                        "thumbnailurl": "https://i.ytimg.com/vi/Ry6PLZOyGFA/hqdefault.jpg",
                        "height": "360"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://i.ytimg.com/vi/Ry6PLZOyGFA/hqdefault.jpg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "fire drill synonyms with definition | Macmillan Thesaurus",
            "htmlTitle": "fire <b>drill synonyms</b> with definition | Macmillan Thesaurus",
            "link": "https://www.macmillanthesaurus.com/fire-drill",
            "displayLink": "www.macmillanthesaurus.com",
            "snippet": "Related terms for 'fire drill': damper, extinguisher, fire brigade, fire certificate, fire \ndepartment, fire extinguisher, firefighter, firefighting.",
            "htmlSnippet": "Related terms for &#39;fire <b>drill</b>&#39;: damper, extinguisher, fire brigade, fire certificate, fire <br>\ndepartment, fire extinguisher, firefighter, firefighting.",
            "cacheId": "jupzsTgRwzEJ",
            "formattedUrl": "https://www.macmillanthesaurus.com/fire-drill",
            "htmlFormattedUrl": "https://www.macmillanthesaurus.com/fire-<b>drill</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSn8aJXlwwAX3OWTuwauXtiFPozKnrVC_Ju4YY1fbdiK38b2ikmepQkqA0",
                        "width": "275",
                        "height": "183"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/external/images/logoThesaurus.png?version=1.0.43",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no",
                        "og:title": "fire drill synonyms with definition | Macmillan Thesaurus",
                        "title": "fire drill synonyms with definition | Macmillan Thesaurus",
                        "og:url": "https://www.macmillanthesaurus.com/fire-drill",
                        "og:description": "Related terms for 'fire drill': damper, extinguisher, fire brigade, fire certificate, fire department, fire extinguisher, firefighter, firefighting"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillanthesaurus.com/external/images/sotd/Sunday_Red.jpg?version=1.0.43"
                    }
                ]
            }
        }
    ]
}
{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - shakespeare  synonyms",
                "totalResults": "4090000",
                "searchTerms": "shakespeare  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - shakespeare  synonyms",
                "totalResults": "4090000",
                "searchTerms": "shakespeare  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.246561,
        "formattedSearchTime": "0.25",
        "totalResults": "4090000",
        "formattedTotalResults": "4,090,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "ShakespearesWords.com",
            "htmlTitle": "ShakespearesWords.com",
            "link": "https://www.shakespeareswords.com/Public/Thesaurus.aspx",
            "displayLink": "www.shakespeareswords.com",
            "snippet": "1957 items ... ... meaning and you want to find out which Shakespearean words express it ... we \ninclude Shakespeare's use of mother to mean 'womanish qualities', ...",
            "htmlSnippet": "1957 items <b>...</b> ... meaning and you want to find out which <b>Shakespearean</b> words express it ... we <br>\ninclude <b>Shakespeare&#39;s</b> use of mother to mean &#39;womanish qualities&#39;,&nbsp;...",
            "cacheId": "nVCSFXZNuj4J",
            "formattedUrl": "https://www.shakespeareswords.com/Public/Thesaurus.aspx",
            "htmlFormattedUrl": "https://www.<b>shakespeares</b>words.com/Public/Thesaurus.aspx",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRpqtMvM9H2YyJL1RgrwYgq5iAcN4e7p5G--g4zOanjpsH82VT9zWuH",
                        "width": "94",
                        "height": "94"
                    }
                ],
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=0.5"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.shakespeareswords.com/Images/ShakespearePortrait100px.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for william-shakespeare | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> for william-<b>shakespeare</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/william-shakespeare",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "uBSMIeo97foJ",
            "formattedUrl": "https://www.synonym.com/synonyms/william-shakespeare",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/william-<b>shakespeare</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSNPaQwyj2t_vB9GfyNeHSwmvM09gteOLaZBnBpO4dPbZHGXuZcGOBvQKFb",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for william-shakespeare",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms for william-shakespeare",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "william-shakespeare | definition: English poet and dramatist considered one of the greatest English writers (1564-1616) | synonyms: William Shakspere, Shakespeare, Bard of Avon, Shakspere",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "william-shakespeare | definition: English poet and dramatist considered one of the greatest English writers (1564-1616) | synonyms: William Shakspere, Shakespeare, Bard of Avon, Shakspere",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/william-shakespeare"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/dist/img/wordtigo-logo.987687df.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for SHAKESPEARE - Thesaurus.net",
            "htmlTitle": "<b>Synonyms</b> for <b>SHAKESPEARE</b> - Thesaurus.net",
            "link": "https://www.thesaurus.net/shakespeare",
            "displayLink": "www.thesaurus.net",
            "snippet": "Synonyms for SHAKESPEARE: shakspere, bard of avon, bard, timon of athens, \ntermagant, William Shakespeare, William Shakspere, the bard, shakspere.",
            "htmlSnippet": "<b>Synonyms</b> for <b>SHAKESPEARE</b>: shakspere, bard of avon, bard, timon of athens, <br>\ntermagant, William <b>Shakespeare</b>, William Shakspere, the bard, shakspere.",
            "cacheId": "kUD8dkzQo2kJ",
            "formattedUrl": "https://www.thesaurus.net/shakespeare",
            "htmlFormattedUrl": "https://www.thesaurus.net/<b>shakespeare</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTHmPbL6L3tWwemgnxXK_eIzcEGEcFI39SJD35kF79sVRInWi-xNtX9sw",
                        "width": "312",
                        "height": "53"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.net/public/desktop/images/logo.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for SHAKESPEARE - Thesaurus.net",
                        "og:site_name": "Thesaurus.net",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "shakespeare | synonyms: shakspere, bard of avon, bard, timon of athens, termagant, William Shakespeare, William Shakspere, the bard, shakspere",
                        "og:title": "Synonyms for SHAKESPEARE - Thesaurus.net",
                        "og:url": "https://www.thesaurus.net/shakespeare",
                        "og:description": "shakespeare | synonyms: shakspere, bard of avon, bard, timon of athens, termagant, William Shakespeare, William Shakspere, the bard, shakspere"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.net/public/desktop/images/logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Shakespeare Translator",
            "htmlTitle": "<b>Shakespeare</b> Translator",
            "link": "https://reference.yourdictionary.com/translation/shakespeare-translator.html",
            "displayLink": "reference.yourdictionary.com",
            "snippet": "Online Shakespeare Translator. Shakespeare's words are so different from the \nones we use today. Alas, don't fret. Thy meaning will be obvious in just a moment\n ...",
            "htmlSnippet": "Online <b>Shakespeare</b> Translator. <b>Shakespeare&#39;s</b> words are so different from the <br>\nones we use today. Alas, don&#39;t fret. Thy meaning will be obvious in just a moment<br>\n&nbsp;...",
            "cacheId": "hjlwFETAgcUJ",
            "formattedUrl": "https://reference.yourdictionary.com/translation/shakespeare-translator.html",
            "htmlFormattedUrl": "https://reference.yourdictionary.com/translation/<b>shakespeare</b>-translator.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0",
                        "author": "YourDictionary staff"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Shakespeare Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Shakespeare Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/shakespeare",
            "displayLink": "www.synonyms.com",
            "snippet": "Find all the synonyms and alternative words for shakespeare at Synonyms.com, \nthe largest free online thesaurus, antonyms, definitions and translations ...",
            "htmlSnippet": "Find all the <b>synonyms</b> and alternative words for <b>shakespeare</b> at <b>Synonyms</b>.com, <br>\nthe largest free online thesaurus, antonyms, definitions and translations&nbsp;...",
            "cacheId": "KsNkuox6rdIJ",
            "formattedUrl": "https://www.synonyms.com/synonym/shakespeare",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>shakespeare</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/shakespeare",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Wooing Synonyms, Wooing Antonyms | Thesaurus.com",
            "htmlTitle": "Wooing <b>Synonyms</b>, Wooing Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/wooing",
            "displayLink": "www.thesaurus.com",
            "snippet": "The wooing on Shakespeare's side was nothing but pastime, though it led to \nmarriage. It is all concerned with the wooing of young Adonis by Venus, an older\n ...",
            "htmlSnippet": "The wooing on <b>Shakespeare&#39;s</b> side was nothing but pastime, though it led to <br>\nmarriage. It is all concerned with the wooing of young Adonis by Venus, an older<br>\n&nbsp;...",
            "cacheId": "sv7dnYhQIoAJ",
            "formattedUrl": "https://www.thesaurus.com/browse/wooing",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/wooing",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of wooing | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for wooing from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Shakespeare Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Shakespeare Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/shakespeare",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for Shakespeare: Collins Dictionary Definition | Collins English \nThesaurus.",
            "htmlSnippet": "Another word for <b>Shakespeare</b>: Collins Dictionary Definition | Collins English <br>\nThesaurus.",
            "cacheId": "ipi41mb4YbUJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/shakespeare",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/<b>shakespeare</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Shakespeare Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "55d6dfa1-49e0-4d1f-b8cc-9b16b2a47cdd",
                        "_csrf_parameter": "_csrf",
                        "title": "Shakespeare Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for Shakespeare: Collins Dictionary Definition | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/shakespeare"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Shakespeare synonyms | Best 4 synonyms for shakespeare",
            "htmlTitle": "<b>Shakespeare synonyms</b> | Best 4 synonyms for shakespeare",
            "link": "https://thesaurus.yourdictionary.com/shakespeare",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 4 synonyms for shakespeare, including: william shakespeare, \nshakspere, William Shakspere, bard of avon and more... Find another word for ...",
            "htmlSnippet": "The best 4 <b>synonyms</b> for <b>shakespeare</b>, including: william <b>shakespeare</b>, <br>\nshakspere, William Shakspere, bard of avon and more... Find another word for&nbsp;...",
            "cacheId": "8kZp4CgKg8kJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/shakespeare",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>shakespeare</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "11 Shakespeare Synonyms | Shakespeare in Thesaurus",
            "htmlTitle": "11 <b>Shakespeare Synonyms</b> | Shakespeare in Thesaurus",
            "link": "https://www.powerthesaurus.org/shakespeare",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Synonyms (Other Words) for Shakespeare & Antonyms (Opposite Meaning) for \nShakespeare.",
            "htmlSnippet": "<b>Synonyms</b> (Other Words) for <b>Shakespeare</b> &amp; Antonyms (Opposite Meaning) for <br>\n<b>Shakespeare</b>.",
            "cacheId": "lXv3KvcKblAJ",
            "formattedUrl": "https://www.powerthesaurus.org/shakespeare",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>shakespeare</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "11 Shakespeare Synonyms | Shakespeare in Thesaurus",
                        "og:description": "Synonyms (Other Words) for Shakespeare & Antonyms (Opposite Meaning) for Shakespeare.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/shakespeare",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "SHAKESPEAREAN (adjective) definition and synonyms | Macmillan ...",
            "htmlTitle": "<b>SHAKESPEAREAN</b> (adjective) definition and <b>synonyms</b> | Macmillan ...",
            "link": "https://www.macmillandictionary.com/us/dictionary/american/shakespearean",
            "displayLink": "www.macmillandictionary.com",
            "snippet": "Definition of SHAKESPEAREAN (adjective): written by or relating to William \nShakespeare; using words in the way Shakespeare did.",
            "htmlSnippet": "Definition of <b>SHAKESPEAREAN</b> (adjective): written by or relating to William <br>\n<b>Shakespeare</b>; using words in the way <b>Shakespeare</b> did.",
            "cacheId": "oKzBz9iXCVIJ",
            "formattedUrl": "https://www.macmillandictionary.com/us/dictionary/.../shakespearean",
            "htmlFormattedUrl": "https://www.macmillandictionary.com/us/dictionary/.../<b>shakespearean</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcR0HWKXgLtrjwcEhr9W3R55DSkf0mNkq-IJh0fXydES94LYatkazhUucdrF",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/external/images/social/social_macmillan.jpg?version=4.0.27",
                        "og:type": "website",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=10.0, minimum-scale=0.1",
                        "og:title": "SHAKESPEAREAN (adjective) definition and synonyms | Macmillan Dictionary",
                        "og:url": "https://www.macmillandictionary.com/dictionary/british/shakespearean",
                        "og:description": "Definition of SHAKESPEAREAN (adjective): written by or relating to William Shakespeare; using words in the way Shakespeare did"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillandictionary.com/external/images/games/poppy-sidebar.jpg?version=4.0.27"
                    }
                ]
            }
        }
    ]
}
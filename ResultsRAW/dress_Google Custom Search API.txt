{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - dress  synonyms",
                "totalResults": "13700000",
                "searchTerms": "dress  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - dress  synonyms",
                "totalResults": "13700000",
                "searchTerms": "dress  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.26176,
        "formattedSearchTime": "0.26",
        "totalResults": "13700000",
        "formattedTotalResults": "13,700,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Dress Synonyms, Dress Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Dress Synonyms</b>, Dress Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/dress",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · apparel · attire · costume · frock · garb · gown · suit · uniform.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; apparel &middot; attire &middot; costume &middot; frock &middot; garb &middot; gown &middot; suit &middot; uniform.",
            "cacheId": "7jZn2Dmh4koJ",
            "formattedUrl": "https://www.thesaurus.com/browse/dress",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>dress</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of dress | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for dress from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Dress Synonyms, Dress Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Dress Synonyms</b>, Dress Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/dress",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms of dress. (Entry 1 of 3). 1 a garment with a joined blouse and skirt \nusually worn ...",
            "htmlSnippet": "<b>Synonyms</b> of <b>dress</b>. (Entry 1 of 3). 1 a garment with a joined blouse and skirt <br>\nusually worn&nbsp;...",
            "cacheId": "bNwaq6bnZhgJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/dress",
            "htmlFormattedUrl": "https://www.merriam-webster.com/<b>thesaurus</b>/<b>dress</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for DRESS",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/dress",
                        "og:title": "Thesaurus results for DRESS",
                        "twitter:aria-text": "Share more words for dress on Twitter",
                        "og:aria-text": "Post more words for dress to Facebook",
                        "og:description": "Dress: a garment with a joined blouse and skirt usually worn by a woman or girl. Synonyms: frock, gown, costume… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Dress: a garment with a joined blouse and skirt usually worn by a woman or girl. Synonyms: frock, gown, costume… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/dress"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Dresses Synonyms, Dresses Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Dresses Synonyms</b>, Dresses Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/dresses",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · apparel · attire · costume · frock · garb · gown · suit · uniform.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; apparel &middot; attire &middot; costume &middot; frock &middot; garb &middot; gown &middot; suit &middot; uniform.",
            "cacheId": "0GVBmOrRq8QJ",
            "formattedUrl": "https://www.thesaurus.com/browse/dresses",
            "htmlFormattedUrl": "https://www.<b>thesaurus</b>.com/browse/<b>dresses</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of dresses | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for dresses from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Dress synonyms | Best 190 synonyms for dress",
            "htmlTitle": "<b>Dress synonyms</b> | Best 190 synonyms for dress",
            "link": "https://thesaurus.yourdictionary.com/dress",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 190 synonyms for dress, including: formal dress, apparel, frock, \nenclothe, attire, toilette, basic black dress, gown, jumper, sheath, cocktail-dress \nand ...",
            "htmlSnippet": "The best 190 <b>synonyms</b> for <b>dress</b>, including: formal <b>dress</b>, apparel, frock, <br>\nenclothe, attire, toilette, basic black <b>dress</b>, gown, jumper, sheath, cocktail-<b>dress</b> <br>\nand&nbsp;...",
            "cacheId": "2Q_cH_y7bAwJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/dress",
            "htmlFormattedUrl": "https://<b>thesaurus</b>.yourdictionary.com/<b>dress</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for dress? | Dress Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for dress? | <b>Dress Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/dress.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 5046 synonyms for dress and other similar words that you can use instead \nbased on 35 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 5046 <b>synonyms</b> for <b>dress</b> and other similar words that you can use instead <br>\nbased on 35 separate contexts from our <b>thesaurus</b>.",
            "cacheId": "NAd0fGcUtaYJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/dress.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>dress</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "2 759 Dress synonyms - Other Words for Dress",
            "htmlTitle": "2 759 <b>Dress synonyms</b> - Other Words for Dress",
            "link": "https://www.powerthesaurus.org/dress/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Dress synonyms. Top synonyms for dress (other words for dress) are clothes, \noutfit and clothing.",
            "htmlSnippet": "<b>Dress synonyms</b>. Top synonyms for dress (other words for dress) are clothes, <br>\noutfit and clothing.",
            "cacheId": "7x-BpDI0-eYJ",
            "formattedUrl": "https://www.powerthesaurus.org/dress/synonyms",
            "htmlFormattedUrl": "https://www.power<b>thesaurus</b>.org/<b>dress</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "2 759 Dress synonyms - Other Words for Dress",
                        "og:description": "Dress synonyms. Top synonyms for dress (other words for dress) are clothes, outfit and clothing.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/dress/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "DRESS (verb) definition and synonyms | Macmillan Dictionary",
            "htmlTitle": "<b>DRESS</b> (verb) definition and <b>synonyms</b> | Macmillan Dictionary",
            "link": "https://www.macmillandictionary.com/us/dictionary/american/dress_1",
            "displayLink": "www.macmillandictionary.com",
            "snippet": "Definition of DRESS (verb): put on clothes; cover cleaned injury with soft cloth; \nadd flavour to salad with liquid mixture; prepare chicken, crab etc ...",
            "htmlSnippet": "Definition of <b>DRESS</b> (verb): put on clothes; cover cleaned injury with soft cloth; <br>\nadd flavour to salad with liquid mixture; prepare chicken, crab etc ...",
            "cacheId": "tNPvo1jD7asJ",
            "formattedUrl": "https://www.macmillandictionary.com/us/dictionary/american/dress_1",
            "htmlFormattedUrl": "https://www.macmillandictionary.com/us/dictionary/american/<b>dress</b>_1",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQq1XwZHyhsplz2gqTWfOMdrRK_HTPsdInhEZEectSt8unMIf5yPEGbNA",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/external/images/social/social_macmillan.jpg?version=4.0.27",
                        "og:type": "website",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=10.0, minimum-scale=0.1",
                        "og:title": "DRESS (verb) definition and synonyms | Macmillan Dictionary",
                        "og:url": "https://www.macmillandictionary.com/dictionary/british/dress_1",
                        "og:description": "Definition of DRESS (verb): put on clothes; cover cleaned injury with soft cloth; add flavour to salad with liquid mixture; prepare chicken, crab etc ..."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillandictionary.com/external/images/bg-wave.svg?version=4.0.27"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Dress Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Dress Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/dress",
            "displayLink": "www.synonyms.com",
            "snippet": "Princeton's WordNet(0.00 / 0 votes)Rate these synonyms: · dress, frock(noun) · \nattire, garb, dress(noun) · apparel, wearing apparel, dress, clothes(adj) · full-\ndress, ...",
            "htmlSnippet": "Princeton&#39;s WordNet(0.00 / 0 votes)Rate these <b>synonyms</b>: &middot; <b>dress</b>, frock(noun) &middot; <br>\nattire, garb, <b>dress</b>(noun) &middot; apparel, wearing apparel, <b>dress</b>, clothes(adj) &middot; full-<br>\n<b>dress</b>,&nbsp;...",
            "cacheId": "S5_8tUWlOr0J",
            "formattedUrl": "https://www.synonyms.com/synonym/dress",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>dress</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/dress",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for dress | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>dress</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/dress",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "8o2WFnYOQU0J",
            "formattedUrl": "https://www.synonym.com/synonyms/dress",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>dress</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for dress",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for dress",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "dress | definition: a one-piece garment for a woman; has skirt and bodice | synonyms: kirtle, strapless, slide fastener, chemise, shift, zipper, sack, coatdress, saree, jumper, cocktail dress, gown, muumuu, bodice, pinny, morning dress, polonaise, Mother Hubbard, kaftan, hemline, neckline, woman's clothing, caftan, sari, dirndl, zip fastener, zip, pinafore, sundress, shirtdress, frock, sheath| antonyms: undock, get off, exit, uncover, undeceive",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "dress | definition: a one-piece garment for a woman; has skirt and bodice | synonyms: kirtle, strapless, slide fastener, chemise, shift, zipper, sack, coatdress, saree, jumper, cocktail dress, gown, muumuu, bodice, pinny, morning dress, polonaise, Mother Hubbard, kaftan, hemline, neckline, woman's clothing, caftan, sari, dirndl, zip fastener, zip, pinafore, sundress, shirtdress, frock, sheath| antonyms: undock, get off, exit, uncover, undeceive",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/dress"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Dress | Synonyms of Dress by Lexico",
            "htmlTitle": "<b>Dress</b> | <b>Synonyms</b> of <b>Dress</b> by Lexico",
            "link": "https://www.lexico.com/synonym/dress",
            "displayLink": "www.lexico.com",
            "snippet": "Phrases. dress someone down. 'the president dressed down the media during \nthe press conference'. SYNONYMS.",
            "htmlSnippet": "Phrases. <b>dress</b> someone down. &#39;the president <b>dressed</b> down the media during <br>\nthe press conference&#39;. <b>SYNONYMS</b>.",
            "cacheId": "ovNjGLGv91wJ",
            "formattedUrl": "https://www.lexico.com/synonym/dress",
            "htmlFormattedUrl": "https://www.lexico.com/<b>synonym</b>/<b>dress</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT-fHqiHsELFzBLFkvhF3MUNoGyDc02pwOeICq9JD-vhUo5TV_0a-_rYOwh",
                        "width": "291",
                        "height": "173"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#50b46c",
                        "og:image": "https://www.lexico.com/lexico-logo.png",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Dress | Synonyms of Dress by Lexico",
                        "og:type": "website",
                        "theme-color": "#50b46c",
                        "og:site_name": "Lexico Dictionaries | English",
                        "og:title": "Dress | Synonyms of Dress by Lexico",
                        "msapplication-tileimage": "/mstile-144x144.png",
                        "csrf-param": "authenticity_token",
                        "og:description": "What is the definition of dress? What is the meaning of dress? How do you use dress in a sentence? What are synonyms for dress?",
                        "twitter:image": "https://www.lexico.com/lexico-logo.png",
                        "viewport": "width=device-width, initial-scale=1, shrink-to-fit=no",
                        "twitter:description": "What is the definition of dress? What is the meaning of dress? How do you use dress in a sentence? What are synonyms for dress?",
                        "csrf-token": "i9xtKXTQq0A2u0/1ZAAm0VWTA5+Pc2cqLwtrs2cYfdpLMs6tttQyXY5lTrXZyALJHk915GGCIiypeJ4RWNRFyw==",
                        "og:url": "https://www.lexico.com/synonym/dress"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.lexico.com/lexico-logo.png"
                    }
                ]
            }
        }
    ]
}
{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - foot  synonyms",
                "totalResults": "42300000",
                "searchTerms": "foot  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - foot  synonyms",
                "totalResults": "42300000",
                "searchTerms": "foot  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.244799,
        "formattedSearchTime": "0.24",
        "totalResults": "42300000",
        "formattedTotalResults": "42,300,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Foot Synonyms, Foot Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Foot Synonyms</b>, Foot Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/foot",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for foot. hoof · pad · paw. TRY foot IN A SENTENCE BELOW.",
            "htmlSnippet": "<b>Synonyms</b> for <b>foot</b>. hoof &middot; pad &middot; paw. TRY <b>foot</b> IN A SENTENCE BELOW.",
            "cacheId": "LMWV1-TDd-0J",
            "formattedUrl": "https://www.thesaurus.com/browse/foot",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>foot</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of foot | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for foot from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Foot Synonyms, Foot Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Foot Synonyms</b>, Foot Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/foot",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms & Antonyms of foot · acme, · apex, · climax, · crest, · culmination, · \nheight, · meridian, · peak, ...",
            "htmlSnippet": "<b>Synonyms</b> &amp; Antonyms of <b>foot</b> &middot; acme, &middot; apex, &middot; climax, &middot; crest, &middot; culmination, &middot; <br>\nheight, &middot; meridian, &middot; peak,&nbsp;...",
            "cacheId": "pjP7IJcZAikJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/foot",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>foot</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for FOOT",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/foot",
                        "og:title": "Thesaurus results for FOOT",
                        "twitter:aria-text": "Share more words for foot on Twitter",
                        "og:aria-text": "Post more words for foot to Facebook",
                        "og:description": "Foot: the lowest part, place, or point. Synonyms: base, basement, bottom… Antonyms: head, top, vertex… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Foot: the lowest part, place, or point. Synonyms: base, basement, bottom… Antonyms: head, top, vertex… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/foot"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Foot synonyms | Best 90 synonyms for foot",
            "htmlTitle": "<b>Foot synonyms</b> | Best 90 synonyms for foot",
            "link": "https://thesaurus.yourdictionary.com/foot",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 90 synonyms for foot, including: twelve inches, footing, cubic foot, \nfoundation, plantar, seat, substratum, foot it, front-foot, over, square-foot and more\n.",
            "htmlSnippet": "The best 90 <b>synonyms</b> for <b>foot</b>, including: twelve inches, footing, cubic <b>foot</b>, <br>\nfoundation, plantar, seat, substratum, <b>foot</b> it, front-<b>foot</b>, over, square-<b>foot</b> and more<br>\n.",
            "cacheId": "jBKEcSJSrSgJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/foot",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>foot</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "On foot Synonyms, On foot Antonyms | Thesaurus.com",
            "htmlTitle": "On <b>foot Synonyms</b>, On foot Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/on%20foot",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for on foot at Thesaurus.com with free online thesaurus, antonyms, \nand definitions. Find descriptive alternatives for on foot.",
            "htmlSnippet": "<b>Synonyms</b> for on <b>foot</b> at Thesaurus.com with free online thesaurus, antonyms, <br>\nand definitions. Find descriptive alternatives for on <b>foot</b>.",
            "cacheId": "saqeJs-F4x0J",
            "formattedUrl": "https://www.thesaurus.com/browse/on%20foot",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/on%20<b>foot</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of on foot | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for on foot from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Foot Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Foot Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/foot",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Additional synonyms · support, · stand, · foot, · rest, · bed, · bottom, · foundation, · \npedestal,.",
            "htmlSnippet": "Additional <b>synonyms</b> &middot; support, &middot; stand, &middot; <b>foot</b>, &middot; rest, &middot; bed, &middot; bottom, &middot; foundation, &middot; <br>\npedestal,.",
            "cacheId": "bLQwnAlhQVIJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/foot",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/<b>foot</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Foot Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "12e368a1-0c0b-4aa5-a2fc-90f3df5b9b5a",
                        "_csrf_parameter": "_csrf",
                        "title": "Foot Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for foot: tootsies | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/foot"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Travel on foot Synonyms, Travel on foot Antonyms | Thesaurus.com",
            "htmlTitle": "Travel on <b>foot Synonyms</b>, Travel on foot Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/travel%20on%20foot",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for travel on foot at Thesaurus.com with free online thesaurus, \nantonyms, and definitions. Find descriptive alternatives for travel on foot.",
            "htmlSnippet": "<b>Synonyms</b> for travel on <b>foot</b> at Thesaurus.com with free online thesaurus, <br>\nantonyms, and definitions. Find descriptive alternatives for travel on <b>foot</b>.",
            "cacheId": "Orbq9GqKGzgJ",
            "formattedUrl": "https://www.thesaurus.com/browse/travel%20on%20foot",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/travel%20on%20<b>foot</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of travel on foot | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for travel on foot from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for foot? | Foot Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for foot? | <b>Foot Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/foot.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 2060 synonyms for foot and other similar words that you can use instead \nbased on 17 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 2060 <b>synonyms</b> for <b>foot</b> and other similar words that you can use instead <br>\nbased on 17 separate contexts from our thesaurus.",
            "cacheId": "THe5IbKKwBMJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/foot.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>foot</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "2 329 Foot synonyms - Other Words for Foot",
            "htmlTitle": "2 329 <b>Foot synonyms</b> - Other Words for Foot",
            "link": "https://www.powerthesaurus.org/foot/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Foot synonyms. Top synonyms for foot (other words for foot) are base, feet and \nbottom.",
            "htmlSnippet": "<b>Foot synonyms</b>. Top synonyms for foot (other words for foot) are base, feet and <br>\nbottom.",
            "cacheId": "buCy2ON8w3gJ",
            "formattedUrl": "https://www.powerthesaurus.org/foot/synonyms",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>foot</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "2 329 Foot synonyms - Other Words for Foot",
                        "og:description": "Foot synonyms. Top synonyms for foot (other words for foot) are base, feet and bottom.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/foot/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for feet? | Feet Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for feet? | <b>Feet Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/feet.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 370 synonyms for feet and other similar words that you can use instead \nbased on 13 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 370 <b>synonyms</b> for <b>feet</b> and other similar words that you can use instead <br>\nbased on 13 separate contexts from our thesaurus.",
            "cacheId": "LUGEWtxuknIJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/feet.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>feet</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Foot Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Foot Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/foot",
            "displayLink": "www.synonyms.com",
            "snippet": "Sep 22, 2019 ... Princeton's WordNet(4.00 / 1 vote)Rate these synonyms: · foot, human foot, pes(\nnoun) the part of the leg of a human being below the ankle joint.",
            "htmlSnippet": "Sep 22, 2019 <b>...</b> Princeton&#39;s WordNet(4.00 / 1 vote)Rate these <b>synonyms</b>: &middot; <b>foot</b>, human <b>foot</b>, pes(<br>\nnoun) the part of the leg of a human being below the ankle joint.",
            "cacheId": "9lzc_ESMbxAJ",
            "formattedUrl": "https://www.synonyms.com/synonym/foot",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>foot</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/foot",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        }
    ]
}
{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - spell  synonyms",
                "totalResults": "137000000",
                "searchTerms": "spell  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - spell  synonyms",
                "totalResults": "137000000",
                "searchTerms": "spell  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.593008,
        "formattedSearchTime": "0.59",
        "totalResults": "137000000",
        "formattedTotalResults": "137,000,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Spell Synonyms, Spell Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Spell Synonyms</b>, Spell Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/spell",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for spell at Thesaurus.com with free online thesaurus, antonyms, and \ndefinitions. Find descriptive alternatives for spell.",
            "htmlSnippet": "<b>Synonyms</b> for <b>spell</b> at Thesaurus.com with free online thesaurus, antonyms, and <br>\ndefinitions. Find descriptive alternatives for <b>spell</b>.",
            "cacheId": "pKeGLv8Lk9sJ",
            "formattedUrl": "https://www.thesaurus.com/browse/spell",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>spell</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of spell | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for spell from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Spell Synonyms, Spell Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Spell Synonyms</b>, Spell Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/spell",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms & Antonyms of spell · bewitch, · charm, · enchant, · ensorcell · (or \nensorcel), · hex, · overlook, · strike.",
            "htmlSnippet": "<b>Synonyms</b> &amp; Antonyms of <b>spell</b> &middot; bewitch, &middot; charm, &middot; enchant, &middot; ensorcell &middot; (or <br>\nensorcel), &middot; hex, &middot; overlook, &middot; strike.",
            "cacheId": "3thQUuupyL0J",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/spell",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>spell</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for SPELL",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/spell",
                        "og:title": "Thesaurus results for SPELL",
                        "twitter:aria-text": "Share more words for spell on Twitter",
                        "og:aria-text": "Post more words for spell to Facebook",
                        "og:description": "Spell: to cast a spell on. Synonyms: bewitch, charm, enchant… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Spell: to cast a spell on. Synonyms: bewitch, charm, enchant… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/spell"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Magic spell Synonyms, Magic spell Antonyms | Thesaurus.com",
            "htmlTitle": "Magic <b>spell Synonyms</b>, Magic spell Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/magic%20spell",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for magic spell at Thesaurus.com with free online thesaurus, \nantonyms, and definitions. Find descriptive alternatives for magic spell.",
            "htmlSnippet": "<b>Synonyms</b> for magic <b>spell</b> at Thesaurus.com with free online thesaurus, <br>\nantonyms, and definitions. Find descriptive alternatives for magic <b>spell</b>.",
            "cacheId": "wfpFHTD4sWYJ",
            "formattedUrl": "https://www.thesaurus.com/browse/magic%20spell",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/magic%20<b>spell</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of magic spell | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for magic spell from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms for MAGIC SPELL - Thesaurus.net",
            "htmlTitle": "<b>Synonyms</b> for MAGIC <b>SPELL</b> - Thesaurus.net",
            "link": "https://www.thesaurus.net/magic%20spell",
            "displayLink": "www.thesaurus.net",
            "snippet": "Synonyms for MAGIC SPELL: mumbo jumbo, Cantrip, evil eye, glamour, weird, \nwanga, curse, exorcism, malocchio, magic spell, charm, magical spell, spell, ...",
            "htmlSnippet": "<b>Synonyms</b> for MAGIC <b>SPELL</b>: mumbo jumbo, Cantrip, evil eye, glamour, weird, <br>\nwanga, curse, exorcism, malocchio, magic <b>spell</b>, charm, magical <b>spell</b>, <b>spell</b>,&nbsp;...",
            "cacheId": "6kANithHtoIJ",
            "formattedUrl": "https://www.thesaurus.net/magic%20spell",
            "htmlFormattedUrl": "https://www.thesaurus.net/magic%20<b>spell</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTHmPbL6L3tWwemgnxXK_eIzcEGEcFI39SJD35kF79sVRInWi-xNtX9sw",
                        "width": "312",
                        "height": "53"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.net/public/desktop/images/logo.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms for MAGIC SPELL - Thesaurus.net",
                        "og:site_name": "Thesaurus.net",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "magic spell | synonyms: mumbo jumbo, Cantrip, evil eye, glamour, weird, wanga, curse, exorcism, malocchio, magic spell, charm, magical spell, spell, conjuration, conjurations, bewitchment, Hocuspocus, bewitchments, whammy, hexing, chant, mumbo-jumbo, magic words, jinx, hocus-pocus, abracadabra, magic charm, incantation, mumbo jumbo",
                        "og:title": "Synonyms for MAGIC SPELL - Thesaurus.net",
                        "og:url": "https://www.thesaurus.net/magic%20spell",
                        "og:description": "magic spell | synonyms: mumbo jumbo, Cantrip, evil eye, glamour, weird, wanga, curse, exorcism, malocchio, magic spell, charm, magical spell, spell, conjuration, conjurations, bewitchment, Hocuspocus, bewitchments, whammy, hexing, chant, mumbo-jumbo, magic words, jinx, hocus-pocus, abracadabra, magic charm, incantation, mumbo jumbo"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.net/public/desktop/images/logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for spell? | Spell Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for spell? | <b>Spell Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/spell.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 8192 synonyms for spell and other similar words that you can use instead \nbased on 31 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 8192 <b>synonyms</b> for <b>spell</b> and other similar words that you can use instead <br>\nbased on 31 separate contexts from our thesaurus.",
            "cacheId": "LFxcP74eADIJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/spell.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>spell</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Spell synonyms | Best 84 synonyms for spell",
            "htmlTitle": "<b>Spell synonyms</b> | Best 84 synonyms for spell",
            "link": "https://thesaurus.yourdictionary.com/spell",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 84 synonyms for spell, including: talisman, interval, chanting, magic, \ndecipher, amulet, term, stroke, spasm, turn, charm and more... Find another word\n ...",
            "htmlSnippet": "The best 84 <b>synonyms</b> for <b>spell</b>, including: talisman, interval, chanting, magic, <br>\ndecipher, amulet, term, stroke, spasm, turn, charm and more... Find another word<br>\n&nbsp;...",
            "cacheId": "dbMoiQCkMc0J",
            "formattedUrl": "https://thesaurus.yourdictionary.com/spell",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>spell</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Magic spell Synonyms, Magic spell Antonyms | Thesaurus.com",
            "htmlTitle": "Magic <b>spell Synonyms</b>, Magic spell Antonyms | Thesaurus.com",
            "link": "http://www.thesaurus.com/browse/magic-spell?jss=0",
            "displayLink": "www.thesaurus.com",
            "snippet": "SWAP magic spell IN A SENTENCE. Join our early testers! See how your \nsentence looks with different synonyms. TRY NOW. Characters: 0/140 ...",
            "htmlSnippet": "SWAP magic <b>spell</b> IN A SENTENCE. Join our early testers! See how your <br>\nsentence looks with different <b>synonyms</b>. TRY NOW. Characters: 0/140&nbsp;...",
            "cacheId": "CpAkPJGUfoQJ",
            "formattedUrl": "www.thesaurus.com/browse/magic-spell?jss=0",
            "htmlFormattedUrl": "www.thesaurus.com/browse/magic-<b>spell</b>?jss=0",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "og:title": "Synonyms of magic spell | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for magic spell from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Spell Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Spell Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/spell",
            "displayLink": "www.synonyms.com",
            "snippet": "Find all the synonyms and alternative words for spell at Synonyms.com, the \nlargest free online thesaurus, antonyms, definitions and translations resource on \nthe ...",
            "htmlSnippet": "Find all the <b>synonyms</b> and alternative words for <b>spell</b> at <b>Synonyms</b>.com, the <br>\nlargest free online thesaurus, antonyms, definitions and translations resource on <br>\nthe&nbsp;...",
            "cacheId": "sUhKc6K5DiQJ",
            "formattedUrl": "https://www.synonyms.com/synonym/spell",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>spell</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/spell",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "86 Magic Spell synonyms - Other Words for Magic Spell",
            "htmlTitle": "86 Magic <b>Spell synonyms</b> - Other Words for Magic Spell",
            "link": "https://www.powerthesaurus.org/magic_spell/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Magic Spell synonyms. Top synonyms for magic spell (other words for magic \nspell) are charm, spell and incantation.",
            "htmlSnippet": "Magic <b>Spell synonyms</b>. Top synonyms for magic spell (other words for magic <br>\nspell) are charm, spell and incantation.",
            "cacheId": "oUQwqqma4b4J",
            "formattedUrl": "https://www.powerthesaurus.org/magic_spell/synonyms",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/magic_<b>spell</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "86 Magic Spell synonyms - Other Words for Magic Spell",
                        "og:description": "Magic Spell synonyms. Top synonyms for magic spell (other words for magic spell) are charm, spell and incantation.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/magic_spell/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Spell Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Spell Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/spell",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "a word used in magic spells, which is supposed to possess magic powers. \nJustice here represents nothing more than magical abracadabra. Synonyms. \nspell,.",
            "htmlSnippet": "a word used in magic <b>spells</b>, which is supposed to possess magic powers. <br>\nJustice here represents nothing more than magical abracadabra. <b>Synonyms</b>. <br>\n<b>spell</b>,.",
            "cacheId": "Ezv7vQ7jdo0J",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/spell",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/<b>spell</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Spell Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "2c1b18fd-9c81-43d3-99b9-8193371b561b",
                        "_csrf_parameter": "_csrf",
                        "title": "Spell Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for spell: indicate, mean, signify, suggest, promise | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/spell"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        }
    ]
}
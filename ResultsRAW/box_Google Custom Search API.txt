{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - box  synonyms",
                "totalResults": "173000000",
                "searchTerms": "box  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - box  synonyms",
                "totalResults": "173000000",
                "searchTerms": "box  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.249993,
        "formattedSearchTime": "0.25",
        "totalResults": "173000000",
        "formattedTotalResults": "173,000,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Box Synonyms, Box Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Box Synonyms</b>, Box Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/box",
            "displayLink": "www.thesaurus.com",
            "snippet": "RELATED WORDS AND SYNONYMS FOR BOX. TV. nounvisual and audio \nentertainment transmitted via radio waves. TV set ...",
            "htmlSnippet": "RELATED WORDS AND <b>SYNONYMS</b> FOR <b>BOX</b>. TV. nounvisual and audio <br>\nentertainment transmitted via radio waves. TV set&nbsp;...",
            "cacheId": "71ODsChUoDoJ",
            "formattedUrl": "https://www.thesaurus.com/browse/box",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>box</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of box | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for box from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Box Synonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Box Synonyms</b> | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/box",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms of box. (Entry 1 of 3). 1 a covered rectangular container for storing or \ntransporting things.",
            "htmlSnippet": "<b>Synonyms</b> of <b>box</b>. (Entry 1 of 3). 1 a covered rectangular container for storing or <br>\ntransporting things.",
            "cacheId": "nkjmkWxM7OMJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/box",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>box</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for BOX",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/box",
                        "og:title": "Thesaurus results for BOX",
                        "twitter:aria-text": "Share more words for box on Twitter",
                        "og:aria-text": "Post more words for box to Facebook",
                        "og:description": "Box: a covered rectangular container for storing or transporting things. Synonyms: bin, caddy, case… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Box: a covered rectangular container for storing or transporting things. Synonyms: bin, caddy, case… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/box"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for box? | Box Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for box? | <b>Box Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/box.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 4686 synonyms for box and other similar words that you can use instead \nbased on 38 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 4686 <b>synonyms</b> for <b>box</b> and other similar words that you can use instead <br>\nbased on 38 separate contexts from our thesaurus.",
            "cacheId": "gOxwwqc7w78J",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/box.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>box</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Boxes Synonyms, Boxes Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Boxes Synonyms</b>, Boxes Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/boxes",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for · carton · crate · pack · package · trunk · bin · case · casket.",
            "htmlSnippet": "<b>Synonyms</b> for &middot; carton &middot; crate &middot; pack &middot; package &middot; trunk &middot; bin &middot; case &middot; casket.",
            "cacheId": "mHu3DeujxjAJ",
            "formattedUrl": "https://www.thesaurus.com/browse/boxes",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>boxes</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of boxes | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for boxes from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Box synonyms | Best 77 synonyms for box",
            "htmlTitle": "<b>Box synonyms</b> | Best 77 synonyms for box",
            "link": "https://thesaurus.yourdictionary.com/box",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 77 synonyms for box, including: spot, pack, carton, crate, case, confine, \npackage, spar, punch, slug, hit and more... Find another word for box at ...",
            "htmlSnippet": "The best 77 <b>synonyms</b> for <b>box</b>, including: spot, pack, carton, crate, case, confine, <br>\npackage, spar, punch, slug, hit and more... Find another word for <b>box</b> at&nbsp;...",
            "cacheId": "Fp1hPsy2Fa4J",
            "formattedUrl": "https://thesaurus.yourdictionary.com/box",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>box</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Letter-box Synonyms, Letter-box Antonyms | Thesaurus.com",
            "htmlTitle": "Letter-<b>box Synonyms</b>, Letter-box Antonyms | Thesaurus.com",
            "link": "http://www.thesaurus.com/browse/letter-box",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for letter-box at Thesaurus.com with free online thesaurus, antonyms, \nand definitions. Find descriptive alternatives for letter-box.",
            "htmlSnippet": "<b>Synonyms</b> for letter-<b>box</b> at Thesaurus.com with free online thesaurus, antonyms, <br>\nand definitions. Find descriptive alternatives for letter-<b>box</b>.",
            "cacheId": "iBbcOlGmo-IJ",
            "formattedUrl": "www.thesaurus.com/browse/letter-box",
            "htmlFormattedUrl": "www.thesaurus.com/browse/letter-<b>box</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQAOaO5s-St1tGH1aL-JCQvYOW_nTf9XmOHvDuNbakvLI5Ya8K_ZhifgP2k",
                        "width": "268",
                        "height": "188"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-208ba0c138c78ab3718720567519407e.png",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "og:title": "I found great synonyms for \"letter-box\" on the new Thesaurus.com!",
                        "fb:admins": "100000304287730,109125464873"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://images.ctfassets.net/55tpbg0qcsp4/4Dv5jfCu1G0oA0K2mw62Ci/f990173542acb54b8af9156f20b3f959/weather_words_1000x700.jpg?w=284&h=240"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "2 026 Box synonyms - Other Words for Box",
            "htmlTitle": "2 026 <b>Box synonyms</b> - Other Words for Box",
            "link": "https://www.powerthesaurus.org/box/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Box synonyms. Top synonyms for box (other words for box) are case, crate and \ncarton.",
            "htmlSnippet": "<b>Box synonyms</b>. Top synonyms for box (other words for box) are case, crate and <br>\ncarton.",
            "cacheId": "xQ07WXyOD9wJ",
            "formattedUrl": "https://www.powerthesaurus.org/box/synonyms",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>box</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "2 026 Box synonyms - Other Words for Box",
                        "og:description": "Box synonyms. Top synonyms for box (other words for box) are case, crate and carton.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/box/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Post-box Synonyms, Post-box Antonyms | Thesaurus.com",
            "htmlTitle": "Post-<b>box Synonyms</b>, Post-box Antonyms | Thesaurus.com",
            "link": "http://thesaurus.com/browse/POST-BOX",
            "displayLink": "thesaurus.com",
            "snippet": "Synonyms for post-box at Thesaurus.com with free online thesaurus, antonyms, \nand definitions. Find descriptive alternatives for post-box.",
            "htmlSnippet": "<b>Synonyms</b> for post-<b>box</b> at Thesaurus.com with free online thesaurus, antonyms, <br>\nand definitions. Find descriptive alternatives for post-<b>box</b>.",
            "cacheId": "5gp6xqF8DGEJ",
            "formattedUrl": "thesaurus.com/browse/POST-BOX",
            "htmlFormattedUrl": "thesaurus.com/browse/POST-<b>BOX</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRw4pYZELIty_bfkAIaLoU9QD7CV-AkbPATHvgXPYxGkDgNqmW6nvPLXw",
                        "width": "64",
                        "height": "64"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "http://www.thesaurus.com/assets/thesaurus_social_logo-208ba0c138c78ab3718720567519407e.png",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "og:title": "I found great synonyms for \"post-box\" on the new Thesaurus.com!",
                        "fb:admins": "100000304287730,109125464873"
                    }
                ],
                "cse_image": [
                    {
                        "src": "http://www.thesaurus.com/assets/thesaurus_social_logo-208ba0c138c78ab3718720567519407e.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Box | Synonyms of Box by Lexico",
            "htmlTitle": "<b>Box</b> | <b>Synonyms</b> of <b>Box</b> by Lexico",
            "link": "https://www.lexico.com/synonym/box",
            "displayLink": "www.lexico.com",
            "snippet": "What is the definition of box? What is the meaning of box? How do you use box in \na sentence? What are synonyms for box?",
            "htmlSnippet": "What is the definition of <b>box</b>? What is the meaning of <b>box</b>? How do you use <b>box</b> in <br>\na sentence? What are <b>synonyms</b> for <b>box</b>?",
            "cacheId": "01A8SNc3GgAJ",
            "formattedUrl": "https://www.lexico.com/synonym/box",
            "htmlFormattedUrl": "https://www.lexico.com/<b>synonym</b>/<b>box</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT-fHqiHsELFzBLFkvhF3MUNoGyDc02pwOeICq9JD-vhUo5TV_0a-_rYOwh",
                        "width": "291",
                        "height": "173"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#50b46c",
                        "og:image": "https://www.lexico.com/lexico-logo.png",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Box | Synonyms of Box by Lexico",
                        "og:type": "website",
                        "theme-color": "#50b46c",
                        "og:site_name": "Lexico Dictionaries | English",
                        "og:title": "Box | Synonyms of Box by Lexico",
                        "msapplication-tileimage": "/mstile-144x144.png",
                        "csrf-param": "authenticity_token",
                        "og:description": "What is the definition of box? What is the meaning of box? How do you use box in a sentence? What are synonyms for box?",
                        "twitter:image": "https://www.lexico.com/lexico-logo.png",
                        "viewport": "width=device-width, initial-scale=1, shrink-to-fit=no",
                        "twitter:description": "What is the definition of box? What is the meaning of box? How do you use box in a sentence? What are synonyms for box?",
                        "csrf-token": "5a2Y5vhrXtUXjVBtjg75lBSZAr3EiQi9sEtqMWSa08srE9SWwzXPne85WdyMIZOI5cXVMzZoNbBoDgMMqlRTnA==",
                        "og:url": "https://www.lexico.com/synonym/box"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.lexico.com/lexico-logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Box Synonyms | Collins English Thesaurus",
            "htmlTitle": "<b>Box Synonyms</b> | Collins English Thesaurus",
            "link": "https://www.collinsdictionary.com/dictionary/english-thesaurus/box",
            "displayLink": "www.collinsdictionary.com",
            "snippet": "Another word for box: container, case, chest, trunk, pack | Collins English \nThesaurus.",
            "htmlSnippet": "Another word for <b>box</b>: container, case, chest, trunk, pack | Collins English <br>\nThesaurus.",
            "cacheId": "BFN_Q4qukFEJ",
            "formattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/box",
            "htmlFormattedUrl": "https://www.collinsdictionary.com/dictionary/english-thesaurus/<b>box</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRZe-ipygcH2xseTVhDiBOGt2-N_GzEKZ5vSaz2cju39ueHlHi9s6P1FYWo",
                        "width": "311",
                        "height": "162"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png",
                        "msapplication-config": "https://www.collinsdictionary.com/browserconfig.xml",
                        "og:type": "website",
                        "theme-color": "#db0100",
                        "og:title": "Box Synonyms | Collins English Thesaurus",
                        "apple-mobile-web-app-title": "Collins Dictionaries",
                        "_csrf": "d0501a51-a46e-42e9-a6b8-fe6b5f79eb36",
                        "_csrf_parameter": "_csrf",
                        "title": "Box Synonyms | Collins English Thesaurus",
                        "og:description": "Another word for box: container, case, chest, trunk, pack | Collins English Thesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=5.0",
                        "_csrf_header": "X-XSRF-TOKEN",
                        "og:url": "https://www.collinsdictionary.com/dictionary/english-thesaurus/box"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.collinsdictionary.com/external/images/social/logo-THESAURUS.png"
                    }
                ]
            }
        }
    ]
}
{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - sock  synonyms",
                "totalResults": "2660000",
                "searchTerms": "sock  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - sock  synonyms",
                "totalResults": "2660000",
                "searchTerms": "sock  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.292285,
        "formattedSearchTime": "0.29",
        "totalResults": "2660000",
        "formattedTotalResults": "2,660,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Sock Synonyms, Sock Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Sock Synonyms</b>, Sock Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/sock",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for sock at Thesaurus.com with free online thesaurus, antonyms, and \ndefinitions. Find descriptive alternatives for sock.",
            "htmlSnippet": "<b>Synonyms</b> for <b>sock</b> at Thesaurus.com with free online thesaurus, antonyms, and <br>\ndefinitions. Find descriptive alternatives for <b>sock</b>.",
            "cacheId": "I1mvmuElvSgJ",
            "formattedUrl": "https://www.thesaurus.com/browse/sock",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>sock</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of sock | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for sock from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Sock Synonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Sock Synonyms</b> | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/sock",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms of sock · bang, · bash, · bat, · beat, · belt, · biff, · blow, · bop, ...",
            "htmlSnippet": "<b>Synonyms</b> of <b>sock</b> &middot; bang, &middot; bash, &middot; bat, &middot; beat, &middot; belt, &middot; biff, &middot; blow, &middot; bop,&nbsp;...",
            "cacheId": "bkWmeLTITDcJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/sock",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>sock</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for SOCK",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/sock",
                        "og:title": "Thesaurus results for SOCK",
                        "twitter:aria-text": "Share more words for sock on Twitter",
                        "og:aria-text": "Post more words for sock to Facebook",
                        "og:description": "Sock: a close-fitting covering for the foot and leg. Synonyms: hose, stocking… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Sock: a close-fitting covering for the foot and leg. Synonyms: hose, stocking… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/sock"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Synonyms and Antonyms for sock | Synonym.com",
            "htmlTitle": "<b>Synonyms</b> and Antonyms for <b>sock</b> | <b>Synonym</b>.com",
            "link": "https://www.synonym.com/synonyms/sock",
            "displayLink": "www.synonym.com",
            "snippet": "Synonym.com is the web's best resource for English synonyms, antonyms, and \ndefinitions.",
            "htmlSnippet": "<b>Synonym</b>.com is the web&#39;s best resource for English <b>synonyms</b>, antonyms, and <br>\ndefinitions.",
            "cacheId": "Laimb78ak5kJ",
            "formattedUrl": "https://www.synonym.com/synonyms/sock",
            "htmlFormattedUrl": "https://www.<b>synonym</b>.com/<b>synonyms</b>/<b>sock</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2kfLDDnPtioWhkwcA4k79uVoBwZGbjMImaZa6YUkJlQd0xTZGjOwZ2Yc",
                        "width": "228",
                        "height": "221"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#FFFFFF",
                        "og:image": "/static/images/icons/synonym/synonym-icon-fullsize.png",
                        "og:type": "article",
                        "twitter:title": "Synonyms and Antonyms for sock",
                        "theme-color": "#d8f1fb",
                        "og:title": "Synonyms and Antonyms for sock",
                        "apple-mobile-web-app-title": "Synonym",
                        "msapplication-tileimage": "/static/images/icons/synonym/favicon144.png",
                        "og:description": "sock | definition: hosiery consisting of a cloth covering for the foot; worn inside the shoe; reaches to between the ankle and the knee | synonyms: hose, knee-hi, athletic sock, argyll, sweat sock, argyle, varsity sock, bobbysock, hosiery, knee-high, tabi, anklets, bobbysocks, anklet, tabis| antonyms: fall back, bottom out, stay in place, top out, miss",
                        "apple-mobile-web-app-status-bar-style": "default",
                        "viewport": "width=device-width",
                        "twitter:description": "sock | definition: hosiery consisting of a cloth covering for the foot; worn inside the shoe; reaches to between the ankle and the knee | synonyms: hose, knee-hi, athletic sock, argyll, sweat sock, argyle, varsity sock, bobbysock, hosiery, knee-high, tabi, anklets, bobbysocks, anklet, tabis| antonyms: fall back, bottom out, stay in place, top out, miss",
                        "apple-mobile-web-app-capable": "no",
                        "og:url": "https://www.synonym.com/synonyms/sock"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonym.com/static/images/wordtigo-background.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Sock synonyms | Best 53 synonyms for sock",
            "htmlTitle": "<b>Sock synonyms</b> | Best 53 synonyms for sock",
            "link": "https://thesaurus.yourdictionary.com/sock",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 53 synonyms for sock, including: stocking, hose, ankle-length stocking, \nstrike, beat, clothes, hosiery, hit, belt, blow, bop and more... Find another word ...",
            "htmlSnippet": "The best 53 <b>synonyms</b> for <b>sock</b>, including: stocking, hose, ankle-length stocking, <br>\nstrike, beat, clothes, hosiery, hit, belt, blow, bop and more... Find another word&nbsp;...",
            "cacheId": "S-aSe9AwZ_QJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/sock",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>sock</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Stockings, tights & socks - SMART thesaurus cloud with synonyms ...",
            "htmlTitle": "Stockings, tights &amp; <b>socks</b> - SMART thesaurus cloud with <b>synonyms</b> ...",
            "link": "https://dictionary.cambridge.org/us/topics/clothes/stockings-tights-and-socks/",
            "displayLink": "dictionary.cambridge.org",
            "snippet": "Stockings, tights & socks - Synonyms, antonyms, and related words and phrases | \nCambridge American English Thesaurus.",
            "htmlSnippet": "Stockings, tights &amp; <b>socks</b> - <b>Synonyms</b>, antonyms, and related words and phrases | <br>\nCambridge American English Thesaurus.",
            "cacheId": "f4vCRgIR4fIJ",
            "formattedUrl": "https://dictionary.cambridge.org/us/topics/.../stockings-tights-and-socks/",
            "htmlFormattedUrl": "https://dictionary.cambridge.org/us/topics/.../stockings-tights-and-<b>socks</b>/",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGQaydQlICvLXfpsh02m74fIxx4nlhCf0DwYXf_LPlQLllVbcJkAUm5ns",
                        "width": "240",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/us/external/images/CDO_logo_120x120.jpg?version=5.0.107",
                        "viewport": "width=device-width,minimum-scale=1,initial-scale=1",
                        "og:title": "Stockings, tights & socks - SMART thesaurus cloud with synonyms and related words",
                        "og:url": "https://dictionary.cambridge.org/us/topics/clothes/stockings-tights-and-socks/",
                        "og:description": "Stockings, tights & socks - Synonyms, antonyms, and related words and phrases | Cambridge American English Thesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://dictionary.cambridge.org/us/external/images/quiz/hook/arts-and-crafts_2.jpg?version=5.0.107"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "253 Socks synonyms - Other Words for Socks",
            "htmlTitle": "253 <b>Socks synonyms</b> - Other Words for Socks",
            "link": "https://www.powerthesaurus.org/socks/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Socks synonyms. Top synonyms for socks (other words for socks) are stockings, \npanty hose and tights.",
            "htmlSnippet": "<b>Socks synonyms</b>. Top synonyms for socks (other words for socks) are stockings, <br>\npanty hose and tights.",
            "cacheId": "Q7sIDwhiM3AJ",
            "formattedUrl": "https://www.powerthesaurus.org/socks/synonyms",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>socks</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "253 Socks synonyms - Other Words for Socks",
                        "og:description": "Socks synonyms. Top synonyms for socks (other words for socks) are stockings, panty hose and tights.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/socks/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Knee sock Synonyms, Knee sock Antonyms | Thesaurus.com",
            "htmlTitle": "Knee <b>sock Synonyms</b>, Knee sock Antonyms | Thesaurus.com",
            "link": "http://www.thesaurus.com/browse/knee-sock?qsrc=2446",
            "displayLink": "www.thesaurus.com",
            "snippet": "Synonyms for knee sock at Thesaurus.com with free online thesaurus, antonyms, \nand definitions. Find descriptive alternatives for knee sock.",
            "htmlSnippet": "<b>Synonyms</b> for knee <b>sock</b> at Thesaurus.com with free online thesaurus, antonyms, <br>\nand definitions. Find descriptive alternatives for knee <b>sock</b>.",
            "cacheId": "sxrzbieYwmoJ",
            "formattedUrl": "www.thesaurus.com/browse/knee-sock?qsrc=2446",
            "htmlFormattedUrl": "www.thesaurus.com/browse/knee-<b>sock</b>?qsrc=2446",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "og:title": "Synonyms of knee sock | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for knee sock from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for socks? | Socks Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for socks? | <b>Socks Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/socks.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 1393 synonyms for socks and other similar words that you can use instead \nbased on 11 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 1393 <b>synonyms</b> for <b>socks</b> and other similar words that you can use instead <br>\nbased on 11 separate contexts from our thesaurus.",
            "cacheId": "JDJ0sTPF0wAJ",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/socks.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>socks</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "socks and stockings - synonyms and related words | Macmillan ...",
            "htmlTitle": "<b>socks</b> and stockings - <b>synonyms</b> and related words | Macmillan ...",
            "link": "https://www.macmillandictionary.com/us/thesaurus-category/american/socks-and-stockings",
            "displayLink": "www.macmillandictionary.com",
            "snippet": "Comprehensive list of synonyms for socks and stockings, by Macmillan \nDictionary and Thesaurus.",
            "htmlSnippet": "Comprehensive list of <b>synonyms</b> for <b>socks</b> and stockings, by Macmillan <br>\nDictionary and Thesaurus.",
            "cacheId": "YiKrVizvNp0J",
            "formattedUrl": "https://www.macmillandictionary.com/us/thesaurus.../socks-and-stockings",
            "htmlFormattedUrl": "https://www.macmillandictionary.com/us/thesaurus.../<b>socks</b>-and-stockings",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSrV7eUfPK6uhcd_X-qcyTUQn8cfdC9uEXrIkzfbhFL57UWFmjsxQF2khc",
                        "width": "310",
                        "height": "163"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/external/images/social/social_macmillan.jpg?version=4.0.27",
                        "og:type": "website",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=10.0, minimum-scale=0.1",
                        "og:title": "socks and stockings - synonyms and related words | Macmillan Dictionary",
                        "og:url": "https://www.macmillandictionary.com/thesaurus-category/british/socks-and-stockings",
                        "og:description": "Comprehensive list of synonyms for socks and stockings, by Macmillan Dictionary and Thesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillandictionary.com/external/images/social/social_macmillan.jpg?version=4.0.27"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Antonym of sock",
            "htmlTitle": "Antonym of <b>sock</b>",
            "link": "https://www.synonyms.com/antonyms/sock",
            "displayLink": "www.synonyms.com",
            "snippet": "Antonyms for sock at Synonyms.com with free online thesaurus, synonyms, \ndefinitions and translations.",
            "htmlSnippet": "Antonyms for <b>sock</b> at <b>Synonyms</b>.com with free online thesaurus, <b>synonyms</b>, <br>\ndefinitions and translations.",
            "cacheId": "BB1HhCarRQoJ",
            "formattedUrl": "https://www.synonyms.com/antonyms/sock",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/antonyms/<b>sock</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/antonyms/sock",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        }
    ]
}
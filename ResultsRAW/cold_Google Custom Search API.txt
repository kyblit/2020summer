{
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - cold  synonyms",
                "totalResults": "63000000",
                "searchTerms": "cold  synonyms",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - cold  synonyms",
                "totalResults": "63000000",
                "searchTerms": "cold  synonyms",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "003330174904582784281:ycdnjzifiqk"
            }
        ]
    },
    "context": {
        "title": "Kyblit2020DataSE"
    },
    "searchInformation": {
        "searchTime": 0.263723,
        "formattedSearchTime": "0.26",
        "totalResults": "63000000",
        "formattedTotalResults": "63,000,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Cold Synonyms, Cold Antonyms | Thesaurus.com",
            "htmlTitle": "<b>Cold Synonyms</b>, Cold Antonyms | Thesaurus.com",
            "link": "https://www.thesaurus.com/browse/cold",
            "displayLink": "www.thesaurus.com",
            "snippet": "RELATED WORDS AND SYNONYMS FOR COLD. aloof. adjectiveremote. above \n· apart · casual ...",
            "htmlSnippet": "RELATED WORDS AND <b>SYNONYMS</b> FOR <b>COLD</b>. aloof. adjectiveremote. above <br>\n&middot; apart &middot; casual&nbsp;...",
            "cacheId": "TIc7cq2PCWkJ",
            "formattedUrl": "https://www.thesaurus.com/browse/cold",
            "htmlFormattedUrl": "https://www.thesaurus.com/browse/<b>cold</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "og:title": "Synonyms of cold | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for cold from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Cold Synonyms, Cold Antonyms | Merriam-Webster Thesaurus",
            "htmlTitle": "<b>Cold Synonyms</b>, Cold Antonyms | Merriam-Webster Thesaurus",
            "link": "https://www.merriam-webster.com/thesaurus/cold",
            "displayLink": "www.merriam-webster.com",
            "snippet": "Synonyms & Antonyms of cold · Synonyms for cold antiseptic, arctic, brittle, chill, \nchilly, clammy, · Words Related to cold bloodless, coldhearted, hard-hearted, ...",
            "htmlSnippet": "<b>Synonyms</b> &amp; Antonyms of <b>cold</b> &middot; <b>Synonyms</b> for <b>cold</b> antiseptic, arctic, brittle, chill, <br>\nchilly, clammy, &middot; Words Related to <b>cold</b> bloodless, coldhearted, hard-hearted,&nbsp;...",
            "cacheId": "NLEHyNR2emUJ",
            "formattedUrl": "https://www.merriam-webster.com/thesaurus/cold",
            "htmlFormattedUrl": "https://www.merriam-webster.com/thesaurus/<b>cold</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSKksTSKYwpW8It403nrjw5t1_a8pLO2PI6ImEG7uvFrNfacgiziPZgG-_O",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#2b5797",
                        "og:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "twitter:title": "Thesaurus results for COLD",
                        "twitter:card": "summary",
                        "theme-color": "#ffffff",
                        "twitter:url": "https://www.merriam-webster.com/thesaurus/cold",
                        "og:title": "Thesaurus results for COLD",
                        "twitter:aria-text": "Share more words for cold on Twitter",
                        "og:aria-text": "Post more words for cold to Facebook",
                        "og:description": "Cold: having a low or subnormal temperature. Synonyms: algid, arctic, bitter… Antonyms: ardent, blazing, boiling… Find the right word.",
                        "twitter:image": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png",
                        "referrer": "unsafe-url",
                        "fb:app_id": "178450008855735",
                        "twitter:site": "@MerriamWebster",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Cold: having a low or subnormal temperature. Synonyms: algid, arctic, bitter… Antonyms: ardent, blazing, boiling… Find the right word.",
                        "og:url": "https://www.merriam-webster.com/thesaurus/cold"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Cold synonyms | Best 179 synonyms for cold",
            "htmlTitle": "<b>Cold synonyms</b> | Best 179 synonyms for cold",
            "link": "https://thesaurus.yourdictionary.com/cold",
            "displayLink": "thesaurus.yourdictionary.com",
            "snippet": "The best 179 synonyms for cold, including: freezing, icy, chilly, frosty, frozen, crisp\n, frigid, below zero, wintry, snowy, penetrating and more... Find another word ...",
            "htmlSnippet": "The best 179 <b>synonyms</b> for <b>cold</b>, including: freezing, icy, chilly, frosty, frozen, crisp<br>\n, frigid, below zero, wintry, snowy, penetrating and more... Find another word&nbsp;...",
            "cacheId": "dMDtTpALc5YJ",
            "formattedUrl": "https://thesaurus.yourdictionary.com/cold",
            "htmlFormattedUrl": "https://thesaurus.yourdictionary.com/<b>cold</b>",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Make one's blood run cold Synonyms, Make one's blood run cold ...",
            "htmlTitle": "Make one&#39;s blood run <b>cold Synonyms</b>, Make one&#39;s blood run cold ...",
            "link": "http://thesaurus.com/browse/make+ones+blood+run+cold",
            "displayLink": "thesaurus.com",
            "snippet": "Synonyms for make one's blood run cold at Thesaurus.com with free online \nthesaurus, antonyms, and definitions. Find descriptive alternatives for make one's\n ...",
            "htmlSnippet": "<b>Synonyms</b> for make one&#39;s blood run <b>cold</b> at Thesaurus.com with free online <br>\nthesaurus, antonyms, and definitions. Find descriptive alternatives for make one&#39;s<br>\n&nbsp;...",
            "cacheId": "mVsmWetFSlQJ",
            "formattedUrl": "thesaurus.com/browse/make+ones+blood+run+cold",
            "htmlFormattedUrl": "thesaurus.com/browse/make+ones+blood+run+<b>cold</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTJxpppdTGPIh5cfR5ejYWhEGQm6eZQG02OxILJQqoiwyGItTKpABzE5qsK",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png",
                        "twitter:card": "summary",
                        "fb:app_id": "118269238218175",
                        "og:site_name": "www.thesaurus.com",
                        "twitter:site": "@Dictionarycom",
                        "msvalidate.01": "DF5542D7723770377E9ABFF59AC1DC97",
                        "og:title": "Synonyms of make one's blood run cold | Thesaurus.com",
                        "fb:admins": "100000304287730,109125464873",
                        "og:description": "Synonyms for make one's blood run cold from Thesaurus.com, the world’s leading online source for synonyms, antonyms, and more."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.thesaurus.com/assets/thesaurus_social_logo-4b42f0643b92eaf85fc0e4e78aa84a8d.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "What is another word for cold? | Cold Synonyms - WordHippo ...",
            "htmlTitle": "What is another word for cold? | <b>Cold Synonyms</b> - WordHippo ...",
            "link": "https://www.wordhippo.com/what-is/another-word-for/cold.html",
            "displayLink": "www.wordhippo.com",
            "snippet": "Find 7357 synonyms for cold and other similar words that you can use instead \nbased on 29 separate contexts from our thesaurus.",
            "htmlSnippet": "Find 7357 <b>synonyms</b> for <b>cold</b> and other similar words that you can use instead <br>\nbased on 29 separate contexts from our thesaurus.",
            "cacheId": "ib4Wi0Ju9x0J",
            "formattedUrl": "https://www.wordhippo.com/what-is/another-word-for/cold.html",
            "htmlFormattedUrl": "https://www.wordhippo.com/what-is/another-word-for/<b>cold</b>.html",
            "pagemap": {
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1.0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "3 657 Cold synonyms - Other Words for Cold",
            "htmlTitle": "3 657 <b>Cold synonyms</b> - Other Words for Cold",
            "link": "https://www.powerthesaurus.org/cold/synonyms",
            "displayLink": "www.powerthesaurus.org",
            "snippet": "Cold synonyms. Top synonyms for cold (other words for cold) are cool, frigid and \nchilly.",
            "htmlSnippet": "<b>Cold synonyms</b>. Top synonyms for cold (other words for cold) are cool, frigid and <br>\nchilly.",
            "cacheId": "rVx18rNwzeMJ",
            "formattedUrl": "https://www.powerthesaurus.org/cold/synonyms",
            "htmlFormattedUrl": "https://www.powerthesaurus.org/<b>cold</b>/<b>synonyms</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTVx9p8yv8Z4HECv2SAtxCx1RRwCs4TQ3i0VROrtknLF_sYU9IFYej4uRc",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "application-name": "Power Thesaurus",
                        "msapplication-config": "/browserconfig.xml",
                        "apple-itunes-app": "app-id=967211375",
                        "og:image": "https://www.radyushin.com/images/pt_logo_1200x1200.png",
                        "theme-color": "#168AE9",
                        "og:type": "website",
                        "og:image:alt": "Power Thesaurus",
                        "twitter:card": "summary",
                        "og:site_name": "Power Thesaurus",
                        "author": "Power Thesaurus",
                        "apple-mobile-web-app-title": "Power Thesaurus",
                        "og:title": "3 657 Cold synonyms - Other Words for Cold",
                        "og:description": "Cold synonyms. Top synonyms for cold (other words for cold) are cool, frigid and chilly.",
                        "fb:app_id": "424964784740714",
                        "twitter:site": "@PowerThesaurus",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0",
                        "og:locale": "en_US",
                        "og:url": "https://www.powerthesaurus.org/cold/synonyms",
                        "google-play-app": "app-id=org.powerthesaurus.powerthesaurus"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.radyushin.com/images/pt_logo_1200x1200.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Cold | Synonyms of Cold by Lexico",
            "htmlTitle": "<b>Cold</b> | <b>Synonyms</b> of <b>Cold</b> by Lexico",
            "link": "https://www.lexico.com/synonym/cold",
            "displayLink": "www.lexico.com",
            "snippet": "adjective · 1'a cold day' SYNONYMS chilly, cool, freezing, icy, snowy, icy-cold, \nglacial, wintry, crisp, frosty, frigid, bitter, bitterly cold, biting, piercing, numbing, \nsharp ...",
            "htmlSnippet": "adjective &middot; 1&#39;a <b>cold</b> day&#39; <b>SYNONYMS</b> chilly, cool, freezing, icy, snowy, icy-<b>cold</b>, <br>\nglacial, wintry, crisp, frosty, frigid, bitter, bitterly <b>cold</b>, biting, piercing, numbing, <br>\nsharp&nbsp;...",
            "cacheId": "J2he1jns6HoJ",
            "formattedUrl": "https://www.lexico.com/synonym/cold",
            "htmlFormattedUrl": "https://www.lexico.com/<b>synonym</b>/<b>cold</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT-fHqiHsELFzBLFkvhF3MUNoGyDc02pwOeICq9JD-vhUo5TV_0a-_rYOwh",
                        "width": "291",
                        "height": "173"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-tilecolor": "#50b46c",
                        "og:image": "https://www.lexico.com/lexico-logo.png",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Cold | Synonyms of Cold by Lexico",
                        "og:type": "website",
                        "theme-color": "#50b46c",
                        "og:site_name": "Lexico Dictionaries | English",
                        "og:title": "Cold | Synonyms of Cold by Lexico",
                        "msapplication-tileimage": "/mstile-144x144.png",
                        "csrf-param": "authenticity_token",
                        "og:description": "What is the definition of cold? What is the meaning of cold? How do you use cold in a sentence? What are synonyms for cold?",
                        "twitter:image": "https://www.lexico.com/lexico-logo.png",
                        "viewport": "width=device-width, initial-scale=1, shrink-to-fit=no",
                        "twitter:description": "What is the definition of cold? What is the meaning of cold? How do you use cold in a sentence? What are synonyms for cold?",
                        "csrf-token": "l9+efCT7x21Q6dllBnc+3gaIh6UjAnQmMapYeuQVdn5l75S3yWywZnU4UDzJJ/S1XE7Jo/gji0huxHZO7k0kCg==",
                        "og:url": "https://www.lexico.com/synonym/cold"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.lexico.com/lexico-logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "100+ words for 'cold' - Reverse Dictionary",
            "htmlTitle": "100+ words for &#39;<b>cold</b>&#39; - Reverse Dictionary",
            "link": "https://reversedictionary.org/wordsfor/cold",
            "displayLink": "reversedictionary.org",
            "snippet": "ice piercing cooler ague cold-blooded keen cryo- snap refrigerate C wintry biting \n... chitter numb warm cryotherapy iced common cold ice–cold thaw exposure frill \n... That project is closer to a thesaurus in the sense that it returns synonyms for a ...",
            "htmlSnippet": "ice piercing cooler ague <b>cold</b>-blooded keen cryo- snap refrigerate C wintry biting <br>\n... chitter numb warm cryotherapy iced common <b>cold</b> ice–<b>cold</b> thaw exposure frill <br>\n... That project is closer to a thesaurus in the sense that it returns <b>synonyms</b> for a&nbsp;...",
            "cacheId": "lQDGutvB03gJ",
            "formattedUrl": "https://reversedictionary.org/wordsfor/cold",
            "htmlFormattedUrl": "https://reversedictionary.org/wordsfor/<b>cold</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRlGeH0MbtCzW6p7RhFECCGsQJgy7TpIzso4wayW-PWq0XbBEkUq9U-YYk",
                        "width": "256",
                        "height": "197"
                    }
                ],
                "metatags": [
                    {
                        "viewport": "width=device-width, initial-scale=1"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://reversedictionary.org/img/logo.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Cold Synonyms & Antonyms | Synonyms.com",
            "htmlTitle": "<b>Cold Synonyms</b> &amp; Antonyms | Synonyms.com",
            "link": "https://www.synonyms.com/synonym/cold",
            "displayLink": "www.synonyms.com",
            "snippet": "Sep 3, 2019 ... Find all the synonyms and alternative words for cold at Synonyms.com, the \nlargest free online thesaurus, antonyms, definitions and translations ...",
            "htmlSnippet": "Sep 3, 2019 <b>...</b> Find all the <b>synonyms</b> and alternative words for <b>cold</b> at <b>Synonyms</b>.com, the <br>\nlargest free online thesaurus, antonyms, definitions and translations&nbsp;...",
            "cacheId": "RJtSPPUe5SAJ",
            "formattedUrl": "https://www.synonyms.com/synonym/cold",
            "htmlFormattedUrl": "https://www.<b>synonyms</b>.com/<b>synonym</b>/<b>cold</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTfFdFBpAbs2y9VqciFjI-PzuYaJNHhUdN6-Q30WbBLt9mc6631QX3kvAW-",
                        "width": "258",
                        "height": "196"
                    }
                ],
                "metatags": [
                    {
                        "fb:app_id": "256377701138813",
                        "viewport": "width=device-width, initial-scale=1",
                        "og:url": "https://www.synonyms.com/synonym/cold",
                        "google-signin-client_id": "567628204450-v86e6utrquhm3h5qp94e14ss79pinecj.apps.googleusercontent.com"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.synonyms.com/root/app_common/img/top_logo_syn.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "cold synonyms with definition | Macmillan Thesaurus",
            "htmlTitle": "<b>cold synonyms</b> with definition | Macmillan Thesaurus",
            "link": "https://www.macmillanthesaurus.com/us/cold",
            "displayLink": "www.macmillanthesaurus.com",
            "snippet": "Synonyms for 'cold': cool, icy, chilly, glacial, chill, bitter, freezing, arctic, bitterly \ncold, bracing, chilling, frigid, ice-cold, parky.",
            "htmlSnippet": "<b>Synonyms</b> for &#39;<b>cold</b>&#39;: cool, icy, chilly, glacial, chill, bitter, freezing, arctic, bitterly <br>\n<b>cold</b>, bracing, chilling, frigid, ice-<b>cold</b>, parky.",
            "cacheId": "5QMVb9cRQm4J",
            "formattedUrl": "https://www.macmillanthesaurus.com/us/cold",
            "htmlFormattedUrl": "https://www.macmillanthesaurus.com/us/<b>cold</b>",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRu4knELk1zAuP1o_6qYG9N8wS3QGS-A7cIYqn929xoH6UJR26QGBoXkuE",
                        "width": "426",
                        "height": "118"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "/us/external/images/logoThesaurus.png?version=1.0.46",
                        "viewport": "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no",
                        "og:title": "cold synonyms with definition | Macmillan Thesaurus",
                        "title": "cold synonyms with definition | Macmillan Thesaurus",
                        "og:url": "https://www.macmillanthesaurus.com/us/cold",
                        "og:description": "Synonyms for 'cold': cool, icy, chilly, glacial, chill, bitter, freezing, arctic, bitterly cold, bracing, chilling, frigid, ice-cold, parky"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.macmillanthesaurus.com/us/external/images/bg-wave.svg?version=1.0.46"
                    }
                ]
            }
        }
    ]
}